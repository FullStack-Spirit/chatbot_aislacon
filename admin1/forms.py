from django import forms
from dashboard.models import *
from django.contrib.admin import widgets
from django.core.files import File
from django.contrib.auth.models import *
from django.forms import ModelChoiceField, ModelMultipleChoiceField

class ClienteForm(forms.ModelForm):

    class Meta:
        model = Cliente
        fields = ('nombre_cliente', \
         	'logotipo')
    def __init__(self, *args, **kwargs):
        super(ClienteForm, self).__init__(*args, **kwargs)
        for campos in self.fields:
            self.fields[campos].widget.attrs.update({'class': 'form-control'})
            self.fields[campos].required = True
            self.fields['nombre_cliente'].widget.attrs["placeholder"] = 'Nombre de cliente'

class AgenteForm(forms.ModelForm):

    class Meta:
        model = Agente
        exclude = ('fecha_registro','webchat_key',)
    def __init__(self, *args, **kwargs):
        super(AgenteForm, self).__init__(*args, **kwargs)
        for campos in self.fields:
            self.fields[campos].widget.attrs.update({'class': 'form-control'})
            self.fields[campos].required = True
        self.fields['telegram_token'].required = False
        self.fields['facebook_token'].required = False
        self.fields['webchat_token'].required = False
        self.fields['api_ws'].required = False


class UsuarioForm(forms.Form):
    apellido_materno = forms.CharField(label = "Apellido Materno", required = True, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido materno'}))
    apellido_paterno = forms.CharField(label = "Apellido Paterno", required = True, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido paterno'}))
    nombres = forms.CharField(label = "Nombre", required = True, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombres'}))
    email = forms.EmailField(label = "Correo electrónico", required = True, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Correo electrónico'}))
    username = forms.CharField(label = "Nombre de usuario", required = True, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre de usuario'}))
    cliente = forms.ModelChoiceField(label = 'Cliente', required = True, queryset=Cliente.objects.all().order_by('nombre_cliente'), initial=1,  widget = forms.Select(attrs={'class': 'form-control'}))
    def __init__(self, *args, **kwargs):
        super(UsuarioForm, self).__init__(*args, **kwargs)
        # Errores predeterminados definidos en el modelo este disparará errores para campo requerido, unico, invalido y con caracterers faltantes
        for field in self.fields.values():
            field.error_messages = {'required':'Ingrese {fieldname}'.format(
                fieldname=field.label), 'unique':'{fieldname} registrado en el sistema'.format(
                fieldname=field.label), 'invalid':'Valor Inválido'.format(
                fieldname=field.label), 'min_length':'Realice completacion de campo {fieldname}'.format(
                fieldname=field.label)}

class BorraSesionPlataforma(forms.Form):
    agente = forms.ModelChoiceField(label = 'Agente', required = True, queryset=Agente.objects.all().order_by('nombre_agente'), initial=1,  widget = forms.Select(attrs={'class': 'form-control'}))
    plataforma = forms.ModelChoiceField(label = 'Plataforma', required = True, queryset=Plataforma.objects.all().order_by('originalDetectIntentRequest_source'), initial=1,  widget = forms.Select(attrs={'class': 'form-control'}))
    def __init__(self, *args, **kwargs):
        super(BorraSesionPlataforma, self).__init__(*args, **kwargs)
        # Errores predeterminados definidos en el modelo este disparará errores para campo requerido, unico, invalido y con caracterers faltantes
        for field in self.fields.values():
            field.error_messages = {'required':'Ingrese {fieldname}'.format(
                fieldname=field.label), 'unique':'{fieldname} registrado en el sistema'.format(
                fieldname=field.label), 'invalid':'Valor Inválido'.format(
                fieldname=field.label), 'min_length':'Realice completacion de campo {fieldname}'.format(
                fieldname=field.label)}

class IngresarGraficaForm(forms.Form):
    nombre_grafica = forms.CharField(label = "Nombre de la gráfica", required = True, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre de la gráfica'}))
    nombre_funcion = forms.CharField(label = "Nombre de la función", required = True, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre de la función'}))
    def __init__(self, *args, **kwargs):
        super(IngresarGraficaForm, self).__init__(*args, **kwargs)
        # Errores predeterminados definidos en el modelo este disparará errores para campo requerido, unico, invalido y con caracterers faltantes
        for field in self.fields.values():
            field.error_messages = {'required':'Ingrese {fieldname}'.format(
                fieldname=field.label), 'unique':'{fieldname} registrado en el sistema'.format(
                fieldname=field.label), 'invalid':'Valor Inválido'.format(
                fieldname=field.label), 'min_length':'Realice completacion de campo {fieldname}'.format(
                fieldname=field.label)}

class FileUploadForm(forms.Form):
    file_source = forms.FileField(label = "Dialogflow Key", required = True)
    nombre_agente = forms.CharField(label = "Nombre Agente", required = True)
    def __init__(self, *args, **kwargs):
        super(FileUploadForm, self).__init__(*args, **kwargs)
        # Errores predeterminados definidos en el modelo este disparará errores para campo requerido, unico, invalido y con caracterers faltantes
        for field in self.fields.values():
            field.error_messages = {'required':'Ingrese {fieldname}'.format(
                fieldname=field.label), 'unique':'{fieldname} registrado en el sistema'.format(
                fieldname=field.label), 'invalid':'Valor Inválido'.format(
                fieldname=field.label), 'min_length':'Realice completacion de campo {fieldname}'.format(
                fieldname=field.label)}
        self.fields['nombre_agente'].widget = forms.HiddenInput()
