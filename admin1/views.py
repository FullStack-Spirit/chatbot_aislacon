from django.shortcuts import render, HttpResponseRedirect
from .forms import *
from dashboard.models import *
from estadisticas.models import *
from django.contrib.auth.models import *
from usuarios.models import *
from django.core.mail import BadHeaderError, send_mail
from django.core.mail import EmailMessage
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
import requests
import json
from django.http import JsonResponse
import os

@login_required(login_url = '/login/')
@staff_member_required(login_url='/home/')
def ingresar_cliente(request, template_name = "admin1/ingresar_cliente.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	estado = 0
	if request.method == "POST":
		form = ClienteForm(request.POST, request.FILES)
		try:
			if (('/' in request.POST['nombre_cliente']) or ('*' in request.POST['nombre_cliente']) or ('+' in request.POST['nombre_cliente']) or ('-' in request.POST['nombre_cliente'])):
				estado = 3
			else:
				if form.is_valid():
					form.save()
					estado = 1
		except Exception as ex:
			print("\n\nVeamos: {}\n\n".format(type(ex).__name__))
			estado=3
	else:
		form = ClienteForm()
	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
@staff_member_required(login_url='/home/')
def ingresar_agente(request, template_name = "admin1/ingresar_agente.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	estado = 0
	if request.method == "POST":
		formUp = FileUploadForm(data=request.POST, files=request.FILES)
		form = AgenteForm(request.POST)
		if formUp.is_valid():
			agentesaved = Agente.objects.get(nombre_agente=request.POST['nombre_agente'])
			agentesaved.webchat_key = request.FILES['file_source']
			agentesaved.save()

			headers = {
			    'content-type': 'application/json',
			}

			data = '{"username": "springAdmin","password": "abcde"}'

			response = requests.post('https://pruebas.springlabsdevs.net/webbot-api/login', headers=headers, data=data)
			response_json = response.json()
			token = response_json['result']['token']

			print("#"*20)
			print(token)

			url = "https://pruebas.springlabsdevs.net/webbot-api/clients/create"

			payload = {
			         "clientName": "A.B. SA de CV",
			         "title": "Soporte",
			         "languageCode": "es-es",
			         "allowedOrigins": "http://springlabsdevs.online",
			         "apiUrl": "http://springlabsdevs.online:3000/demo/api",
			         "color": "danger",
			         "dateFormat": "ll",
			         "senderName": "Ascesor",
			         "senderPhotoUrl": "https://placehold.it/550x550?text=No+photo",
					 "key": request.FILES['file_source'],
			}

			headers = {
			    'token': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImU1MGE0NzIxLTg5ZGMtNTQ0Ny1hYmFiLTZkNGI4OWRlNThhZSIsInVzZXJuYW1lIjoic3ByaW5nQWRtaW4iLCJwZXJtaXNzaW9ucyI6WyJhbGwiXSwiaWF0IjoxNTU5MjM0OTE5LCJleHAiOjE1NTkyNjM3MTl9.5yChGyNEefViUtkzXI_8AreMXvECgtqHbr-KzSklXww",
			    'Content-Type': "application/json",
			    'cache-control': "no-cache",
			    'Postman-Token': "362075cc-5e36-4498-ae0a-c692665ddf71"
			    }

			response = requests.request("POST", url, data=payload, headers=headers)

			print(response)

			estado = 4
		else:
			try:
				if (('/' in request.POST['nombre_agente']) or ('*' in request.POST['nombre_agente']) or ('+' in request.POST['nombre_agente'])):
					estado = 3
				else:
					if form.is_valid():
						nuevo_agente = form.save(commit=False)
						nuevo_agente.fecha_registro = timezone.now()
						nuevo_agente.save()
						estado=1
						agentesaved = Agente.objects.get(nombre_agente=request.POST['nombre_agente'])
			except:
				estado=3
	else:
		form = AgenteForm()
		formUp = FileUploadForm()
	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
@staff_member_required(login_url='/home/')
def ingresar_usuario(request, template_name = "admin1/ingresar_usuario.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	estado = 0
	if request.method == "POST":
		form = UsuarioForm(request.POST)
		password = 'p4ss_t3mp0r4l'
		# password = User.objects.make_random_password()
		if form.is_valid():
			objCliente = Cliente.objects.get(nombre_cliente=form.cleaned_data['cliente'])
			usuarioNuevo = User.objects.create_user(username=form.cleaned_data['username'], password=password, email=form.cleaned_data['email'], first_name=form.cleaned_data['nombres'])
			grupoAdmin = Group.objects.get(name='Cliente Administrador')
			grupoAdmin.user_set.add(usuarioNuevo)
			usuarioNuevo.is_active = False
			usuarioNuevo.save()
			userExt = Usuario(user = usuarioNuevo, apellido_paterno=form.cleaned_data['apellido_paterno'], apellido_materno=form.cleaned_data['apellido_materno'], cliente = objCliente)
			userExt.save()
			email = EmailMessage('Bienvenido a SpringLabsBots',\
			 'Contraseña: p4ss_t3mp0r4l ' + '\n' + 'Usuario: ' + usuarioNuevo.username,\
			 to=[usuarioNuevo.email])
			#email.send()
			estado=1

	else:
		form = UsuarioForm()

	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
@staff_member_required(login_url='/home/')
def borrar_sesiones_plataforma(request, template_name = "admin1/borrar_sesiones_plataforma.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	estado = 0
	if request.method == "POST":
		form = BorraSesionPlataforma(request.POST)
		if form.is_valid():
			agente = Agente.objects.get(nombre_agente=form.cleaned_data['agente'])
			plataforma = Plataforma.objects.get(originalDetectIntentRequest_source=form.cleaned_data['plataforma'])
			# Todas las sesiones del agente
			sesiones = Sesion.objects.filter(id_proyecto=agente)
			# Todas las interacciones del agente filtradas por plataforma
			interacciones = Interaccion.objects.filter(session__in = sesiones, originalDetectIntentRequest_source = plataforma)
			# Lista de sesiones de ese agente con esa plataformas_del_agente
			lista = []
			for dato in interacciones:
				lista.append(dato.session.session)
			# Sesiones del agente con esa plataformas_del_agente
			sesiones = Sesion.objects.filter(session__in=lista)
			sesiones.delete()
			estado=1

	else:
		form = BorraSesionPlataforma()

	return render(request, template_name, locals(),)


@login_required(login_url = '/login/')
@staff_member_required(login_url='/home/')
def ingresar_grafica(request, template_name = "admin1/ingresar_grafica.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	estado = 0
	if request.method == "POST":
		form = IngresarGraficaForm(request.POST)
		try:
			if form.is_valid():
				nombre_grafica = form.cleaned_data['nombre_grafica']
				nombre_funcion = form.cleaned_data['nombre_funcion']
				nombre_funcion = nombre_funcion.replace('  ', ' ')
				nombre_sin_espacios = nombre_funcion.replace(' ', '_').lower()
				nombre_div = nombre_sin_espacios + "_div"
				nombre_variable = nombre_sin_espacios + "_var"
				nueva_grafica = Grafica(nombre_grafica = nombre_grafica,\
					nombre_funcion = nombre_funcion,\
					nombre_div = nombre_div,\
					nombre_variable = nombre_variable)
				nueva_grafica.save()
				estado=1
		except:
			estado=3
	else:
		form = IngresarGraficaForm()



	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
@staff_member_required(login_url='/home/')
def asignar_grafica(request, id_cliente, template_name = "admin1/asignar_grafica.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	estado = 0

	if id_cliente == "0":
		clientes = Cliente.objects.all()
		# print("\n\nSeleccionar cliente")
		# print(clientes)
	else:
		cliente = Cliente.objects.get(pk = id_cliente)
		graficas_seleccionadas = GraficaCliente.objects.all().filter(cliente = cliente)

		lista_graficas_seleccionadas = []
		for grafica in graficas_seleccionadas:
			lista_graficas_seleccionadas.append(grafica.grafica.nombre_grafica)

		graficas_seleccionadas2 = Grafica.objects.all().filter(nombre_grafica__in = lista_graficas_seleccionadas)
		graficas_no_seleccionadas = Grafica.objects.all().exclude(nombre_grafica__in = lista_graficas_seleccionadas)
		# print("\n\nCliente")
		# print(cliente)
		# print("\n\n Graficas seleccionadas")
		# print(graficas_seleccionadas2)
		# print("\n\n Graficas NO seleccionadas")
		# print(graficas_no_seleccionadas)

	return render(request, template_name, locals(),)


@login_required(login_url = '/login/')
@staff_member_required(login_url='/home/')
def agregar_graficas(request, id_cliente, id_grafica):

	cliente = Cliente.objects.get(pk = id_cliente)
	grafica = Grafica.objects.get(pk = id_grafica)

	grafica_seleccionada = GraficaCliente.objects.all().filter(cliente = cliente, grafica = grafica).exists()

	if grafica_seleccionada == 0:
		print("\n\n\nNO EXISTE RELACIÓN")
		grafica_nueva = GraficaCliente(cliente = cliente, grafica = grafica)
		grafica_nueva.save()
	else:
		print("\n\n\nSI EXISTE RELACIÓN")
		grafica_seleccionada = GraficaCliente.objects.all().filter(cliente = cliente, grafica = grafica)
		grafica_seleccionada.delete()

	return HttpResponseRedirect('/asignar_grafica/%s' % id_cliente)
