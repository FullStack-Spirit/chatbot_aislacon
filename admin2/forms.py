from django import forms
from usuarios.models import *
from django.contrib.admin import widgets 
from django.core.files import File
from django.contrib.auth.models import *
from dashboard.models import *
from usuarios.models import *
from django.db import IntegrityError
from django.contrib.auth.models import User
from django.forms import ModelChoiceField, ModelMultipleChoiceField

class UserFormOperador(forms.Form):

    username = forms.CharField(label = "Usuario",max_length=50,min_length=3 ,required = True, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Usuario'}))
    email = forms.EmailField(label = "Correo electrónico", required = True, widget = forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Correo electrónico'}))
    nombres = forms.CharField(label = "Nombre", required = True, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre del usuario'}))
    apellido_paterno = forms.CharField(label = "Apellido Paterno", required = True, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido Paterno'}))
    apellido_materno = forms.CharField(label = "Apellido Materno", required = True, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido Materno'}))
    def __init__(self, *args, **kwargs):
        super(UserFormOperador, self).__init__(*args, **kwargs)
        # Errores predeterminados definidos en el modelo este disparará errores para campo requerido, unico, invalido y con caracterers faltantes
        for field in self.fields.values():
            field.error_messages = {'required':'Ingrese {fieldname}'.format(
                fieldname=field.label), 'unique':'{fieldname} registrado en el sistema'.format(
                fieldname=field.label), 'invalid':'Valor Inválido'.format(
                fieldname=field.label), 'min_length':'Realice completacion de campo {fieldname}'.format(
                fieldname=field.label)}


class UsuarioAgenteForm(forms.Form):

    agente = forms.CharField()

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(UsuarioAgenteForm, self).__init__(*args, **kwargs)
        
        # AQUÍ ESTÁ EL USUARIO
        # print(self.user)

        usuario = self.user
        usuarioExt = Usuario.objects.get(user=usuario)
        cliente = Cliente.objects.get(nombre_cliente=usuarioExt.cliente)

        self.fields['agente'] = forms.ModelMultipleChoiceField(label = 'Bots disponibles', required = False, queryset=Agente.objects.filter(cliente=cliente), widget = forms.CheckboxSelectMultiple())

        for field in self.fields.values():
            field.error_messages = {'required':'Ingrese {fieldname}'.format(
                fieldname=field.label), 'unique':'{fieldname} registrado en el sistema'.format(
                fieldname=field.label), 'invalid':'Valor Inválido'.format(
                fieldname=field.label), 'min_length':'Realice completacion de campo {fieldname}'.format(
                fieldname=field.label)}
                
        for campos in self.fields:
            self.fields[campos].widget.attrs.update({'class': 'form-control'})
            self.fields['agente'].widget.attrs["class"] = 'i-checks'
