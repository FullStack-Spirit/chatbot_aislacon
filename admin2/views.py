from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from dashboard.models import *
from usuarios.models import *
from datetime import datetime, timedelta
from django.db import IntegrityError
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.utils import timezone
from django.db.models import *
from dashboard.utils import *
from django.db.models.functions import Lower
from .forms import *
import time
from django.core.mail import EmailMessage
from smtplib import SMTPException
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import user_passes_test


def group_check(user):
    return user.groups.filter(name__in=['Cliente Administrador', 'Administrador'])


@login_required(login_url = '/login/')
@user_passes_test(group_check, login_url='/home/')
def equipos_agente(request, template_name = "admin2/equipos_agente.html"):
    datos_empresa = DatosEmpresa.objects.first()
    usuario = request.user
    try:
        extras = Usuario.objects.get(user = usuario.pk)
        foto = 1
    except:
        foto = 0
    usuarioExt = Usuario.objects.get(user=usuario)
    cliente = Cliente.objects.get(nombre_cliente=usuarioExt.cliente)
    agentes = Agente.objects.filter(cliente=cliente)
    return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
@user_passes_test(group_check, login_url='/home/')
def equipos_agenteusuarios(request, id_agente, template_name = "admin2/equipos_agenteusuarios.html"):
    datos_empresa = DatosEmpresa.objects.first()
    usuario = request.user
    try:
        extras = Usuario.objects.get(user = usuario.pk)
        foto = 1
    except:
        foto = 0
    agente = Agente.objects.get(pk=id_agente)
    cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
    operadores = AgenteUsuario.objects.all().filter(agente=agente)
    #CREAMOS LA LISTA DE LOS OPERADORES A PARTIR DEL AGENTE
    lista_de_operadores = []
    for dato in operadores:
        lista_de_operadores.append(dato.user)

    operadores_no_asignados_ext = Usuario.objects.filter(cliente = cliente).exclude(user__in=lista_de_operadores)
    lista_de_operadores_no_asignados = []
    for dato in operadores_no_asignados_ext:
        lista_de_operadores_no_asignados.append(dato.user)
    operadores_finales = Usuario.objects.filter(user__in=lista_de_operadores)
    operadores_no_asignados = User.objects.filter(username__in = lista_de_operadores_no_asignados).exclude(groups__name='Cliente Administrador')
    return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
@user_passes_test(group_check, login_url='/home/')
def ingresar_operador(request, template_name = "admin2/ingresar_operador.html"):
    datos_empresa = DatosEmpresa.objects.first()
    usuario = request.user
    try:
        extras = Usuario.objects.get(user = usuario.pk)
        foto = 1
    except:
        foto = 0
    usuarioExt = Usuario.objects.get(user=usuario)
    cliente = Cliente.objects.get(nombre_cliente=usuarioExt.cliente)
    agentes = Agente.objects.filter(cliente=cliente)
    userNew = ""
    estado = 0
    if request.method == "POST":
        operador_post = UserFormOperador(request.POST)
        password = "p4ss_t3mp0r4l"
        agentes_post = UsuarioAgenteForm(request.POST, user=request.user)
        try:
            if operador_post.is_valid():
                usuarioNuevo = User.objects.create_user(username=operador_post.cleaned_data['username'], password=password, email=operador_post.cleaned_data['email'], first_name=operador_post.cleaned_data['nombres'])
                userNew = operador_post.cleaned_data['username']
                grupoUsuario = Group.objects.get(name='Usuario Cliente')
                grupoUsuario.user_set.add(usuarioNuevo)
                usuarioNuevo.is_active = False
                usuarioNuevo.save()
                userExt = Usuario(user = usuarioNuevo, apellido_paterno=operador_post.cleaned_data['apellido_paterno'], apellido_materno=operador_post.cleaned_data['apellido_materno'], cliente = cliente)
                userExt.save()
                email = EmailMessage('Bienvenido a SpringLabsBots',\
                 'Contraseña: p4ss_t3mp0r4l ' + '\n' + 'Usuario: ' + usuarioNuevo.username,\
                 to=[usuarioNuevo.email])
                email.send()
                estado = 1
        except IntegrityError:
            mensaje_error = "El nombre de usuario (username) ya existe."
        except SMTPException as e:
            mensaje_error = "La notificación vía email no pudo ser enviada."
        if estado == 1:
            try:
                if agentes_post.is_valid():
                    print("************ AGENTES_POST YA ES VALIDO ****************")
                    userNew = User.objects.get(username = userNew)
                    userNewExt = Usuario.objects.get(user=userNew)
                    agentesInForm = agentes_post.cleaned_data['agente']
                    for dato in agentesInForm:
                        objAgente = Agente.objects.get(nombre_agente=dato)
                        if objAgente:
                            asignacion = AgenteUsuario(user=userNewExt,agente=objAgente)
                            asignacion.save()
                    mensaje_exito = "El usuario se registró de manera exitosa."
            except:
                # NO SE DEBE REGISTRAR EL USUARIO SI HAY ALGÚN ERROR
                borrar = User.objects.get(username = userNew)
                estado=2
        operador = UserFormOperador()
        usuarioAgente = UsuarioAgenteForm(user=request.user)
    else:
        operador = UserFormOperador()
        usuarioAgente = UsuarioAgenteForm(user=request.user)
    return render(request, template_name, locals(),)

# El request trae el id se la sesion
@login_required(login_url = '/login/')
def asignar_bot(request):

    agente_id = request.GET.get('agente_id', None)
    operador_id = request.GET.get('operador_id', None)

    agente = Agente.objects.get(pk=agente_id)
    operador = User.objects.get(pk = operador_id)
    operadorExt = Usuario.objects.get(user = operador)

    asignacion = AgenteUsuario(
        user = operadorExt,
        agente = agente,
    )
    asignacion.save()

    data = {'resultado': 'Usuario asignado exitosamente'}

    return JsonResponse(data)

@login_required(login_url = '/login/')
def desvincular_bot(request):

    agente_id = request.GET.get('agente_id', None)
    operador_id = request.GET.get('operador_id', None)

    agente = Agente.objects.get(pk=agente_id)
    operador = User.objects.get(pk = operador_id)
    operadorExt = Usuario.objects.get(user = operador)
    vinculo = AgenteUsuario.objects.get(user = operadorExt, agente = agente)
    vinculo.delete()

    data = {'resultado': 'Usuario desvinculado exitosamente'}

    return JsonResponse(data)
