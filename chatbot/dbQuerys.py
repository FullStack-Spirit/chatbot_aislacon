# Se crea un archivo para realizar querys.
import pymysql
import psycopg2
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

database = config['DEFAULT']['DB_NAME']
password = config['DEFAULT']['DB_PASSWORD']
user = config['DEFAULT']['DB_USER']
host = config['DEFAULT']['DB_HOST']
basedatos = config['DEFAULT']['DB_BASEDATOS']
 
class Query:
    def __init__(self,busqueda,tabla,*data):
        try:
            if basedatos === 'mysql':
                    conexion = pymysql.connect( host=host,
                                                user=user,
                                                password=password,
                                                db=database)
                elif:
                    basedatos === 'postgres':
                    conexion = pymysql.connect( host=host,
                                                user=user,
                                                password=password,
                                                database=database)
                self.conexion = conexion
                self.busqueda = busqueda
                self.tabla = tabla
                self.data = data
                        
            except:
                    print("Error en la conexion de la base de datos")
    
    def Insert(self):
        try:
            with self.conexion.cursor() as cursor:

                    consulta = 'INSERT INTO '+str(self.tabla)+'(nombre,apellido_paterno,apellido_materno,usuario,password) VALUES (%s, %s, %s, %s, %s);'
                    #Insertamos en la tabla Usuario
                    cursor.execute(consulta, (
                        self.data.nombre, 
                        self.data.apellido_paterno, 
                        self.data.apellido_materno, 
                        self.data.usuario, 
                        self.data.password))
                        self.conexion.commit()
                    
                    return True
        finally:
                    self.conexion.close() 
    
    def Select(self):
        try:
            with self.conexion.cursor() as cursor:
                    consulta = 'SELECT *  FROM '+str(self.tabla)+'(nombre,apellido_paterno,apellido_materno,usuario,password) VALUES (%s, %s, %s, %s, %s);'
                    #Insertamos en la tabla Usuario
                    cursor.execute(consulta, (
                        self.data.nombre, 
                        self.data.apellido_paterno, 
                        self.data.apellido_materno, 
                        self.data.usuario, 
                        self.data.password))
                        self.conexion.commit()
                    
                    return True
        finally:
                    self.conexion.close() 
        
    def Delete(self):
        pass
