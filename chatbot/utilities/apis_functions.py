from django.db import connection
import requests

def EnvioAPISalesforse(nombre, apellidos, empresa, direccion, ciudad, estado, sector, nicho, proyectos, camaras, comentario, email, sitioweb, telefono, cel, session):
    
    #-----------------------------------------------------------------------------------------------------------------------
    correo = ["mrodriguez@grupoaislacon.com.mx","ahurtado@grupoaislacon.com.mx","cvargas@grupoaislacon.com.mx","rmata@grupoaislacon.com.mx","contactosweb@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","jaime@grupoaislacon.com.mx","jaime@grupoaislacon.com.mx","jchapa@grupoaislacon.com.mx","rdario@grupoaislacon.com.mx"]
    correo2 = ["nalvarado@grupoaislacon.com.mx","cvargas@grupoaislacon.com.mx","mrodriguez@grupoaislacon.com.mx","rmata@grupoaislacon.com.mx","contactosweb@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","jaime@grupoaislacon.com.mx","jchapa@grupoaislacon.com.mx","rdario@grupoaislacon.com.mx"]
    correo3 = ["rvaldes@grupoaislacon.com.mx","cvargas@grupoaislacon.com.mx","mrodriguez@grupoaislacon.com.mx","rmata@grupoaislacon.com.mx","contactosweb@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","jaime@grupoaislacon.com.mx","jchapa@grupoaislacon.com.mx","rdario@grupoaislacon.com.mx"]
    correo4 = ["cvargas@grupoaislacon.com.mx","cvargas@grupoaislacon.com.mx","mrodriguez@grupoaislacon.com.mx","rmata@grupoaislacon.com.mx","contactosweb@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","jaime@grupoaislacon.com.mx","jchapa@grupoaislacon.com.mx","rdario@grupoaislacon.com.mx"]
    correo5 = ["rvaldes@grupoaislacon.com.mx","cvargas@grupoaislacon.com.mx","mrodriguez@grupoaislacon.com.mx","rmata@grupoaislacon.com.mx","rvaldes@grupoaislacon.com.mx","cvargas@grupoaislacon.com.mx","mrodriguez@grupoaislacon.com.mx","rmata@grupoaislacon.com.mx","contactosweb@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","jaime@grupoaislacon.com.mx","jchapa@grupoaislacon.com.mx","rdario@grupoaislacon.com.mx"]
    correo6 = ["cvargas@grupoaislacon.com.mx","cvargas@grupoaislacon.com.mx","mrodriguez@grupoaislacon.com.mx","rmata@grupoaislacon.com.mx","contactosweb@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","jaime@grupoaislacon.com.mx","jaime@grupoaislacon.com.mx","jchapa@grupoaislacon.com.mx","rdario@grupoaislacon.com.mx"]
    correo7 = ["cvargas@grupoaislacon.com.mx","cvargas@grupoaislacon.com.mx","mrodriguez@grupoaislacon.com.mx","rmata@grupoaislacon.com.mx","contactosweb@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","jaime@grupoaislacon.com.mx","jchapa@grupoaislacon.com.mx,""rdario@grupoaislacon.com.mx"]
    correo8 = ["cvargas@grupoaislacon.com.mx","cvargas@grupoaislacon.com.mx","mrodriguez@grupoaislacon.com.mx","rmata@grupoaislacon.com.mx","contactosweb@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","jaime@grupoaislacon.com.mx","jchapa@grupoaislacon.com.mx","rdario@grupoaislacon.com.mx"]
    correo9 = ["mrodriguez@grupoaislacon.com.mx","nalvarado@grupoaislacon.com.mx","cvargas@grupoaislacon.com.mx","rmata@grupoaislacon.com.mx","contactosweb@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","jchapa@grupoaislacon.com.mx","rdario@grupoaislacon.com.mx"]
    correo10 = ["cvargas@grupoaislacon.com.mx","cvargas@grupoaislacon.com.mx","mrodriguez@grupoaislacon.com.mx","rmata@grupoaislacon.com.mx","contactosweb@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","jaime@grupoaislacon.com.mx","jchapa@grupoaislacon.com.mx","rdario@grupoaislacon.com.mx"]
    correo11 = ["cvargas@grupoaislacon.com.mx","cvargas@grupoaislacon.com.mx","mrodriguez@grupoaislacon.com.mx","rmata@grupoaislacon.com.mx","contactosweb@grupoaislacon.com.mx","notificacioncontacto@grupoaislacon.com.mx","jaime@grupoaislacon.com.mx","jchapa@grupoaislacon.com.mx","rdario@grupoaislacon.com.mx"]
    #--------------------------------------------------------------------------------------------------------------------------
    conteo = camaras.split(";")
    camaras1 = camaras.replace("_", " ")
    query2 = connection.cursor()
    consulta2 = "SELECT id FROM sector WHERE nombre= '"+str(sector)+"'"
    query2.execute(consulta2)
    datos2 = query2.fetchone()
    valorSectorId = int(datos2[0])
    print(valorSectorId)
    try:
        url = "https://test.salesforce.com/servlet/servlet.WebToLead"
        querystring = {
            "encoding":"UTF-8",
            "oid":"00D0x0000000QAt",
            "retURL":"http://aislacon.com.mx",
            "first_name":nombre,
            "last_name":apellidos,
            "debug":"0",
            "debugEmail":"notificacioncontacto@grupoaislacon.com.mx",
            "company":empresa,
            "00Ni000000CjbFg":direccion,    #Direccion
            "00Ni000000CjbFh":ciudad,    #Ciudad
            "00Ni000000CjbFq":estado, #Estado
            "00Ni000000CjbFl":"",
            "phone":telefono,
            "mobile":cel,
            "URL":sitioweb,   #delegacion
            "email":email, 
            "00Ni000000CjbZ7":"",   #Cuando el sector seleccione otro
            "00Ni000000CjbZA":"Varios",   #Valor de los nichos separados por ;
            "00Ni000000CjbZQ":proyectos, #Proyecto
            "00Ni000000CjbZP":"Por definir",    #Camaras separadas por ;
            "lead_source":"web",    #Valor Fijo
            "Observaciones":comentario,   #
            "industry":sector  #SECTOR
        }
        print("---------------------------------------------------------------------------------------------------------------------------------------------")
        headers = {
            'content-type': "application/x-www-form-urlencoded"
        }
        response = requests.request("POST", url, headers=headers, params=querystring)
        print(response.text)
        print("---------------------------------------------------------------------------------------------------------------------------------------------")
        print("lo que se le mando a  salesforce")
        print("---------------------------------------------------------------------------------------------------------------------------------------------")
        print(querystring)
        print("---------------------------------------------------------------------------------------------------------------------------------------------")
        print("Respuesta salesforce")
    finally:
        url = "http://200.76.17.172/aislatracker/trackPost.action"
        data=[{
                "name": "atrackToken",
                "value": "b904c8cb6be6953dfda7782177f4e54e"
            },
            {
                "name":"nombre",
                "value":empresa
            },
            {
                "name":"last_name",
                "value":apellidos
            },
            {
                "name":"contacto",
                "value":nombre
            },
            {
                "name":"direccion",
                "value":direccion
            },
            {
                "name": "email",
                "value": email
            },
            {
                "name":"observaciones",
                "value":comentario
            },
            {
                "name":"estado",
                "value":estado
            },
            {
                "name":"ciudad",
                "value":ciudad
            },{
                "name":"camara[]",
                "value": 15
              }]
        if int(valorSectorId) == 2:
            for elemento in correo:
                data.append({
                    "name":"vendedor",
                    "value":elemento
                })
        elif int(valorSectorId) == 3:
            for elemento in correo2:
                data.append({
                    "name":"vendedor",
                    "value":elemento
                })
        elif int(valorSectorId) == 4:
            for elemento in correo3:
                data.append({
                    "name":"vendedor",
                    "value":elemento
                })
        elif int(valorSectorId) == 5:
            for elemento in correo4:
                data.append({
                    "name":"vendedor",
                    "value":elemento
                })
        elif int(valorSectorId) == 6:
            for elemento in correo5:
                data.append({
                    "name":"vendedor",
                    "value":elemento
                })
        elif int(valorSectorId) == 7:
            for elemento in correo6:
                data.append({
                    "name":"vendedor",
                    "value":elemento
                })
        elif int(valorSectorId) == 8:
            for elemento in correo7:
                data.append({
                    "name":"vendedor",
                    "value":elemento
                })
        elif int(valorSectorId) == 9:
            for elemento in correo8:
                data.append({
                    "name":"vendedor",
                    "value":elemento
                })
        elif int(valorSectorId) == 10:
            for elemento in correo9:
                data.append({
                    "name":"vendedor",
                    "value":elemento
                })
        elif int(valorSectorId) == 11:
            for elemento in correo10:
                data.append({
                    "name":"vendedor",
                    "value":elemento
                })
        elif int(valorSectorId) == 12:
            for elemento in correo11:
                data.append({
                    "name":"vendedor",
                    "value":elemento
                })
        #for elemento in conteo:
        #    query = connection.cursor()
        #    consulta = "SELECT id FROM tipo_camara WHERE nombre= '"+str(elemento)+"'"
        #    query.execute(consulta)
        #    datos = query.fetchone()
        #    realid = int(datos[0])-1
        #    data.append({
        #        "name":"camara[]",
        #        "value": ""+str(realid)+""
        #    })
        query = connection.cursor()
        consulta = "SELECT id FROM proyecto WHERE nombre= '"+str(proyectos)+"'"
        query.execute(consulta)
        datos = query.fetchone()
        realid = int(datos[0])-1
        data.append(
            {
                "name":"proyecto",
                "value":""+str(realid)+""
            }
        )
        allresult = data
        payload = "data="+str(allresult)+""
        headers = {
            'content-type': "application/x-www-form-urlencoded",
        }
        print("Lo que se le envio a aislatraker")
        print("---------------------------------------------------------------------------------------------------------------------------------------------")
        print(payload)
        print("---------------------------------------------------------------------------------------------------------------------------------------------")
        print("respuesta aislatraker")
        print("---------------------------------------------------------------------------------------------------------------------------------------------")
        response = requests.request("POST", url, data=payload, headers=headers)
        print(response.text)
        print("---------------------------------------------------------------------------------------------------------------------------------------------")
   
