from .responses import *
from .apis_functions import EnvioAPISalesforse
#NOTA:
#Todas las clases tendran que resibir self, aun que no se vaya a utilizar la data (En casos muy extremos)
#self lo que hace es recibir toda la data de la logica de lo funcion _init_
#podremos acceder a los objectos almacenados con self. -> Lo que va a consultar <-

class Function:
    #Se agrefa la funcion __init__ para compartir informaci?n entre clases y subir en perfomance
    def __init__(self, data,plataform):
        self.data = data
        self.plataform = plataform
        self.sesion = self.data['session']

    def ContactoInicio(self):
        apellidos = self.data['queryResult']['parameters']['apellidos']
        nombre = self.data['queryResult']['parameters']['nombre']
        empresa = self.data['queryResult']['parameters']['empresa']
        telefono = self.data['queryResult']['parameters']['telefono']
        direccion = self.data['queryResult']['parameters']['domicilio']
        cel = self.data['queryResult']['parameters']['cel']
        email = self.data['queryResult']['parameters']['correo']
        sitioweb = self.data['queryResult']['parameters']['sitioweb']
        estado = self.data['queryResult']['parameters']['estado']
        ciudad = self.data['queryResult']['parameters']['municipio']
        proyectos = self.data['queryResult']['parameters']['proyecto']
        sector = self.data['queryResult']['parameters']['sector']
        camaras = self.data['queryResult']['parameters']['camara']
        nicho = self.data['queryResult']['parameters']['nicho']
        comentario = self.data['queryResult']['parameters']['mensaje']
        session = self.sesion
        EnvioAPISalesforse(nombre, apellidos, empresa, direccion, ciudad, estado, sector, nicho, proyectos, camaras, comentario, email, sitioweb, telefono, cel, session)
        return True

    def default_func(self):
      print("Intencion sin programacion")

