from .functions import *
from django.http import HttpResponse

def Getfunction(nameIntention, data, plataform):
    #Creamos un contructor con el objeto de las funciones
    funciones = Function(data, plataform)
    #En el primer puesto del arreglo ponemos "Nombre de la intencion", en la segunda posicion "La funcion a ejecutar con esa intencion"
    #  ---> No poner parentesis porque crashea la app <----- 
    options = {
        'Contacto': funciones.ContactoInicio,
        'default': funciones.default_func
    }
    #mandamos a ejecutar la funcion
    result = options.get(nameIntention, options.get('default'))()
    return result

def details(data):
    #Obtenemos el tipo de plataforma de la que viene la información
    try:
        plataform = data['originalDetectIntentRequest']['source']
    except:
        plataform = "Whatsapp"
    nameIntention = data['queryResult']['intent']['displayName']
    #Ejecutamos la funcion para ejecutar por intencion, funciones especificas.
    response = Getfunction(nameIntention,data, plataform)
    return response
