from django.http import JsonResponse


#RESPUESTA DE TEXTO SIMPLE
def simple_response(text):
    reply = {
        "fulfillmentText": "",
        'fulfillmentMessages': [{'text': {'text': [text]}}],
    }
    return reply

#COMBINACIÓN DE TIPOS DE RESPUESTAS (TXT + CARD)
def combinated_response(*responses):
    aux=[]
    responses=list(responses[0])
    for i in responses:
        aux.insert(0,i['fulfillmentMessages'][0],)
    aux.reverse()
    reply = {
        "fulfillmentText": "",
        'fulfillmentMessages': aux,
    }
    return reply


# FUNCION GENERADORA DE TARJETAS CON BOTONES DINÁMICOS (DE 1 A 3 BOTONES)
def card_dinamica(title, subtitle, url, *btns):
    buttons = []
    for btn in btns:
        buttons.append({'text': btn})
    reply = {
        'fulfillmentMessages': [
            {
                'card': {
                    'title': title,
                    'subtitle': subtitle,
                    'imageUri': url,
                    'buttons':
                        buttons

                },

            }
        ],
    }
    return reply


# FUNCIÓN GENERADORA DE QUICK REPLIECE CON VARIAS OPCIONES SEGUN PERMITA LA PLATAFORMA.
def quick_Replice_dinamico(text, options):
    opts = []
    for option in options:
        opts.append(option)
    reply = {
        'fulfillmentMessages': [
            {
                'quickReplies': {
                    'title': text,
                    'quickReplies':
                        opts
                },
            },
        ]
    }
    return reply

def responseWhithID(text, data):
    respuestaDialog = []
    ids = []
    names = []
    for dato in data:
        filterdatos = dato[1].replace("_", " ")
        ids.append(dato[0])
        names.append(filterdatos)
    rango = len(ids)
    for i in range(rango):
        reply1 = 'n- '+str(ids[i])+': '+str(names[i])
        respuestaDialog.append(reply1)
    respuestaDialog1 = str(respuestaDialog)
    respuestaDialog2 = respuestaDialog1.replace("[", "")
    respuestaDialog3 = respuestaDialog2.replace("]", "")
    respuestaDialog4 = respuestaDialog3.replace("'n", "\n")
    respuestaDialog5 = respuestaDialog4.replace("'", "")
    reply = {
        "fulfillmentText": "",
        'fulfillmentMessages': 
        [
            {'text': 
            {'text': [text + str(respuestaDialog5),]
            }}
            ],
    }
    return reply