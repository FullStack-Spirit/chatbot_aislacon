from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .utilities.options import details
from django.views.decorators.csrf import csrf_exempt
import json
import os

def bot_request(data):
    return details(data)
