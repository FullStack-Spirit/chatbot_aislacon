from django.contrib import admin
from .models import *

admin.site.register(Cliente)
admin.site.register(Agente)
admin.site.register(Sesion)
admin.site.register(Interaccion)
admin.site.register(Plataforma)
admin.site.register(Facebook_card)
admin.site.register(Buttons_card)
admin.site.register(DefaultFallbackCount)
admin.site.register(DatosEmpresa)
admin.site.register(AsyncChat)
admin.site.register(Skype_card)
admin.site.register(Skype_buttons_card)
admin.site.register(Cita)
