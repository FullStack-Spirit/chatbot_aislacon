import requests
from django.core.exceptions import PermissionDenied
from .models import *
from functools import wraps
from django.http import HttpResponseRedirect
from django.http import HttpResponse

def decorator_function(original_function):
    def wrapper_function(*args, **kwargs):
        print('Executed Before', original_function.__name__)
        result = original_function(*args, **kwargs)
        argumentos = args
        print("@"*20)
        print(argumentos)
        print('Executed After', original_function.__name__, '\n')
        return result
    return wrapper_function
