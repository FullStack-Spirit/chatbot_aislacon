from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.db.models.functions import *
from dashboard.models import *
from usuarios.models import Usuario, AgenteUsuario
from notifications.signals import notify
import requests
import json

def fallbackCount(resultado, plataforma, session):
	if plataforma == 'facebook' or plataforma == 'Web Chat' or plataforma == 'whatsapp' or plataforma == 'telegram':
		if resultado == True:
			session_en_turno = Sesion.objects.get(session=session)
			try:
				contador = DefaultFallbackCount.objects.get(session=session_en_turno)
				contador.cantidad = contador.cantidad + 1
				contador.save()
			except:
				new_DefaultFallbackCount = DefaultFallbackCount(
					session = session_en_turno
					)
				new_DefaultFallbackCount.save()

def define_status_sesion(plataforma, session):
	status = 1
	lista_de_usuarios = []
	chat_id = None
	source = Plataforma.objects.get(originalDetectIntentRequest_source=plataforma)
	try:
		session_en_turno = Sesion.objects.get(session=session)
		if session_en_turno.status != 2 and session_en_turno.status != 3:
			contador = DefaultFallbackCount.objects.get(session=session_en_turno)
			limite = session_en_turno.id_proyecto.limite_fallback
			if contador.cantidad < limite:
				status = 1
			else:
				# DEBE SER NOTIFICADA
				# Obtengo el Agente a partir de la Sesion
				agente = Agente.objects.get(nombre_agente = session_en_turno.id_proyecto)
				try:
					usuarios = AgenteUsuario.objects.filter(agente=agente)
					# OPERADORES DEL BOT
					for dato in usuarios:
						lista_de_usuarios.append(dato.user.user)
				except:
					pass
				print("@"*20)
				print(lista_de_usuarios)
				cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
				usuarios_generales = Usuario.objects.filter(cliente=cliente)
				# ADMINISTRADORES DE PARTE DEL CLIENTE
				usuarios = User.objects.filter(id__in=usuarios_generales, groups__name='Cliente Administrador')
				for dato in usuarios:
					lista_de_usuarios.append(dato)
				usuario_system = User.objects.get(username='system')
				the_notification = str(source.id) #id de la platafroma
				if source.originalDetectIntentRequest_source == 'facebook':
					chat_id = session_en_turno.facebook_chat
				elif source.originalDetectIntentRequest_source == 'telegram':
					chat_id = session_en_turno.originalDetectIntentRequest_chat_id
				else:
					chat_id = None
				notify.send(sender=usuario_system,
					recipient=lista_de_usuarios,
					verb=the_notification,
					description='Chat solicita atención humana',
					action_object=agente, # agente de la notificación
					target=session_en_turno, # sesion del agente de la notificacion
					chat_id=chat_id,) # puede ser none
				status = 3 # Notificada
		else:
			status = session_en_turno.status
	except:
		status = 1
	return status
