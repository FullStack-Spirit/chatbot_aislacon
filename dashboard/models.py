#-*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from .utils import *

class Cliente(models.Model):
	nombre_cliente = models.CharField('Nombe o razón social',max_length=600, unique = True)
	logotipo = models.ImageField(upload_to = 'logotipos/', blank = True, null = True)

	def __str__ (self):
		return str (self.nombre_cliente)

class Agente(models.Model):
	cliente = models.ForeignKey(Cliente, related_name='cliente_bot')
	nombre_agente = models.CharField('Nombre (identificador del agente DialogFlow)', max_length=600, unique = True)
	id_proyecto = models.CharField('Id del proyecto en Google Cloud Platform.', max_length=600, unique = True)
	telegram_token = models.CharField('Token de Telegram (solo si aplica)', max_length=600, blank = True, null = True, unique = True)
	fecha_registro = models.DateTimeField(auto_now_add = True)
	facebook_token = models.CharField('Token de Facebook (solo si aplica)', max_length=1200, blank = True, null = True, unique = True)
	limite_fallback = models.IntegerField(blank = False, default=3)
	webchat_token = models.CharField('Token de WebChat (solo si aplica)', max_length=1200, blank = True, null = True, unique = True)
	webchat_key = models.FileField('Key Dialogflow', upload_to = 'keys/', blank = True, null = True)
	api_ws = models.BooleanField(default=False)
	user_wavy = models.CharField('Usuario de API Wavy (solo si aplica)', max_length=1200, blank = True, null = True, unique = True)
	whatsapp_token = models.CharField('Token de wavy (solo si aplica)', max_length=1200, blank = True, null = True, unique = True)

	def __str__ (self):
		return str (self.nombre_agente)

class Sesion(models.Model):
	session = models.CharField(max_length=600)
	id_proyecto = models.ForeignKey(Agente, related_name='agente_sesion')
	originalDetectIntentRequest_chat_id = models.CharField(max_length=600, blank = True, null = True)
	originalDetectIntentRequest_chat_last_name = models.CharField(max_length=600, blank = True, null = True)
	originalDetectIntentRequest_chat_first_name = models.CharField(max_length=600, blank = True, null = True)
	originalDetectIntentRequest_chat_type = models.CharField(max_length=600, blank = True, null = True)
	facebook_chat = models.CharField(max_length=600, blank = True, null = True)
	status = models.IntegerField(blank = False, default=1)

	def __str__ (self):
		return str ((self.id_proyecto.id_proyecto)+" - "+(self.session))

class Plataforma(models.Model):
	originalDetectIntentRequest_source = models.CharField(max_length=600, unique=True)

	def __str__ (self):
		return str (self.originalDetectIntentRequest_source)

class Interaccion(models.Model):
	responseId = models.CharField(max_length=600)
	session = models.ForeignKey(Sesion, related_name='sesion_interaccion')
	originalDetectIntentRequest_source = models.ForeignKey(Plataforma, related_name='plataforma_interaccion')
	queryText = models.CharField(max_length=600)
	allRequiredParamsPresent = models.CharField(max_length=600)
	fulfillmentText = models.CharField(max_length=10000)
	intent_name = models.CharField(max_length=600)
	intent_displayName = models.CharField(max_length=600)
	intentDetectionConfidence = models.CharField(max_length=600)
	languageCode = models.CharField(max_length=600)
	date = models.DateTimeField(auto_now_add = False)
	action = models.CharField(max_length=600, blank = True, null = True)

	def __str__ (self):
		return str ((self.responseId)+" - "+(extrae_nombre(self.session.session)))

class Facebook_card(models.Model):
	responseId = models.ForeignKey(Interaccion, related_name='id_interaccion_card')
	title = models.CharField(max_length=600)
	imageUri = models.CharField(max_length=600, blank = True, null = True)
	subtitle = models.CharField(max_length=600, blank = True, null = True)

	def __str__ (self):
		return '{0} - {1}'.format(self.responseId.session.id_proyecto, self.responseId)

class Buttons_card(models.Model):
	card = models.ForeignKey(Facebook_card, related_name='id_card')
	text = models.CharField(max_length=600)

	def __str__ (self):
		return '{0}'.format(self.card)

class DefaultFallbackCount(models.Model):
	session = models.ForeignKey(Sesion, related_name='sesion_default_count')
	cantidad = models.IntegerField(blank = False, default=1)

	def __str__ (self):
		return '{0} - {1}'.format(extrae_nombre(self.session.session), self.cantidad)

class Skype_card(models.Model):
	responseId = models.ForeignKey(Interaccion, related_name='id_interaccion_card_skype')
	title = models.CharField(max_length=600)
	imageUri = models.CharField(max_length=600, blank = True, null = True)

	def __str__ (self):
		return '{0} - {1}'.format(self.responseId.session.id_proyecto, self.responseId)

class Skype_buttons_card(models.Model):
	skype_card = models.ForeignKey(Skype_card, related_name='id_skype_card')
	text = models.CharField(max_length=600)

	def __str__ (self):
		return '{0}'.format(self.skype_card)

class DatosEmpresa(models.Model):
	nombre_empresa = models.CharField('Nombre del sistema',max_length=600,  blank = True, null = True)
	logotipo = models.ImageField(upload_to = 'logotipos/', blank = True, null = True)

	def __str__ (self):
		return str (self.nombre_empresa)

class AsyncChat(models.Model):
	responseId = models.ForeignKey(Interaccion, related_name='Interaccion_AsyncChat', unique=False)
	id_user = models.ForeignKey(User, related_name='user_asynchat', unique=False)
	date = models.DateTimeField(auto_now_add = False, blank = True, null = True)
	def __str__ (self):
		return str (self.responseId.queryText)

class AsyncSession(models.Model):
	sessionId = models.ForeignKey(Sesion, related_name='Sesion_AsyncSession', unique=False)
	id_user = models.ForeignKey(User, related_name='user_asynsession', unique=False)
	date = models.DateTimeField(auto_now_add = False, blank = True, null = True)
	status = models.IntegerField(blank = False, default = 0)
	def __str__ (self):
		return str (self.sessionId.session)


# class HandoverOperadorText(models.Model):
# 	sesion = models.ForeignKey(Sesion, related_name='sesion_interaccion_handover_operador')
# 	text_operador = models.CharField(max_length=600, blank = True, null = True)
# 	text_operador_timestamp = models.DateTimeField(auto_now_add = False)
# 	operador = models.OneToOneField(User, on_delete=models.CASCADE)

# 	def __str__ (self):
# 		return str (self.sesion)

class Cita(models.Model):
	nombre_usuario = models.CharField(max_length=600, blank = False, null = False)
	email_usuario = models.CharField(max_length=600, blank = False, null = False)
	telefono_usuario = models.CharField(max_length=255, blank = False, null = False)
	fecha = models.DateTimeField(auto_now_add = True)
	estado = models.IntegerField(blank = False, default=0)

	def __str__ (self):
		return str ((self.nombre_usuario)+" - "+(str(self.fecha)))
