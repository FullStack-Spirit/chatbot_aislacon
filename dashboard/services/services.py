# -*- coding: utf-8 -*-
#-------------------------IMPORTS-------------------------#
from dashboard.models import Skype_card, Skype_buttons_card
from dashboard.models import Interaccion, User
from usuarios.models import Usuario, Cliente
from usuarios.forms import ProfileForm, UserForm, ProfileImageForm
from pprint import pprint
#-----------------------------------------------------------------------------------------------------------------------
#----------------------------------------------Servicios para Skype-----------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------
def getBotMessage(json_response):
    '''
    Funcion encargada de obtener la respuesta en formato cadena del bot
    :param json_response: respuesta obtenida en formato json
    :return: bot_message: respuesta del bot en formato cadena | None si no responde el bot
    '''
    if 'queryResult' in json_response and 'fulfillmentText' in json_response['queryResult']:
        bot_message = json_response['queryResult']['fulfillmentText']
    elif 'queryResult' in json_response and 'fulfillmentMessages' in json_response['queryResult']:
        bot_message = json_response['queryResult']['fulfillmentMessages']
    else:
        bot_message = None
    return bot_message
#-----------------------------------------------------------------------------------------------------------------------
def getClientInfo(json_response):
    '''
    Funcion encargada de validar la fuente del bot (skype o telegram)
    obtener su información básica
    :param json_response: respuesta obtenida en formato json
    :return: originalDetectIntentReques: diccionario
    '''
    originalDetectIntentRequest = {}
    # se verifica si existe la clave 'source' en donde viene el tipo de plataforma SKYPE || TELEGRAM
    if 'source' in json_response['originalDetectIntentRequest']:
        if json_response['originalDetectIntentRequest']['source'] == 'skype':
            originalDetectIntentRequest['chat_id'] = json_response['originalDetectIntentRequest']['payload']['data']['address']['bot']['id']
            originalDetectIntentRequest['chat_last_name']  = None
            originalDetectIntentRequest['chat_first_name'] = None
            originalDetectIntentRequest['chat_type'] = None
        elif json_response['originalDetectIntentRequest']['source'] == 'telegram':
            originalDetectIntentRequest['chat_id'] = json_response['originalDetectIntentRequest']['payload']['data']['message']['chat']['id']
            originalDetectIntentRequest['chat_last_name']  = json_response['originalDetectIntentRequest']['payload']['data']['message']['chat']['last_name']
            originalDetectIntentRequest['chat_first_name'] = json_response['originalDetectIntentRequest']['payload']['data']['message']['chat']['first_name']
            originalDetectIntentRequest['chat_type'] = json_response['originalDetectIntentRequest']['payload']['data']['message']['chat']['type']
        else:
            originalDetectIntentRequest = None
    else:
        # la plataforma es whats
        originalDetectIntentRequest['chat_id'] = ''
        originalDetectIntentRequest['chat_last_name'] = ''
        originalDetectIntentRequest['chat_first_name'] = ''
        originalDetectIntentRequest['chat_type'] = ''
    return originalDetectIntentRequest
#-----------------------------------------------------------------------------------------------------------------------
def collectButtonOptions(json_response, interaction):
    response = None
    from pprint import pprint
    pprint(interaction)
    if len(json_response['queryResult']['fulfillmentMessages']) > 0:
        if 'card' in json_response['queryResult']['fulfillmentMessages'][0]:
            title = json_response['queryResult']['fulfillmentMessages'][0]['card']['title'] if 'title' in json_response['queryResult']['fulfillmentMessages'][0]['card'] else 'Sin título'
            imageUri = json_response['queryResult']['fulfillmentMessages'][0]['card']['imageUri'] if json_response['queryResult']['fulfillmentMessages'][0]['card']['imageUri'] else ''
            optionSelected = json_response['queryResult']['queryText']
            #creacion de nueva tarjeta
            interaccion = Interaccion.objects.get(responseId = json_response['responseId'])
            new_card = Skype_card(
                responseId = interaccion,
                title = title,
                imageUri = imageUri
            )
            new_card.save()
            pprint(new_card.save())

            option = Skype_buttons_card(
                skype_card = new_card,
                text = optionSelected
            )
            option.save()

    return response
#-----------------------------------------------------------------------------------------------------------------------
#--------------------------------------------Servicios para usuarios----------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------
def getUserInfoById(idUser = None):
    user = None
    userExt = None
    client = None
    try:
        if idUser:
            user = User.objects.get(pk=idUser)
            userExt = Usuario.objects.get(user=user)
            client = Cliente.objects.get(nombre_cliente=userExt.cliente)
    except Exception as ex:
        print("\nSe ha dedectado una excepcion en getUserInfoById: {}\n".format(ex.__dict__))
    return user, userExt, client
#-----------------------------------------------------------------------------------------------------------------------
def updateUserInfo(request = None, userToEdit = None):
    response = None
    try:
        profile_form = ProfileForm(request.POST)
        if profile_form.is_valid():
            userToEdit.apellido_paterno = profile_form.cleaned_data['apellido_paterno']
            userToEdit.apellido_materno = profile_form.cleaned_data['apellido_materno']
            userToEdit.fecha_nacimiento = profile_form.cleaned_data['fecha_nacimiento']
            userToEdit.direccion = profile_form.cleaned_data['direccion']
            userToEdit.tlf = profile_form.cleaned_data['tlf']
            userToEdit.edad = profile_form.cleaned_data['edad']
            userToEdit.cargo = profile_form.cleaned_data['cargo']
            userToEdit.save()
        response = userToEdit
    except Exception as ex:
        print("\nSe ha dedectado una excepcion en updateUserInfo: {0}".format(type(ex).__name__))
    return response
#-----------------------------------------------------------------------------------------------------------------------
def updateUserImage(request = None, userToEdit = None, idUsuario = None):
    response = None
    try:
        imagePost = ProfileImageForm(request.POST, request.FILES)
        if imagePost.is_valid():
            editions = Usuario.objects.get(pk=idUsuario)
            editions.foto = imagePost.cleaned_data['foto']
            editions.save()
            response = imagePost.cleaned_data['foto']
    except Exception as ex:
        print("\nSe ha dedectado una excepcion en updateUserImage: {0}\n".format(type(ex).__name__))
    return response
#-----------------------------------------------------------------------------------------------------------------------
def updateUserPassword(request = None, userToEdit = None):
    response = None
    try:
        cambio_contrasena = UserForm(request.POST)
        if cambio_contrasena.is_valid():
            if cambio_contrasena.cleaned_data['password'] == "" and \
            cambio_contrasena.cleaned_data['confirm'] == "":
                print("ALGÚN DATO ESTÁ VACIO")
                pass
            # SI los campos del formulario estan llenos, cambia la contraseña
            else: 
                usuario_contrasena = User.objects.get(pk = userToEdit.user.id)
                contrasena = cambio_contrasena.cleaned_data['password']
                confirm = cambio_contrasena.cleaned_data['confirm']
                usuario_contrasena.set_password(contrasena)#se
                usuario_contrasena.save()#se guarda la contraseña
            
        response = usuario_contrasena
    except Exception as ex:
        print("\nSe ha dedectado una excepcion en updateUserPassword: {0}".format(type(ex).__name__))
    return response
#-----------------------------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------------------------------------

