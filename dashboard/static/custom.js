function submit_message(message) {

    $.post( "/send_message", {
        message: message, 
    }, handle_response);
    
    function handle_response(data) {
      // append the bot repsonse to the div
      $('.chat-discussion').append(`
            <div class="chat-message left" id="loading">
            <img class="message-avatar" src="https://cdn3.iconfinder.com/data/icons/faticons/32/user-01-128.png" alt="" >
            <div class="message">
                <a class="message-author" href="#"> Michael Smith </a>
                <span class="message-date"> Mon Jan 26 2015 - 18:39:23 </span>
                <span class="message-content">
                    ${data.message}
                </span>
            </div>
        </div>
      `)
      // remove the loading indicator
      // $( "#loading" ).remove();
    }
}


$('#target').on('submit', function(e){
    e.preventDefault();
    const input_message = $('#input_message').val()
    // return if the user does not enter any text
    if (!input_message) {
      return
    }
    
    // Humano
    $('.chat-discussion').append(`
        <div class="chat-message right">
            <img class="message-avatar" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA1VBMVEX///9GT1UooNeMkJXIzdBETVNGUFXe4eI9z+u6vL9ASlAoodeerbc+SE5CS1JFTlX4+fnBw8Vib3fp6uv19vbU1tc5Q0q23fBMVVvw8PETnNbc3t96gIT3/P7k5ealqaxeZmtTW2HZ7viRlpmpra+boKPq9vtrc3ff8flbY2iFio60t7rH5fRGq9tjuOFyeH16wuWd0es9qNqr2e91wOSMyui6xMuotb4xPERx2vAwx+dkxOd9ipOKl6FXteCwvMM+v+Sl5vSk5vTQ8fl52/A0t+C87fdSScbLAAAdaUlEQVR4nO09C3uiWLIhgrbYPGweivjAIBoxiYnp7sn03c3uzOzd//+TLudU1eEgmJgGTc/9Ut9ujxEV6px6V52qi4sP+IAP+IAP+IAP+IAPeF+4vrm5mrz3Q5wQbu5vd7vb52/L936QE8HkfjS6ZDDaPb73s5wElt8APwb/P1F8hP0DNHt/f0Jd3gyK8LhjmN3ef7vrMUTv3/sB68LN7WgfOHVeZ+z4jaF4e/3ej3gMeF3vwJXJ82UlcPa7Zhd3N2d80J+EcPXkP0WVOE6+klDpXfYuxcvL3hW/fM/eHpz1YX8GpitTURR961Rce+zJG4d/9DK0+MZN7v4eGI51RbEUReuXL13d5ngV4ZldHuyyi78+lS58hYGlb0qXJrf59u1y4Lri7vGGC5rL519dXahrzeIoDkuMOHkgJhx9vZHgDpHmV0e/usp3tmY7Q6+t6ONg/9rNTuxgYZ8GEt2Odr+6+Z0afAMVbV3awitBo/ushihm/46er871pD8JXR9IVDFKXDi5E9bnw/61wfMluzjq3f/q6j6MtEyMZkga49K1R0GId2VCXA7ubnfP325+dRKdrgyFcaFllpVhTom3xwlLV52l2zTxSuz8nhCbyIRzdf/S9Z2QMkdpdLs7tnQGw21Z67wb9E0NMPRnpWv3woj5dsxP2a0hrZZuxb/KNnprk9NopihK1x5zTXgMqzljv91uK/BzilFpAJ4fgrEJctSMOvvXSFH0uJf0KtiJnv2UxX+OvTDiEzzv2yFBErWG3f1LS8GEssXiegXoCJj254xENcMwDZ2jaRxyxs4J6pPCH6ZdxYRCE37LadSOn3wZnnLwtYw8tWHLm6ppZiK1FSO1z4lLJYQRSgZzVbp2I5jwTlIUGwU3/QD4XIbaLZ1tZxSeDZMDYKf4uOa8RFDXwq0fSSaZMz+AYJs0Dnyuwz83LKmfc0Pi43MNS15hzoQ9iQmDrVGFniVekXRxU7aJeom3zwyeJZ6rxDAPwm+4l5hwZr2AHfdNWvDJYPwrYDjd6vBg+rrsMl2SVy+7tuoejVqCOuklUSlSc0XA4IzgMpeJ7cCLLlNPYsKgrSEylcyosUt+4rJPjg32qTJ3nxVm/GnZ2pddpvsql8mNcUky86dfAcxDsZRhq+91MxrNXurbdzXcQtoIP3X3r4nYWsFa2wyR2zSj0h5bgBTSlbkC5O8vTvf4r4OzIiaMSgt9xcmTYfksWWte20QSLUteDiBdwNPkv7x6T4XvxswcZf8r66ylcOt7Utwi4EvCrc7kwJN7kUnC1arUseeE7jBjGuYG6OXHFYpiJLtMM4NJS4ttzUGfQTUMDcWqZhzY6TNB50k7qAlF3ELWhBcbEp/acHr4d510rmuapZnmcPyuvpMz1ulxS89xLYKEcgCNhXIQXhQfrhqv5sP5Ku6/qxi1E6HBSkZHnoQZycFDMg4Uo1WSvEVwA2fqOKeTMXbghGq/u0jiNB2Px2krmW36qtdx5Ofq6mRqlVymyTdBozITkgF7IHNzJnCnXncRb6N1W/d9w9B13dR1wzB8w5qvV2myUUOgHLD7mcyoCHALGv1astba7EvvJh8DdTZezZl/rXGBbgmhzf7NOD/DdRht401oBxTgNrWS+7YU1tqt7DKtTLQ9/eScSBHYQbjYKr5hmoCWBfhZZA4LDdw2TcP315EGn9CGZWtNMOGlHDwkJrTMsvlzesg2b2v6ZpUlrBRcG3ppavhSe0ETXsqFBxsfXQhzVQpWnRyc2XiomQrqYvgP+jNtrtHxpdLGT/C32ha3qUp67WrHTbXL3qhgra1RUbSfzq3Dnf72ydc1sT9tQaOAiQX0SW8XX2rD0n4sd6NqJsQAoXVmJnQ2K0WvokRNYyKUYmGZUDVNrfgJhqteVtyixqmQ7oxJE5rntVKCJMNPUCQ8fIZahhgTmWmmBbuZx6b2N4sk3WZCNntflxBV9LLMGOQuk/RunxCs8JNPCJ1k6O+52ZphDqNxsvGcCnHnOmE3SZk20eFrFYo7T/XKcQvBhIp+RiZ0NpGu7aGnrFobdfqiLLcdr59s5wbblHI0Oq8JupVdplQnt77VPCKHoL/VZOWQkaaxTtSqrSuDHXiLreX75YxCXo4gp3pndKcKyXsqCFPfbAv2szR9vkreGFkONrPDTHh5J72r+grcyzwbE7qzuZ5vXyZb1rHahJmRx9ZkTcjLpCDHcq58ZyfVKZ6XLa3uK4tpM27KVxE8lKw1O9aogGF7noiLO9MNYUwrpjJuTLoBE/aKwcOLmYEImqvzuLPTWJKgmr7tNmYF54pCrrfoaHg7bX4eRaGuJPyMYYNRguUdBPB7RWttTEkYf3YOGrU3ei5ijGjRJNkIl0kuerJjEy13vZxaPAEESVtjdjO7p9lOG0085uUIDxKNdtFXUbTqotqGwR5bgkSNdXMMyEAoitGzhKDIhGrKORRFuPVZjIRnr4Zps3ItD3DLTBgIJjTO4TJ5K8GCerRpmOvvSYzuZE0400gnrc4Qt5hSNYGi+fOmwwh5OYIc4FbnJvnJZ8jEq5mdRrdLXlvQoOOFb4nG5tbarfSuQ7Vb7fYZ0tTeSuyg9hqFuovxfDiMjrfEJ8JaK1THxgbWNr0a4G4AnIgMUf1Vy8JJLVPLwFwfa4M8ULK+J1tr3SGGc8z56eMWrPQTZdrqNbXk8EoQZmqZ/nHckzOhHMH3hKIoV7U3Ds5YxBBeD1UmOnuoiD3fcdmFXFHIcQsb2IJt4xmstTRH8HXeyowCfdV3PJ45O4ZOqw3uhUjCNKx5K8CesWCzlXnZxvj1GIL3hOEw2zpST+cYyrX2m8yDgXrT04fwgeMZQ0SvEl2gZluH1R0ZmRmtIwgsp9LCIdCkTXx4KF/fFHTWJhi/+uu2rzoeMgkDGfaMEyuS1xVwdZujeCnMbjdGBaydWBsGa7TVzNdqGO3+yufVudmqB5lS9C2l4vRSJQzyc4WjOxGhSeh8yZEy+efAFrfRXrlNGCsiutgeJ+PhW8qtru/zsz2ijNTd4i6eNNnUHwI3aBUJhgLMIhatp/iiqevZS/8oIuUwGdyOSrvYIVv/hAlDUbyrlauTZfC2vlBfpok5IuNN9WTLryJh8Uy2mxdhwul01egpLqLxslLarAWBatY2HhqZ2WYMW2+LTy8f8pQF7WKf4mz6iVhRJSZsv/SwbmKKqLux7ju2l6zmUeq9mbIGdLw3L+mOfbI1TmKbOhEEm7X2SyuYuf7C7Vh1a+muPJRBKLpkMOqn8C/sBFO2B2vkGHioTjLdME/qLvSV0P6Ut/AiDcTXKbRiONd47cqLCR8vogoJf+XVNz5EilskgBdPkP6nwuYmgWS18YKq32CBQvYAzdSp5sX5hCIVmfiNW28esFf7paSkShW+xgpY1a79FFfPuIs7NMPJUWw8teaipjCjwzSqkpYwIGli92cztS6OVz1Si/gGnT0wjzcgjoIullOXj1oJUDFUpCkQm+rM0lYrrSdOL3J3ilz+AOnU0hrdxADFtD4+KKWnEdiNlr7gn/GSFoP6Rxqpgp3iUn0sZzHSur8sg0r8fVAVUumdZoKfq7YQamMoTiFQYeIKl9JoMou/0vFIg3hneXUtn+C02bEHJsWHXIi6XUKwnJ1/M4gGERh7I2HTpHmKgjQPNk8Gz73eVymcuUFDxoBA0YYQfGvBQiUQK/bAQLWxIspcN5fuig20uGlDgDfyiK0zBynjw/HFfgr4pbXNGg4TLP0iYaNiJOU1H+54wBokS7iwWLQ7oqi7i+fjwHOziUTjfkMCnRQ/HbVoGeCZvaC63gYb7uO1c0FKlgZ13YCjOW08nimETG1lKIDCxJip8dCNqupb8zOAB2sUn37vSgTD4IbTCKotnzibTgWCzdyew3NRY6AJqW+b+XUVvaGItuR+1CvwBR4t0riecBIk0UYV8tWu4GT00VXVmwnZtHQUk/ndAENsFhaCpIUGFoIJmzUb6UDCCG6JuSHFaETW2HMWb7by+l1ReA36yW4ZoEo4EXsxSNGmq11QYyDZ2Imp7CnoGqDyLWqb5FaLwwEYP+mDyQoxexcQbJE5GjhBQ+IGLRukG9T6zRwIRWUovOoBxRZgC93UhC3kqg814QxttUBV1bAZFFG84SFud6zzk6YvH3g6DtytBgYEESmpihGoihBiqIYsZlL8rOtlGKoNKS0Up3iAdAYdiJrI1KhzjZVzCiNwOYIcLbVd5Ba3BdkJG6013G5AUG0gnMGA+u6AK+ygIdlAmcRC4QWrPhEp1e2iagraEGrn1DIFLkyQRkOOoNpQEP76VmaOC6z1rq/0bdQVT/jUEyJSjPBtfNCV3JrpFlR9oDa5hWid9i7R2U9Q6dcu9UbNo0X4t7DzBcfnXGgXHCYbaLRB0w1vDdJUHULMa1j3Zz3QBTrlb0kZYjmWN+eVez4X2h4XpCkKcAcQbC46vQRZM4I6KVL6fl0Xe4PePZK7KHa5o8uWMOhQzmCECLewyYpFUIko4ig2VjudyKmwra1R5F8DpfQwRBvAbQxZzqBIgi18e7riBQAPY4RFGgtLsjRqANgO4mDnACUputshl2haW/KaYlwL2MJGD0QsC1IcO37oNXsG2T6yIf4MHbTawZ99UBU8NYg2N8mZhsUMB7QXgRE7wIh1+1yET0WTDe9BfhNYdJAwDRJZVUwb58ILvr693Pre8o5nWs1oTd/nCTVqfLekzCzWfX7hNil4FQ4QKd4PiLTh8h7gEer8QT5NPVGzQIWOdslNMYvgDtl5SVxF0BUYewJJ2pSyJ7gq9N+ZoRtcL9EGZKhTUS6ZbHgPzwA25Fe7HMMFIBU0aa8JWELvYFxfFU3Tl4sKXgEbtIFwNNFJo9rdPr8HGuUz2fN1Gtb2APwQBnV7vggxyl4rRYNpEHIsyCgl6xdai2DfOC5o0oKgabrKbgIF4KiMHTh7Ua/4xIH6EgrRLKlaCfOVLd5jFGJwNtf3KSrAsHF1z+FBluQ2WKb11AXqHHKdRFdGFKVgdj9xQeOC74uEGZ5C0FCsnc4EYySjVmEGRrvJ9iPrHq0K1EjgWAVyquk0opQiKCM0iqGs1lzXwhCWiU7oUgsOdCwCOOz4xP9wIEIje07N1yvjEqOLCCSkzeuYhswJs/LqBDq2igEvh59cs3z4g2MYFzBsfA8RQ8yXgKB/sb/Qq6CCd2jib1CY7bkQlzXhj4o9bBxDDH2jVYxRQKuO2u1DfY6GlE4YYgQD5BB62QUMMUTz87I06M/ieFY6z4iiDvMlSRMYKgUMeT+qnoQh40PEsCBpLjr19GE4g3DBbM+qZhiyERYCQ+skGPYIwykGEvgfRW0xrWXThDHlr+IiiriHvdPuYU6lnA8tBSQNanx8olp2KUS04lgSXQjNY4iS5gCGASgk0BZFq82tE0hkXkrc74Qb6QcBKjFUGsCwJEsBQ1Z6zTQ+4LHgGJIr49UQNRtM7bjMmC+Uv5MsBQxjHeKJtbQFxkJIHxa1BVltsIZqKgUxkBF/anUZYmDfsp8slKvcFLQF6sNaR732bJpB0abhlnebbLpQ9oCRTH/K/bZnFHTtlzAEuxQ1PixwPasthGK8kl2KGCYG8/Ghl/RFEBdyvzXCGIxK2dF3nskqUCkuMVptaJfWsrzBLst9i6LlfdGVPWB3IYdLwQf2fqpFBo+HJN1NLEXQAXCsWsG3qGhh9wYI4BgH9Ua/pvQvek8QxaAqlH4hOcpSa16nc/zd7UC0swGxDHmswhLt+YdafQ/YHhd8/H0POICjSFi4E4KSJh09VXl/+2Nvb2eGnkfe5QGNj/UKwscHQV+v0BQjURinofZ3I+ppH5ltSzS45cI94yDxyNC7/8g7hVIEGUOvGUEUv01xGiCh0IA2ffUS3RCxEwfgWci7lxumsABtinZhEp8IM4DxBMfJAcxUwX2wqmqxHzLHIgkUdH2/iWhi15fpUEQTaexeV2cpYEqmg3uRF5pMAcVjWBERRJRgC8tlf9eQYMe748HSmhHhzhMoVfyVK0rMoDCF44hWGzX7rJCbubCnx+7iVJW3EPOQ5b15hKg+xrxTnbesKA/5eBO4UPJAeQuK6tP5h2Cs85kRqLS8VkFhXLg4R+O1fgOEICxNAAtVcVAKE4iUt+AY1u41hBKZ4sqUmaEOeAuMiSNGe5t4EYQ4KuQlieqGBQSRC9MKAVLIPaHvVje7hpaRKLwU2TUUpmjW+ZSPATGfJy0DGodysDTKJhakRBUmWuPyFl6PZHsDD0NWzDF5G8z0gqihDCl1LA7GEDJGnGywa6RuPALFaSWOtuMRgh28vgCJXHGM4XEkz/xbYB6/bpYbSy/pvNo1nX6gjg6YnKLDUGh+93PhIlCswNGdCvxEthhclKot3MvjYxlM7UoFjOtTLQZVKojeP3z4Qjsvue5yk7IrxZDcqTS6xwlsBNfp5OgJa0bUNlbsDLq/xVoMbV0TQSp5EPU0VG0iqthjtAlwzTNBGG+63a6krG2nI8M0ZAOL1AKEJImmaM1UMRd6p1h6jcTVQHNI7MPo4xOLngdEph5cN2kTvdasy0D2CQIZw1AtgaBpm8y1ChqFkyWiJgoEhKLVP4hI0hJ/ic9olcfW2TADzrLooTyOYHdTCNIEOanubV9OoMwhRCasygeg64ZESmmZdf1qDww3idpE0eyPWjhhpCM/waICit0CiplvVLmFodQBdIqqPq08xYCKCmeRYW1iI30ysCCA9MW16K5Czw4nnvIxfm6/CkWGJNvIwvYVxKsjPIqqx5gge6DJtsEUdxOF3upeQQD5iGKmlIODTkQ0ISAU+yWL1A46IZM0YdjZq4+2VXIKq3v3YY869JxcrofbzfQRdsF2EMenKRyVN+LCaYymoFOBYrcyimFXnC2VzoLRskz73dxywAQ7lXyxikFLaZvNdPgkKkQyFY4+xaP40UPOikKJOQLFI2NRmKhgR6XoC+pK0SPxi7iuooIADZq6JhveSin+mjiNIDaRm4hWWypPCkjcdLvh67LAWdAGtjaE4KatS2fJqG3NCG5JpZdaM3Vzzhaq8ddIESIeJWba2Fj9Nf8ssHFzFPuv4DjtxikhKNovwtxCEUejg6RYXI79I5o6FUTFR8LGFX3SRQNjZ8hOnRj/+PxdfMn1ujmOL0xx7XRF2ElSE9N1kfn3wph4FvjYjjevAs5zES7UkuyavBM8swu06LfPn7/nrJ8zY6b/+57j7ksFOwi7rVTsX2shaC5c6wUNRGuKBYN44rO59hh07mcubSIGn0ULoIWiDf/5OQPJIA2kbWRYZmhOMzzB7p56/U3SkiDOG4SGWxQkGC29gcQoyTZkCkVvRs4woDWL9zlRhBUzZ8bffuYgeQW209/kCDIs4iSZMUiSuFWAdJYrFofP81AsA+drCYeGtnCNFluDxR5A9/koLdFLPJ+0aLf+CRh+/i4LlqmQOEnrMMQL6WFV7B5ikph5KAYWaAsbbcSDwwhEBd8yb/Mr6DT412dCUWYPtwM4zl7ET1qULp4VMbbIltRWgSLt4ix3k/W51MhAFyeBSSdKLUad3wjF3wqi03bUwwhmgqYrR/7dBbbQ0um8j1CFFIbGIv2KieR1gOv0zJXf0sPkI5jyVsauQPHzvtHtbPYZrwVsuWcQhGOD38gyt4igaDhASUvV5yUwmtJsiTWdBRaDF4XtJg89dQShfv6+rwHdqZoJT4FnzKZaeXtRRrc7RzM/b1n4sBeidZGctKbbDKlQ0p4XAlKNW49OJjDIefHzbxVLnDmJToe7Fh2nrB4vwhSbh1h+6TYiMIQ9SCpmrtcFCBtK/TZEy+1Cw+bvn3McO28y/DvJEx1CH4qs4U0P+7VSSCHE3gYnaActJJiQ63kPR2k6g+3lKH7+fnxKYTqjBkyKrgjdL5phifEIyC2a1XyXT5uOw4mksjRg+X+kLiDTz2/HsZNEGo3bpAZFF7J5SOJMpX5mzZkzOeCgMyuvHBcrvPvxu9Qw1v0u4/ib+pr7NO2PdUPDiWX6MC++EC2UhIlPXf/q5e4PAh7zl2aaDoBLev/76dN/pDEiduc3CcVonm68QyayG/aTSKHW2Wx+RH4m9EbQCPVOxFPV1gnEDAB2fTfnYgEfQJj++PTpk7yLmQMsENyamq7PV/EmLOkPp5+kkWKI7vyaoc3ypbgqj8xbYCOqiqE7zcB0RdZEjiITdNkWMvhL/mzwHfZxDM+uGb5vRduYjXZU2bTHOF3N2UQref7HWkpWTAY76notoiWej7UJ0cmaX2JzAcUU0pwfzv0BGH76o/DhDmPHfwzzSbWKafLBZPCPbmqWdE3R1y1Jg06+id6XYtRTuIJucCej0QvmP1CzIRFLn3wd7T4R/DkpfHr6/Z98k/JBSeyl+EN6qft+LAukiZgdkOsJzoSW1W6fdDKCjXQqt7p9+CEw/PT7X8XPu7PxXDPL091l0HRlvtf0bCAG5uUkao9RUZy46zzq/bbUR3jy5ycJ/jspfiHwZtuhr2sHsMzodpguinb6kkxRmUTFSTWz1uGDI2DThm47mtTNt4Dif/7a/0ow3aQRZz1d42BpmqmZbKaeFsX9/Rz/zXPvEoWM1NJ7M4S5qpp18hlB1B5dl8o6/yuj+OnPZcXXHDUTn9tVtJ4zWEerbZpsKkJwV/mMeLkt+8JCRVGeU9o8UJdUY5zv4l+/yyj+/sek8pssNcPTo144dSqNkuuH2xy/3jexVH0aFuw30P/tVZhSg01NGgXw138K2/j7PjseBcuHvJM38zvFb3RpMoJxntnvHEW2poaUnpz8UUDxx/Pj8m1ITq4ferxtCoqZu9wlW5BdoJ1rJmcnb+MphchkSv2xG41uv90cj+Py5n43IgHD/vsgKNRdkF2nb08sRnNQaRygGUnmxfLPHwJB/qy7u4cqoVOG64e7vAs7q1y7zWcE2WJogHFqPSGD6BdszmVdjdz471yfsZ18kVwn12z3JPnJtHzOgZnPJnpLn28HGXi0i9GXmSQVJ/8tIAh7cnv/MLiq2szlzeDh623x09nn76WOk3yslMVmnxlHzAtpFLCt9fDLly//kAX/8o9/j/Ye+XJ02ds9390/PA5urq6urzK4GTx++3p3u9vHLtv05yt5NomYGmu8Pk6jaeiMjTZg+GVbqJ1Yyjqt8PAjosaR9LrwkULH0ItpS4zf1U4/faUMbKKOxTH88mVWWODrx+fS5hwBo939QGbZPk6VYsfFm2kT+lZwWoaGGH7Zq0NbDu4uxZBN4q/iH3svR6PdN5k+L4LZUIymbrrv8/GwGba/EMR7q5xpgN3lQejl/zLt0Lv9WlSebnctKFSPzj0+XYL+ai1Q3G72pPnk5uGrkCUvkG3v+X5wXfim7aV59MZ8dWzWSSEcE4rRsL0qRRcmVwOm7YRY6fV6MrLZ+727h5LCdOM1DQLMfO30rGqwAjZbht+ajwXyo6pRlpOrTDU83+52HD9EdHd7m+mPvb3jME0saQOPHtd2QvBSbdjGJTe1cbdSrE+W1zc3g8HjA4fHQaYZl5MqWyecrfP4m6aP35VCCdxEoVFz2aIPV5ufV11hK9Ly+KKhNzvdtAaEqaYrNBvINIbpC/Uzh8HZrExDBHQsfVhV6/1e4G5WfDIaNigwhtvFG5F0+klkoFPGm9xZ24YbhdUFZ9Y25Pi1MR8nR/enCfrxSoruMwm6bnR+cjPgJJEO8TDYBN3w5+NuWJHrlcAOpl6yavsGllIDnSvROcIxPwFTxFHaSt+M0mSjdoJyyZfrhP1Nsp3rRj76mi+NFu3bDr8O2NOZ4uv5ODK2o5quWfNoNW4ls02XVT7zxMyMhRYzJarRoG38tKk/zRe/LH4A/bQYx+fbYpq6wRMyBAbLzGj5J1DVWOuqoyS/GtjebOtDThfHv8JLi1Chd/N3ML1tbWcNNY4+NdiBGq9Z/kw5FjKG9ddxKYf6S0O2k+O1YhQlzwHsjHY0/rvsXgHcsD8bR0NdP4CmlfGnoQ+jdNY/+kz7rweZutvEq7lloHTR2f90HdLAynwVd72/MXY52EFH7S6SOB2Px9vs/2mcLDbe34vtPuADPuADPuADPuADPuAd4P8AnU6ggoQ37rMAAAAASUVORK5CYII=" alt="" >
            <div class="message">
                <a class="message-author" href="#"> Michael Smith </a>
                <span class="message-date"> Mon Jan 26 2015 - 18:39:23 </span>
                <span class="message-content">
                    ${input_message}
                </span>
            </div>
        </div>
    `)
    
    // Loading
    // $('.chat-discussion').append(`
    //     <div class="chat-message left" id="loading">
    //         <img class="message-avatar" src="https://cdn3.iconfinder.com/data/icons/faticons/32/user-01-128.png" alt="" >
    //         <div class="message">
    //             <a class="message-author" href="#"> Michael Smith </a>
    //             <span class="message-date"> Mon Jan 26 2015 - 18:39:23 </span>
    //             <span class="message-content">
    //                 <b>...</b>
    //             </span>
    //         </div>
    //     </div>
    // `)
    
    // clear the text input 
    $('#input_message').val('')
    
    // send the message
    submit_message(input_message)
});
