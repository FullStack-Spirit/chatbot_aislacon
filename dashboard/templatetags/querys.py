#-*- coding: utf-8 -*-
from django.contrib import admin
from django import template
from django.contrib.auth.models import Group
from dashboard.models import *

import calendar
import datetime

from django.utils.html import avoid_wrapping
from django.utils.timezone import is_aware, utc
from django.utils.translation import gettext, ngettext_lazy

import json
import re

TIME_STRINGS = {
	'year': ngettext_lazy('%d year', '%d years'),
	'month': ngettext_lazy('%d month', '%d months'),
	'week': ngettext_lazy('%d week', '%d weeks'),
	'day': ngettext_lazy('%d day', '%d days'),
	'hour': ngettext_lazy('%d hour', '%d hours'),
	'minute': ngettext_lazy('%d minute', '%d minutes'),
}

TIMESINCE_CHUNKS = (
	(60 * 60 * 24 * 365, 'year'),
	(60 * 60 * 24 * 30, 'month'),
	(60 * 60 * 24 * 7, 'week'),
	(60 * 60 * 24, 'day'),
	(60 * 60, 'hour'),
	(60, 'minute'),
)

register = template.Library()
@register.filter(name='has_group')
def has_group(user, group_name):
	try:
		group = Group.objects.get(name=group_name)
	except:
		return False  # group doesn't exist, so for sure the user isn't part of the group

	# for superuser or staff, always return True
	# if user.is_superuser or user.is_staff:
	#     return True

	return user.groups.filter(name=group_name).exists()

@register.assignment_tag
def nombre_plataforma(id_plataforma):
	plataforma = Plataforma.objects.get(pk=id_plataforma)
	if plataforma.originalDetectIntentRequest_source == 'skype':
		plataforma = "Web Chat Sin Handover"
	return plataforma

@register.assignment_tag
def titulo_chat(session_id):
	titulo = ""
	sesion = Sesion.objects.get(pk=session_id)
	seccion_final = ""
	for i in reversed(sesion.session):
		if i != '/':
			seccion_final = i+seccion_final
		else:
			break
	seccion_final = seccion_final.replace('+','')
	seccion_final = re.sub('_', '', seccion_final)
	return seccion_final

@register.assignment_tag
def cliente_agente(session_id):
	sesion = Sesion.objects.get(pk=session_id)
	cliente = sesion.id_proyecto.cliente
	return cliente

@register.assignment_tag
def hora_inicial(session_id):
	sesion = Sesion.objects.get(pk=session_id)
	interaccion = Interaccion.objects.filter(session=sesion)
	for dato in interaccion:
		inicio = dato.date
		break
	return inicio

@register.assignment_tag
def hora_final(session_id):
	sesion = Sesion.objects.get(pk=session_id)
	interaccion = Interaccion.objects.filter(session=sesion)
	conteo = interaccion.count()
	fin = interaccion[conteo-1].date
	return fin

@register.assignment_tag
def nombre_agente(id_agente):
	return Agente.objects.get(pk=id_agente)

@register.assignment_tag
def timesince(d, now=None, reversed=False, time_strings=None):
	"""
	Take two datetime objects and return the time between d and now as a nicely
	formatted string, e.g. "10 minutes". If d occurs after now, return
	"0 minutes".
	Units used are years, months, weeks, days, hours, and minutes.
	Seconds and microseconds are ignored.  Up to two adjacent units will be
	displayed.  For example, "2 weeks, 3 days" and "1 year, 3 months" are
	possible outputs, but "2 weeks, 3 hours" and "1 year, 5 days" are not.
	`time_strings` is an optional dict of strings to replace the default
	TIME_STRINGS dict.
	Adapted from
	https://web.archive.org/web/20060617175230/http://blog.natbat.co.uk/archive/2003/Jun/14/time_since
	"""
	if time_strings is None:
		time_strings = TIME_STRINGS

	# Convert datetime.date to datetime.datetime for comparison.
	if not isinstance(d, datetime.datetime):
		d = datetime.datetime(d.year, d.month, d.day)
	if now and not isinstance(now, datetime.datetime):
		now = datetime.datetime(now.year, now.month, now.day)

	now = now or datetime.datetime.now(utc if is_aware(d) else None)

	if reversed:
		d, now = now, d
	delta = now - d

	# Deal with leapyears by subtracing the number of leapdays
	leapdays = calendar.leapdays(d.year, now.year)
	if leapdays != 0:
		if calendar.isleap(d.year):
			leapdays -= 1
		elif calendar.isleap(now.year):
			leapdays += 1
	delta -= datetime.timedelta(leapdays)

	# ignore microseconds
	since = delta.days * 24 * 60 * 60 + delta.seconds
	if since <= 0:
		# d is in the future compared to now, stop processing.
		return avoid_wrapping(gettext('0 minutes'))
	for i, (seconds, name) in enumerate(TIMESINCE_CHUNKS):
		count = since // seconds
		if count != 0:
			break
	result = avoid_wrapping(time_strings[name] % count)
	if i + 1 < len(TIMESINCE_CHUNKS):
		# Now get the second item
		seconds2, name2 = TIMESINCE_CHUNKS[i + 1]
		count2 = (since - (seconds * count)) // seconds2
		if count2 != 0:
			result += gettext(', ') + avoid_wrapping(time_strings[name2] % count2)
	return result

@register.assignment_tag
def chat_id(json_data):
	chat=""
	for key, value in json_data.items():
		if key == "chat_id":
			chat = value
	return chat

@register.assignment_tag
def generador_url(objeto, plataforma):
	url = ""
	if plataforma == "facebook":
		# url 'facebook_room' agente.id plataforma.id sesion chat
		url = "/facebook_room/"+objeto.action_object_object_id+"/"+objeto.verb+"/"+objeto.target_object_id+"/"+chat_id(objeto.data)
	elif plataforma == "whatsapp":
		# https://api.whatsapp.com/send?phone={{titulo}}
		agente = Agente.objects.get(pk = objeto.action_object_object_id)
		if agente.api_ws == True:
			url = "/whatsapp_room/"+objeto.action_object_object_id+"/"+objeto.verb+"/"+objeto.target_object_id
		else:
			url = "https://api.whatsapp.com/send?phone="+titulo_chat(objeto.target_object_id)
	elif plataforma == "telegram":
		# https://api.whatsapp.com/send?phone={{titulo}}
		url = "/telegram_room/"+objeto.action_object_object_id+"/"+objeto.verb+"/"+objeto.target_object_id+"/"+chat_id(objeto.data)
	return url

@register.assignment_tag
def nombre_usuario_session(session_id):
	nombre = ""
	sesion = Sesion.objects.get(pk=session_id)
	if sesion.originalDetectIntentRequest_chat_first_name != None:
		if sesion.originalDetectIntentRequest_chat_last_name != None:
			nombre = ""+str(sesion.originalDetectIntentRequest_chat_first_name)+" "+str(sesion.originalDetectIntentRequest_chat_last_name)
		else:
			nombre = ""+str(sesion.originalDetectIntentRequest_chat_first_name)
	else:
		nombre = "No Definido"
	return nombre
