#-*- coding: utf-8 -*-
from django.contrib import admin
import json
import re

def extrae_nombre(sesion):
	nombre_bot = sesion
	nombre_bot_final = ""
	pos1=-1
	pos2=-1
	x=0
	for x in nombre_bot:
		if pos1 == 0 and pos2 < 0:
			if not x == '/':
				nombre_bot_final = nombre_bot_final + x
		if x == '/':
			if pos1 < 0 and pos2 < 0:
				pos1 = 0
			elif pos1 == 0 and pos2 < 0:
				pos2 = 0
			else:
				break
	return nombre_bot_final

def extrae_agente(intent_displayName):
	agente = ""
	lon = len(intent_displayName)
	print(lon)
	for i in reversed(intent_displayName):
		if i != '/':
			agente = i+agente
		else:
			break
	return agente

def identifica_source(session):
	source = "unknown"
	seccion_final = ""
	for i in reversed(session):
		if i != '/':
			seccion_final = i+seccion_final
		else:
			break
	if seccion_final[0] == '+':
		source = 'whatsapp'
	else:
		source = "Web Chat"
	return source

def session_uuid(session):
	titulo = ""
	seccion_final = ""
	for i in reversed(session):
		if i != '/':
			seccion_final = i+seccion_final
		else:
			break
	seccion_final = seccion_final.replace('+','')
	seccion_final = re.sub('_', '', seccion_final)
	return seccion_final
