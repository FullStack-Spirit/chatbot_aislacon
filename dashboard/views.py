# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.core.mail import BadHeaderError, send_mail
from .models import *
import datetime
from datetime import *
from django.db import IntegrityError
import os
import dialogflow
import requests
import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.utils import timezone
from django.db.models import *
from .utils import *
from django.db.models.functions import Lower
from usuarios.models import *
from .forms import *
from django.core.exceptions import *
from django.db.models import Q
import time
from django.db.models.functions import *
from django.contrib.admin.views.decorators import staff_member_required
from dashboard.management.commands.funciones import fallbackCount, define_status_sesion
from notifications.signals import notify
from notifications.models import *
from django.views.decorators.csrf import csrf_exempt
from operator import itemgetter
from .decorators import *
import telebot
from dashboard.templatetags.querys import titulo_chat
from pymessenger.bot import Bot
from estadisticas.models import *
# Importamos la función de prueba de graficas
from estadisticas.servicios.graficas import *
# Create your views here.
import uuid
import psycopg2
from django.db import connection
import simplejson as json
import base64
import binascii
import pytz
from chatbot.views import bot_request



def index(request, template_name = "dashboard/index_login.html"):
	datos_empresa = DatosEmpresa.objects.first()
	if request.method == 'POST':
		form = ActivaEmpleado(request.POST)
		if form.is_valid():
			correo = request.POST['email']
			usuario = request.POST['username']
			contrasena = form.cleaned_data['password']
			# Consultar si existe correo
			try:
				usuario_inactivo = User.objects.get(Q(email = correo) & Q(username = usuario))
				if usuario_inactivo:
					# Verificar su status (ACTIVADO O NO ACTIVADO)
					if usuario_inactivo.is_active:
						mensaje = "El usuario ya se encuentra activo"
					else:
						valid = usuario_inactivo.check_password(contrasena)
						if not valid:
							mensaje = "La contraseña es incorrecta"
						else:
							usuario_inactivo.is_active = True
							usuario_inactivo.save()
							exito = "El usuario ha sido activado"
			except:
				mensaje = "El email o nombre de usuario son incorrectos"
		else:
			print(form.errors)
	else:
		form = ActivaEmpleado()

	return render(request, template_name, locals(),)

def login_view(request):
	datos_empresa = DatosEmpresa.objects.first()
	state = ""
	username = ""
	password = ""
	next = ""
	mensaje = ""

	if request.GET:
		next = request.GET['next']

	if request.POST:
		username = request.POST['username']
		password = request.POST['password']

		try:
			if '@' in username:
				check = User.objects.get(email=username)
			else:
				check = User.objects.get(username=username)
			username = check.username
			user = authenticate(username=username, password=password)
			if user is not None:
				if user.is_active:
					login(request, user)
					if next == "":
						return HttpResponseRedirect('/home/')
					else:
						return HttpResponseRedirect(next)
			else:
				mensaje = "Este usuario debe activarse"
			valid = check.check_password(password)
			if not valid:
				mensaje = "La contraseña es incorrecta"
		except ObjectDoesNotExist:
			if '@' in username:
				mensaje = "El email no existe"
			else:
				mensaje = "El nombre de usuario no existe"



	return render(request, 'dashboard/login.html', {'mensaje':mensaje, 'username': username, 'datos_empresa': datos_empresa, 'next': next,},)

@login_required(login_url = '/login/')
def login_out(request):
	logout(request)
	return HttpResponseRedirect('/')

@login_required(login_url = '/login/')
def home(request, template_name = "dashboard/dashboard.html"):
	# Verificamos si el usuario tiene foto, Si existe algún registro en
	# Tabla usuarios que referencie al usuario en turno y si este posee
	# foto cargada al sistema
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		clientes = Cliente.objects.all()
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0

	# === INTERACCIONES POR DÍA ===
	# CLIENTE ESPECIFICO DEL USUARIO EN TURNO
	if User.objects.filter(groups__name='Administrador', id=usuario.id).count() > 0:
		pass
	else:
		cliente = Cliente.objects.get(pk=extras.cliente.id)

	if User.objects.filter(groups__name='Cliente Administrador', id=usuario.id).count() > 0:

		# === USUARIOS NO ADMINISTRADORES ===
		userNo_admins = User.objects.filter(groups__name='Usuario Cliente')
		# === USUARIOS DEL CLIENTE ===
		usuarios = Usuario.objects.filter(cliente=cliente, user__in=userNo_admins)
		# BOTS DEL CLIENTE
		agentes = Agente.objects.filter(cliente=cliente)
		# SESIONES DE TODOS AGENTES DEL CLIENTE
		sesiones = Sesion.objects.filter(id_proyecto__in=agentes)
		if sesiones.count() > 0:

			# Obtiene las graficas que quiere el cliente en el dashboard
			graficas_solicitadas = GraficaCliente.objects.all().filter(cliente = cliente)
			# Recorre las graficas que quiere el usuario
			for graph in graficas_solicitadas:
				grafica = Grafica.objects.get(pk = graph.grafica.pk)
				"""nombre = str(grafica.nombre_funcion) + "(" + str(extras.pk) + "," + str(cliente.pk) + ")"
				print(nombre)
				intents_totales = eval(nombre)
				print(intents_totales)"""

				# Si quiere la gráfica de intenciones frecuentes
				if grafica.nombre_funcion == "intenciones_frecuentes":
					intents_totales = intenciones_frecuentes(extras.pk,cliente.pk, agentes, sesiones)

				# Si quiere la gráfica de frases no reconocidas
				elif grafica.nombre_funcion == "frases_no_reconocidas":
					frases_noReconocidas = frases_no_reconocidas(extras.pk,cliente.pk, agentes, sesiones)

				# Si quiere la gráfica de interacciones por hora
				elif grafica.nombre_funcion == "interacciones_por_hora":
					interacciones_hora = interacciones_por_hora(extras.pk,cliente.pk, agentes, sesiones)

					# SESIONES DEL DÍA
					sesiones_del_dia = Interaccion.objects.values('session_id').filter(date__date=date.today(),session__in=sesiones).distinct().count()
					x = interacciones_hora.aggregate(Avg('total'))

				elif grafica.nombre_funcion == "estadisticas_7_dias":
					estadisticas_return = estadisticas_7_dias(extras.pk,cliente.pk, agentes, sesiones)
					ult = estadisticas_return[0]

					interacciones_totales = estadisticas_return[1]

					fecha6 = estadisticas_return[2]
					fecha5 = estadisticas_return[3]
					fecha4 = estadisticas_return[4]
					fecha3 = estadisticas_return[5]
					fecha2 = estadisticas_return[6]
					fecha1 = estadisticas_return[7]
					fecha7 = estadisticas_return[8] - timedelta(days=1)

					nro_interacciones_7_dias = estadisticas_return[9]

					nro_interacciones1 = estadisticas_return[10]
					nro_interacciones2 = estadisticas_return[11]
					nro_interacciones3 = estadisticas_return[12]
					nro_interacciones4 = estadisticas_return[13]
					nro_interacciones5 = estadisticas_return[14]
					nro_interacciones6 = estadisticas_return[15]
					nro_interacciones7 = estadisticas_return[16]

					nro_sesiones_sesiones_7_dias = estadisticas_return[17]

					nro_sesiones1 = estadisticas_return[18]
					nro_sesiones2 = estadisticas_return[19]
					nro_sesiones3 = estadisticas_return[20]
					nro_sesiones4 = estadisticas_return[21]
					nro_sesiones5 = estadisticas_return[22]
					nro_sesiones6 = estadisticas_return[23]
					nro_sesiones7 = estadisticas_return[24]
					nro_sesionest = estadisticas_return[25]

		else:
			mensaje_sin_estadisticas = "No hay estadísticas que mostrar"

	elif User.objects.filter(groups__name='Usuario Cliente', id=usuario.id).count() > 0:

		usuario_real = Usuario.objects.get(user = usuario)
		try:
			agentes_usuario_real = AgenteUsuario.objects.filter(user=usuario_real)
			lista_de_id_agente = []
			for dato in agentes_usuario_real:
				lista_de_id_agente.append(dato.agente.id_proyecto)
			agentes_finales = Agente.objects.filter(id_proyecto__in=lista_de_id_agente)

			# SESIONES DE TODOS AGENTES DEL USUARIO
			sesiones_por_usuario = Sesion.objects.filter(id_proyecto__in=agentes_finales)

			# Obtiene las graficas que quiere el cliente en el dashboard
			graficas_solicitadas = GraficaCliente.objects.all().filter(cliente = cliente)
			# Recorre las graficas que quiere el usuario
			for graph in graficas_solicitadas:
				grafica = Grafica.objects.get(pk = graph.grafica.pk)
				print("YA ESTA EN FOR")
				# Si quiere la gráfica de intenciones frecuentes
				if grafica.nombre_funcion == "intenciones_frecuentes":
					intents_totales_por_usuario = intenciones_frecuentes(extras.pk,cliente.pk, agentes_finales, sesiones_por_usuario)
					# Si quiere la gráfica de frases no reconocidas
				elif grafica.nombre_funcion == "frases_no_reconocidas":
					frases_noReconocidas_por_usuario = frases_no_reconocidas(extras.pk,cliente.pk, agentes_finales, sesiones_por_usuario)
				# Si quiere la gráfica de interacciones por hora
				elif grafica.nombre_funcion == "interacciones_por_hora":
					interacciones_hora_por_usuario = interacciones_por_hora(extras.pk,cliente.pk, agentes_finales, sesiones_por_usuario)
					print("\n\n\n INTERACCIONES POR HORA POR USUARIO")
					print(interacciones_hora_por_usuario)
					print("\n\n\n")
					# SESIONES DEL DÍA
					sesiones_del_dia_por_usuario = Interaccion.objects.values('session_id').filter(date__date=date.today(),session__in=sesiones_por_usuario).distinct().count()
					x_por_usuario = interacciones_hora_por_usuario.aggregate(Avg('total'))


				elif grafica.nombre_funcion == "estadisticas_7_dias":
					estadisticas_return = estadisticas_7_dias(extras.pk,cliente.pk, agentes_finales, sesiones_por_usuario)
					ult = estadisticas_return[0]

					interacciones_totales_por_usuario = estadisticas_return[1]

					fecha6_por_usuario = estadisticas_return[2]
					fecha5_por_usuario = estadisticas_return[3]
					fecha4_por_usuario = estadisticas_return[4]
					fecha3_por_usuario = estadisticas_return[5]
					fecha2_por_usuario = estadisticas_return[6]
					fecha1_por_usuario = estadisticas_return[7]
					fecha7_por_usuario = estadisticas_return[8]

					nro_interacciones_7_dias_por_usuario = estadisticas_return[9]

					nro_interacciones1_por_usuario = estadisticas_return[10]
					nro_interacciones2_por_usuario = estadisticas_return[11]
					nro_interacciones3_por_usuario = estadisticas_return[12]
					nro_interacciones4_por_usuario = estadisticas_return[13]
					nro_interacciones5_por_usuario = estadisticas_return[14]
					nro_interacciones6_por_usuario = estadisticas_return[15]
					nro_interacciones7_por_usuario = estadisticas_return[16]

					nro_sesiones_sesiones_7_dias_por_usuario = estadisticas_return[17]

					nro_sesiones1_por_usuario = estadisticas_return[18]
					nro_sesiones2_por_usuario = estadisticas_return[19]
					nro_sesiones3_por_usuario = estadisticas_return[20]
					nro_sesiones4_por_usuario = estadisticas_return[21]
					nro_sesiones5_por_usuario = estadisticas_return[22]
					nro_sesiones6_por_usuario = estadisticas_return[23]
					nro_sesiones7_por_usuario = estadisticas_return[24]
					nro_sesionest_por_usuario = estadisticas_return[25]

		except:
			mensaje_sin_estadisticas = "No hay estadísticas que mostrar, el usuario no tiene bots asignados"

	return render(request, template_name, locals(),)

def activar(request, template_name = "dashboard/activar.html"):
	datos_empresa = DatosEmpresa.objects.first()
	if request.method == 'POST':
		form = ActivaEmpleado(request.POST)
		if form.is_valid():
			correo = request.POST['email']
			usuario = request.POST['username']
			contrasena = form.cleaned_data['password']
			# Consultar si existe correo
			try:
				usuario_inactivo = User.objects.get(Q(email = correo) & Q(username = usuario))
				if usuario_inactivo:
					# Verificar su status (ACTIVADO O NO ACTIVADO)
					if usuario_inactivo.is_active:
						mensaje = "El usuario ya se encuentra activo"
					else:
						valid = usuario_inactivo.check_password(contrasena)
						if not valid:
							mensaje = "La contraseña es incorrecta"
						else:
							usuario_inactivo.is_active = True
							usuario_inactivo.save()
							exito = "El usuario ha sido activado"
			except:
				mensaje = "El email o nombre de usuario son incorrectos"
		else:
			print(form.errors)
	else:
		form = ActivaEmpleado()
	return render(request, template_name, locals(),)

@csrf_exempt
def get_request_detail(request):
	from dashboard.services.services import getBotMessage, getClientInfo, collectButtonOptions
	originalDetectIntentRequest_chat_id=None
	originalDetectIntentRequest_chat_last_name=None
	originalDetectIntentRequest_chat_first_name=None
	originalDetectIntentRequest_chat_type=None
	originalDetectIntentRequest_source = 'Web Chat'
	# Tarjetas de facebook
	title = None
	action = None
	imageUri = None
	card = None
	text = None
	subtitle = None
	facebook_chat = None
	print("datadatadatadatadatadata")
	if request.method == 'POST':
		data = json.loads(request.body)
		print("\n\n\n INICIO JSON DIALOGFLOW")
		print(data)
		
		
		reply = bot_request(data)
		
		
		print("FIN JSON DIALOGFLOW\n\n\n")
		print("aqui llegó la respuesta de todo el pedo \n")
		webapi = ""
		try:
			print(data)
			webapi = data['plataforma']
			if webapi == 'webchatapi':
				if webchat_catcher(data['queryText'], data['session']):
					return HttpResponse(status=200)
		except:
			print("No hay uso de api para webchat")
		responseId = data['responseId']
		queryText = data['queryResult']['queryText']
		allRequiredParamsPresent = data['queryResult']['allRequiredParamsPresent']
		try:
			fulfillmentText = data['queryResult']['fulfillmentText']
		except:
			try:
				fulfillmentText = data['queryResult']['fulfillmentMessages'][0]['text']['text'][0]
			except:
				print("Excepcion seccción 8")
		"""
		if SiNo != 1:
			
		"""
		intent_name = data['queryResult']['intent']['name']
		intent_displayName = data['queryResult']['intent']['displayName']
		intentDetectionConfidence = data['queryResult']['intentDetectionConfidence']
		languageCode = data['queryResult']['languageCode']
		session = data['session']

		try:
			facebook_chat = data['originalDetectIntentRequest']['payload']['data']['sender']['id']
		except:
			print("No hay data para facebook (facebook_chat)")

		try:
			originalDetectIntentRequest_chat_id = data['originalDetectIntentRequest']['payload']['chat']['id']
		except:
			print("No chat_id")
		try:
			originalDetectIntentRequest_chat_last_name = data['originalDetectIntentRequest']['payload']['from']['last_name']
		except:
			print("No chat_last_name")
		try:
			originalDetectIntentRequest_chat_first_name = data['originalDetectIntentRequest']['payload']['from']['first_name']
		except:
			print("No chat_first_name_telegram")
		try:
			originalDetectIntentRequest_chat_type = data['originalDetectIntentRequest']['payload']['chat']['type']
		except:
			print("No chat_type")
		try:
			originalDetectIntentRequest_chat_first_name = data['originalDetectIntentRequest']['payload']['ws_username']
		except:
			print("No chat_first_name_whatsapp")
		try:
			action = data['queryResult']['action']
		except:
			print("No action")

		#clienteInfo = getClientInfo(data) # Devuelve None para facebook

		try:
			originalDetectIntentRequest_source = data['originalDetectIntentRequest']['source']
		except:
			originalDetectIntentRequest_source = identifica_source(session)

		try:
			if originalDetectIntentRequest_source == "telegram":
				if telegram_sesion(originalDetectIntentRequest_chat_id) != "":
					session = telegram_sesion(originalDetectIntentRequest_chat_id)
		except:
			print("Excepción al guardar sesion de telegram")

		# ========== SALVAR REGISTROS ==========
		# 1.- REGISTRAMOS LA SESION
		if registra_sesion(session) == False:
			nueva_sesion = Sesion(
				session = session,
				id_proyecto = Agente.objects.get(id_proyecto=extrae_nombre(session)),
				originalDetectIntentRequest_chat_id=originalDetectIntentRequest_chat_id,
				originalDetectIntentRequest_chat_last_name=originalDetectIntentRequest_chat_last_name,
				originalDetectIntentRequest_chat_first_name=originalDetectIntentRequest_chat_first_name,
				originalDetectIntentRequest_chat_type=originalDetectIntentRequest_chat_type,
				facebook_chat = facebook_chat,
				)
			nueva_sesion.save()

		# CONSULTAMOS LA SESSION EN TURNO
		session_en_turno = Sesion.objects.get(session=session)
		
		# IS FALLBACK COUNT
		try:
			resultado = intent_name = data['queryResult']['intent']['isFallback']
			fallbackCount(resultado, originalDetectIntentRequest_source, session)
		except:
			print("La intención NO es DEFAULTFALLBACK")


		# 2.- REGISTRAMOS LA PLATAFORMA
		try:
			nueva_plataforma = Plataforma(
				originalDetectIntentRequest_source=originalDetectIntentRequest_source,
				)
			nueva_plataforma.save()
		except:
			print("Excepcion secccion 4 - error en sesion - plataforma")

		# DEFINIMOS STATUS DE LA SESION Y NOTIFICACIÓN
		# 1.- NORMAL
		# 2.- EN HANDOVER
		# 3.- ES ESPERA DE HANDOVER Y CON NOTIFICACIÓN
		session_en_turno.status = define_status_sesion(originalDetectIntentRequest_source, session)
		session_en_turno.save()

		# 3.- REGISTRAMOS LAS INTERACCIONES POR SESION
		if session_en_turno.status == 2:
			fulfillmentText = ""

		try:

			if Sesion.objects.get(session=session):
				utc=pytz.UTC

				nueva_interaccion = Interaccion(
					responseId = data['responseId'],
					queryText = data['queryResult']['queryText'],
					allRequiredParamsPresent = data['queryResult']['allRequiredParamsPresent'],
					fulfillmentText = fulfillmentText,
					intent_name = data['queryResult']['intent']['name'],
					intent_displayName = data['queryResult']['intent']['displayName'],
					intentDetectionConfidence = data['queryResult']['intentDetectionConfidence'],
					languageCode = data['queryResult']['languageCode'],
					session = Sesion.objects.get(session=session),
					originalDetectIntentRequest_source = Plataforma.objects.get(originalDetectIntentRequest_source=originalDetectIntentRequest_source),
					date=datetime.now(),
					action=action,
					)

				nueva_interaccion.save()

		except:
			print("Excepcion seccion 5 - NO se logro guardar la interaccion")

		# 3.1 Skype buttons
		try:
			collectButtonOptions(data, nueva_interaccion)
		except:
			pass
		# 4.- SOLO SI TIENE TARJETA DE FACEBOOK
		try:
			# ALMACENAMOS LA TARJETA ID
			if data['queryResult']['fulfillmentMessages'][0]['platform'] == 'FACEBOOK':
				try:
					title = data['queryResult']['fulfillmentMessages'][1]['card']['title']
					try:
						imageUri = data['queryResult']['fulfillmentMessages'][1]['card']['imageUri']
					except:
						print("Tarjeta sin imagen")
					try:
						subtitle = data['queryResult']['fulfillmentMessages'][1]['card']['subtitle']
					except:
						print("Tarjeta sin subtitulo")
					nueva_tarjeta = Facebook_card(
						responseId = Interaccion.objects.get(responseId = data['responseId']),
						title = title,
						imageUri = imageUri,
						subtitle = subtitle
					)
					nueva_tarjeta.save()
					# ALMACENAMOS LOS BOTONES EN CASO DE HABERLOS
					if data['queryResult']['fulfillmentMessages'][1]['card']['buttons']:
						botones = data['queryResult']['fulfillmentMessages'][1]['card']['buttons']
						id_response = Interaccion.objects.get(responseId = data['responseId'])
						id_response = id_response.id
						for boton in botones:
							nuevo_boton = Buttons_card(
								card = Facebook_card.objects.get(responseId = id_response),
								text = boton['text']
								)
							nuevo_boton.save()
				except:
					print("Excepción sección 9")
				try:
					title = data['queryResult']['fulfillmentMessages'][0]['card']['title']
					try:
						imageUri = data['queryResult']['fulfillmentMessages'][0]['card']['imageUri']
					except:
						print("Tarjeta sin imagen")
					try:
						subtitle = data['queryResult']['fulfillmentMessages'][0]['card']['subtitle']
					except:
						print("Tarjeta sin subtitulo")
					nueva_tarjeta = Facebook_card(
						responseId = Interaccion.objects.get(responseId = data['responseId']),
						title = title,
						imageUri = imageUri,
						subtitle = subtitle
					)
					nueva_tarjeta.save()
					# ALMACENAMOS LOS BOTONES EN CASO DE HABERLOS
					if data['queryResult']['fulfillmentMessages'][0]['card']['buttons']:
						botones = data['queryResult']['fulfillmentMessages'][0]['card']['buttons']
						id_response = Interaccion.objects.get(responseId = data['responseId'])
						id_response = id_response.id
						for boton in botones:
							nuevo_boton = Buttons_card(
								card = Facebook_card.objects.get(responseId = id_response),
								text = boton['text']
								)
							nuevo_boton.save()
				except:
					print("Excepción sección 10")

				try:
					# ALMACENAMOS LOS BOTONES EN CASO DE HABERLOS
					if data['queryResult']['fulfillmentMessages'][1]['quickReplies']['quickReplies']:
						title = "Opciones rápidas"
						imageUri = "Ninguna"
						nueva_tarjeta = Facebook_card(
							responseId = Interaccion.objects.get(responseId = data['responseId']),
							title = title,
							imageUri = imageUri,
							subtitle = subtitle
						)
						nueva_tarjeta.save()
						botones = data['queryResult']['fulfillmentMessages'][1]['quickReplies']['quickReplies']
						id_response = Interaccion.objects.get(responseId = data['responseId'])
						id_response = id_response.id
						for boton in botones:
							nuevo_boton = Buttons_card(
								card = Facebook_card.objects.get(responseId = id_response),
								text = boton
								)
							nuevo_boton.save()
				except:
					print("Excepción sección 11")
		except:
			print("Excepcion secccion 7")

		# ESTÁ TOMADA POR HANDOVER
		if session_en_turno.status == 2:
			if originalDetectIntentRequest_source == 'whatsapp':
				reply = {
					"fulfillmentText": "Procesando, en un momento obtendrá la respuesta del asesor...",
					'fulfillmentMessages': [{'text': {'text': ["Procesando, en un momento obtendrá la respuesta del asesor..."]}}],
				}
			else:
				reply = {
					"fulfillmentText": "",
					'fulfillmentMessages': [{'text': {'text': [""]}}],
				}

			# handover_interaccion_user = HandoverUserText(
			# 	sesion = Sesion.objects.get(session=session),
			# 	text_user = data['queryResult']['queryText'],
			# 	text_user_timestamp=timezone.now(),
			# 	)
			# handover_interaccion_user.save()

		try:
			return JsonResponse(reply)
		except:
			pass  
		#Se agregarán las citas del bot SAITO cuando entre a intent "Ventas y precios"
		if data['queryResult']['intent']['displayName'] == 'Ventas.y.precios':
			nombre_user = data['queryResult']['parameters']['user']
			email_user = data['queryResult']['parameters']['email']
			number_user = data['queryResult']['parameters']['number']
			number_user_int = int(number_user)
			nuevaCita = Cita(nombre_usuario = nombre_user,\
				email_usuario = email_user,\
				telefono_usuario = int(number_user))
			nuevaCita.save()
			print("SE GUARDO SATISFACTORIAMENTE LA CITA")
	return HttpResponse('')

# MANEJO DE BLOQUES POR TRY CATCH
# ESTA FUNCIÓN DEBE DEVOLVER FALSE PARA LOS CASOS DE INSERCIÓN
def registra_sesion(sesion):
	# DETERMINAMOS EL NOMBRE DEL AGENTE "id_proyecto"
	id_proyecto = extrae_nombre(sesion)
	try:
		# VERIFICAMOS SI ESE ID DE PROYECTO ESTÁ REGISTRADO EN EL SISTEMA
		if Agente.objects.get(id_proyecto=id_proyecto):
			try:
				# VERIFICAMOS SI LA SESION EXISTE O NO
				if Sesion.objects.get(session=sesion):
					return True
			except:
				return False
	except:
		return True
	return True

@login_required(login_url = '/login/')
def listar_agentes(request, id_cliente, template_name = "dashboard/agentes.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	cliente = Cliente.objects.get(pk=id_cliente)
	agentes = Agente.objects.filter(cliente=cliente)
	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
def listar_interacciones(request, id_agente, template_name = "dashboard/interacciones.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
	# SESIONES DEL AGENTE
	sesiones_del_agente = Sesion.objects.filter(id_proyecto=Agente.objects.get(pk=id_agente))

	sesiones_order_by_date_interaccion = Interaccion.objects.filter(session__in=sesiones_del_agente)\
	.order_by('-date')\
	.values('session')

	if sesiones_del_agente.count() > 0:
		# INTERACCIONES TOTALES DEL AGENTE
		interacciones_totales = Interaccion.objects.filter(session__in=sesiones_del_agente).count()
		# PLATAFORMAS DEL AGENTE
		plataformas_del_agente = Interaccion.objects.filter(session__in=sesiones_del_agente)\
		.values('originalDetectIntentRequest_source').distinct()
		# SESION/PLATAFORMA/INTERACCIONES
		sesion_plataforma_interaccion = Interaccion.objects.filter(session__in=sesiones_del_agente)\
		.values('session','originalDetectIntentRequest_source')\
		.annotate(total_sesiones=(Count('session', distinct=True)))\
		.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))
		# PLATAFORMA/INTERACCIONES
		plataforma_interacciones = Interaccion.objects.filter(session__in=sesiones_del_agente)\
		.values('originalDetectIntentRequest_source').distinct()\
		.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))
		# CONTEO DE SESIONES DEL AGENTE
		sesiones_del_agente_contador = Sesion.objects.filter(id_proyecto=Agente.objects.get(pk=id_agente)).count()
		# PLATAFORMA/SESIONES
		total_sesion_plataforma = Interaccion.objects.filter(session__in=sesiones_del_agente)\
		.values('originalDetectIntentRequest_source')\
		.annotate(total_sesiones=(Count('session', distinct=True)))

		# Obtiene las graficas que quiere el cliente en el dashboard
		graficas_solicitadas = GraficaCliente.objects.all().filter(cliente = cliente)
		# Recorre las graficas que quiere el usuario
		for graph in graficas_solicitadas:
			grafica = Grafica.objects.get(pk = graph.grafica.pk)
			"""nombre = str(grafica.nombre_funcion) + "(" + str(extras.pk) + "," + str(cliente.pk) + ")"
			print(nombre)
			intents_totales = eval(nombre)
			print(intents_totales)"""

			# Si quiere la gráfica de intenciones frecuentes
			if grafica.nombre_funcion == "intenciones_frecuentes":
				intents_totales = intenciones_frecuentes(extras.pk,cliente.pk, agente, sesiones_del_agente)

			# Si quiere la gráfica de frases no reconocidas
			elif grafica.nombre_funcion == "frases_no_reconocidas":
				frases_noReconocidas = frases_no_reconocidas(extras.pk,cliente.pk, agente, sesiones_del_agente)

			# Si quiere la gráfica de interacciones por hora
			elif grafica.nombre_funcion == "interacciones_por_hora":
				interacciones_hora = interacciones_por_hora(extras.pk,cliente.pk, agente, sesiones_del_agente)

				# SESIONES DEL DÍA
				sesiones_del_dia = Interaccion.objects.values('session_id').filter(date__date=date.today(),session__in=sesiones_del_agente).distinct().count()
				x = interacciones_hora.aggregate(Avg('total'))

			elif grafica.nombre_funcion == "estadisticas_7_dias":
				estadisticas_return = estadisticas_7_dias(extras.pk,cliente.pk, agente, sesiones_del_agente)
				ult = estadisticas_return[0]

				interacciones_totales = estadisticas_return[1]

				fecha6 = estadisticas_return[2]
				fecha5 = estadisticas_return[3]
				fecha4 = estadisticas_return[4]
				fecha3 = estadisticas_return[5]
				fecha2 = estadisticas_return[6]
				fecha1 = estadisticas_return[7]
				fecha7 = estadisticas_return[8] - timedelta(days=1)

				nro_interacciones_7_dias = estadisticas_return[9]

				nro_interacciones1 = estadisticas_return[10]
				nro_interacciones2 = estadisticas_return[11]
				nro_interacciones3 = estadisticas_return[12]
				nro_interacciones4 = estadisticas_return[13]
				nro_interacciones5 = estadisticas_return[14]
				nro_interacciones6 = estadisticas_return[15]
				nro_interacciones7 = estadisticas_return[16]

				nro_sesiones_sesiones_7_dias = estadisticas_return[17]

				nro_sesiones1 = estadisticas_return[18]
				nro_sesiones2 = estadisticas_return[19]
				nro_sesiones3 = estadisticas_return[20]
				nro_sesiones4 = estadisticas_return[21]
				nro_sesiones5 = estadisticas_return[22]
				nro_sesiones6 = estadisticas_return[23]
				nro_sesiones7 = estadisticas_return[24]
				nro_sesionest = estadisticas_return[25]

	else:
		mensaje_sin_estadisticas = "No hay estadísticas que mostrar"

	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
def interacciones_source(request, id_agente, source, template_name="dashboard/base.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
	plataforma = Plataforma.objects.get(pk=source)
	# SESIONES DEL AGENTE
	sesiones_del_agente = Sesion.objects.filter(id_proyecto=Agente.objects.get(pk=id_agente))
	interacciones = Interaccion.objects.filter(session__in = sesiones_del_agente, originalDetectIntentRequest_source=plataforma).order_by('-date')
	sesiones = Interaccion.objects.select_related('session').filter(originalDetectIntentRequest_source=plataforma,session__in = sesiones_del_agente)\
	.values('session').distinct()\
	.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))
	try:
		tarjetas = Facebook_card.objects.select_related('responseId').filter(responseId__in=interacciones)
		botones = Buttons_card.objects.select_related('card').filter(card__in=tarjetas)
	except:
		pass
	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
def sesiones(request, id_agente, source, template_name="dashboard/sesiones.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
	plataforma = Plataforma.objects.get(pk=source)
	# SESIONES DEL AGENTE
	sesiones_del_agente = Sesion.objects.filter(id_proyecto=Agente.objects.get(pk=id_agente))
	interacciones = Interaccion.objects.filter(session__in = sesiones_del_agente, originalDetectIntentRequest_source=plataforma).order_by('-date')
	# SESIONES DE LA PLATAFORMA (SOURCE)
	sesiones = Interaccion.objects.select_related('session').filter(originalDetectIntentRequest_source=plataforma,session__in = sesiones_del_agente)\
	.values('session').distinct()\
	.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))
	# PLATAFORMAS DEL AGENTE
	plataformas_del_agente = Interaccion.objects.filter(session__in=sesiones_del_agente)\
	.values('originalDetectIntentRequest_source').distinct()\
	.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))

	query2 = connection.cursor()
	query2.execute("SELECT DISTINCT dashboard_sesion.session,dashboard_plataforma.\"originalDetectIntentRequest_source\", MAX(dashboard_interaccion.date), dashboard_plataforma.id, dashboard_sesion.id, dashboard_sesion.\"originalDetectIntentRequest_chat_first_name\" FROM dashboard_sesion INNER JOIN dashboard_interaccion ON dashboard_interaccion.session_id = dashboard_sesion.id INNER JOIN dashboard_plataforma ON dashboard_plataforma.id = dashboard_interaccion.\"originalDetectIntentRequest_source_id\" WHERE dashboard_plataforma.\"originalDetectIntentRequest_source\" = '"+str(plataforma)+"' GROUP BY dashboard_sesion.session, dashboard_plataforma.\"originalDetectIntentRequest_source\",dashboard_plataforma.id, dashboard_sesion.id ORDER BY MAX(dashboard_interaccion.date) DESC")
	result = query2.fetchall()
	try:
		tarjetas = Facebook_card.objects.select_related('responseId').filter(responseId__in=interacciones)
		botones = Buttons_card.objects.select_related('card').filter(card__in=tarjetas)
	except:
		pass
	return render(request, template_name, locals(),)


@login_required(login_url = '/login/')
def timeline(request, id_agente, source, session, template_name="dashboard/timeline.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
	plataforma = Plataforma.objects.get(pk=source)
	# SESIONES DEL AGENTE
	sesiones_del_agente = Sesion.objects.filter(id_proyecto=Agente.objects.get(pk=id_agente))
	interacciones = Interaccion.objects.filter(session__in = sesiones_del_agente, originalDetectIntentRequest_source=plataforma).order_by('-date')
	# SESIONES DE LA PLATAFORMA (SOURCE)
	sesiones = Interaccion.objects.select_related('session').filter(originalDetectIntentRequest_source=plataforma,session__in = sesiones_del_agente)\
	.values('session').distinct()\
	.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))
	# PLATAFORMAS DEL AGENTE
	plataformas_del_agente = Interaccion.objects.filter(session__in=sesiones_del_agente)\
	.values('originalDetectIntentRequest_source').distinct()\
	.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))

	dataSesion = Sesion.objects.get(pk=session)
	dataInteraccion = Interaccion.objects.filter(session = dataSesion).order_by('-date')

	try:
		tarjetas = Facebook_card.objects.select_related('responseId').filter(responseId__in=interacciones)
		botones = Buttons_card.objects.select_related('card').filter(card__in=tarjetas)
	except:
		pass
	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
def timelinePDF(request, id_agente, source, session, template_name="dashboard/timelinePDF.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
	plataforma = Plataforma.objects.get(pk=source)
	# SESIONES DEL AGENTE
	sesiones_del_agente = Sesion.objects.filter(id_proyecto=Agente.objects.get(pk=id_agente))
	interacciones = Interaccion.objects.filter(session__in = sesiones_del_agente, originalDetectIntentRequest_source=plataforma).order_by('-date')
	# SESIONES DE LA PLATAFORMA (SOURCE)
	sesiones = Interaccion.objects.select_related('session').filter(originalDetectIntentRequest_source=plataforma,session__in = sesiones_del_agente)\
	.values('session').distinct()\
	.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))
	# PLATAFORMAS DEL AGENTE
	plataformas_del_agente = Interaccion.objects.filter(session__in=sesiones_del_agente)\
	.values('originalDetectIntentRequest_source').distinct()\
	.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))

	dataSesion = Sesion.objects.get(pk=session)
	dataInteraccion = Interaccion.objects.filter(session = dataSesion).order_by('-date')
	print(dataInteraccion)

	try:
		tarjetas = Facebook_card.objects.select_related('responseId').filter(responseId__in=interacciones)
		botones = Buttons_card.objects.select_related('card').filter(card__in=tarjetas)
	except:
		pass
	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
def facebook_room(request, id_agente, source, session, chat_id, template_name="dashboard/facebook_room.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
	plataforma = Plataforma.objects.get(pk=source)

	# buscamos la session
	dataSesion = Sesion.objects.get(pk=session)
	interacciones = Interaccion.objects.filter(session=dataSesion).order_by('date')
	if dataSesion.status != 2:
		dataSesion.status = 2 # en handover
		dataSesion.save()

		try:
			notificaciones = Notification.objects.filter(target_object_id=session)
			notificaciones.delete()
		except:
			pass
	else:
		mensaje = "Esta sesión ya fue atendida por otro usuario"

	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
def whatsapp_room(request, id_agente, source, session, template_name="dashboard/whatsapp_room.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
	plataforma = Plataforma.objects.get(pk=source)

	# buscamos la session
	dataSesion = Sesion.objects.get(pk=session)
	interacciones = Interaccion.objects.filter(session=dataSesion).order_by('date')
	if dataSesion.status != 2:
		dataSesion.status = 2 # en handover
		dataSesion.save()

		try:
			notificaciones = Notification.objects.filter(target_object_id=session)
			notificaciones.delete()
		except:
			pass
	else:
		mensaje = "Esta sesión ya fue atendida por otro usuario"

	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
# @decorator_function
def telegram_room(request, id_agente, source, session, chat_id, template_name="dashboard/telegram_room.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
	plataforma = Plataforma.objects.get(pk=source)

	# buscamos la session
	dataSesion = Sesion.objects.get(pk=session)
	interacciones = Interaccion.objects.filter(session=dataSesion).order_by('date')
	if dataSesion.status != 2:
		dataSesion.status = 2 # en handover
		dataSesion.save()

		try:
			notificaciones = Notification.objects.filter(target_object_id=session)
			notificaciones.delete()
		except:
			pass
	else:
		mensaje = "Esta sesión ya fue atendida por otro usuario"

	return render(request, template_name, locals(),)

# El request trae el id se la sesion
@login_required(login_url = '/login/')
def cambiar_sesion_status_on_close(request):
	session_id = request.GET.get('session_id', None)

	try:
		sesion = Sesion.objects.get(session=session_id)
	except:
		sesion = Sesion.objects.get(pk=session_id)
	sesion.status=1
	sesion.save()

	try:
		conteo_fallbacks = DefaultFallbackCount.objects.get(session=sesion)
		conteo_fallbacks.delete()
	except:
		print("Excepción al eliminar conteo de DefaultFallBacks")

	try:
		notificaciones = Notification.objects.filter(target_object_id=session_id)
		notificaciones.delete()
	except:
		pass

	headers = {
		'content-type': 'application/json',
	}

	data = '{"username": "springAdmin","password": "abcde"}'

	response = requests.post('https://pruebas4.springlabsdevs.net/webbot-api/login', headers=headers, data=data)
	response_json = response.json()
	token = response_json['result']['token']

	url = "https://pruebas4.springlabsdevs.net/webbot-api/messages/changeSessionStatus"

	sesionString = session_uuid(str(sesion.session))

	payload = "{\n    \"id\": \""+sesionString+"\",\n    \"dialogFlowStatus\": 1\n}"
	headers = {
	    'clientId':"30c34f89-e3d1-4371-b697-8775f956f2e7",
	    'Content-Type': "application/json",
	    'token': token,
	    'User-Agent': "PostmanRuntime/7.15.0",
	    'Accept': "*/*",
	    'Cache-Control': "no-cache",
	    'Postman-Token': "50a528f4-6a3d-436e-89be-cae7a0197c2b,3a9f6905-126b-4415-8290-5cba3a220402",
	    'Host': "pruebas4.springlabsdevs.net",
	    'accept-encoding': "gzip, deflate",
	    'content-length': "79",
	    'Connection': "keep-alive",
	    'cache-control': "no-cache"
	    }

	response = requests.request("POST", url, data=payload, headers=headers)

	data = {'resultado': 'status cambiado con exito al salir'}

	return JsonResponse(data)

# El request trae el id se la sesion
@login_required(login_url = '/login/')
def cambiar_sesion_status_on_enter(request):
	session_id = request.GET.get('session_id', None)

	try:
		sesion = Sesion.objects.get(session=session_id)
	except:
		sesion = Sesion.objects.get(pk=session_id)
	interacciones = Interaccion.objects.filter(session = sesion)
	for i in interacciones:
		print(i)
		plataforma = i.originalDetectIntentRequest_source.originalDetectIntentRequest_source
		break

	sesion.status=2
	sesion.save()

	try:
		conteo_fallbacks = DefaultFallbackCount.objects.get(session=sesion)
		conteo_fallbacks.delete()
	except:
		print("Excepción al eliminar conteo de DefaultFallBacks")

	try:
		notificaciones = Notification.objects.filter(target_object_id=session_id)
		if notificaciones.count() > 0:
			notificaciones.delete()
		else:
			notificaciones = Notification.objects.filter(target_object_id=sesion.id)
			notificaciones.delete()
	except:
		print("Excepción al eliminar notificaciones")

	headers = {
		'content-type': 'application/json',
	}

	data = '{"username": "springAdmin","password": "abcde"}'

	response = requests.post('https://pruebas4.springlabsdevs.net/webbot-api/login', headers=headers, data=data)
	response_json = response.json()
	token = response_json['result']['token']

	url = "https://pruebas4.springlabsdevs.net/webbot-api/messages/changeSessionStatus"

	sesionString = session_uuid(str(sesion.session))

	payload = "{\n    \"id\": \""+sesionString+"\",\n    \"dialogFlowStatus\": 2\n}"
	headers = {
	    'clientId':"30c34f89-e3d1-4371-b697-8775f956f2e7",
	    'clientId':"30c34f89-e3d1-4371-b697-8775f956f2e7",
	    'Content-Type': "application/json",
	    'token': token,
	    'User-Agent': "PostmanRuntime/7.15.0",
	    'Accept': "*/*",
	    'Cache-Control': "no-cache",
	    'Postman-Token': "50a528f4-6a3d-436e-89be-cae7a0197c2b,3a9f6905-126b-4415-8290-5cba3a220402",
	    'Host': "pruebas4.springlabsdevs.net",
	    'accept-encoding': "gzip, deflate",
	    'content-length': "79",
	    'Connection': "keep-alive",
	    'cache-control': "no-cache"
	    }

	response = requests.request("POST", url, data=payload, headers=headers)

	data = {'resultado': 'status cambiado con exito al entrar'}

	return JsonResponse(data)

@login_required(login_url = '/login/')
def notificaciones(request, template_name="dashboard/notificaciones.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		clientes = Cliente.objects.all()
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0

	# CLIENTE ESPECIFICO DEL USUARIO EN TURNO
	if User.objects.filter(groups__name='Administrador', id=usuario.id).count() > 0:
		pass
	else:
		cliente = Cliente.objects.get(pk=extras.cliente.id)


	query3 = connection.cursor()
	query3.execute("SELECT notifications_notification.id, dashboard_plataforma.\"originalDetectIntentRequest_source\" AS plataforma, dashboard_agente.nombre_agente, notifications_notification.description, notifications_notification.timestamp AS fecha, notifications_notification.data, notifications_notification.verb AS id_plat, target_object_id AS id_sesion, dashboard_plataforma.id FROM notifications_notification INNER JOIN dashboard_agente ON CAST(dashboard_agente.id AS INT) = CAST(notifications_notification.action_object_object_id AS INT) INNER JOIN dashboard_plataforma ON CAST(dashboard_plataforma.id AS INT) = CAST(notifications_notification.verb AS INT) WHERE notifications_notification.recipient_id = '"+ str(usuario.id) +"' ")
	result3 = query3.fetchall()


	return render(request, template_name, locals(),)

# El request trae el id se la sesion
@login_required(login_url = '/login/')
def eliminar_notificacion(request):

	session_id = request.GET.get('session_id', None)
	sesion = Sesion.objects.get(pk=session_id)
	sesion.status=1
	sesion.save()
	try:
		conteo_fallbacks = DefaultFallbackCount.objects.get(session=sesion)
		conteo_fallbacks.delete()
	except:
		print("Excepción al eliminar conteo de DefaultFallBacks")
	try:
		notificaciones = Notification.objects.filter(target_object_id=session_id)
		notificaciones.delete()
	except:
		print("Excepción al eliminar notificaciones")

	data = {'resultado': 'status cambiado con exito'}

	return JsonResponse(data)
 

def eliminar_notificacion2(request, id_cita):
	query = connection.cursor()
	query.execute("UPDATE \"bot-prospectos\" SET \"statusCita\" = 1 WHERE \"idProspecto\" = '"+ id_cita +"'")

	return HttpResponseRedirect('/estadisticas_personalizadas/')

def eliminar_notificacion5(request, id_cita):
	query = connection.cursor()
	query.execute("UPDATE \"bot-prospectos\" SET \"statusCita\" = 2 WHERE \"idProspecto\" = '"+ id_cita +"'")

	return HttpResponseRedirect('/estadisticas_personalizadas/')

def eliminar_notificacion3(request, id_ses, id_plat, id_not):

	query2 = connection.cursor()
	query2.execute("SELECT timestamp FROM notifications_notification WHERE id = '"+ id_not +"' ")
	result2 = query2.fetchall()

	query = connection.cursor()
	query.execute("SELECT dashboard_plataforma.\"originalDetectIntentRequest_source\" , notifications_notification.data FROM notifications_notification INNER JOIN dashboard_plataforma ON CAST(dashboard_plataforma.id AS INT) = CAST(notifications_notification.verb AS INT) WHERE notifications_notification.id = '"+ id_not +"' ")
	result = query.fetchall()

	query3 = connection.cursor()
	query3.execute("UPDATE dashboard_sesion SET status = 1 WHERE id = '"+ id_ses +"'")

	for r in result2:
		query4 = connection.cursor()
		query4.execute("DELETE FROM notifications_notification WHERE timestamp = '"+ str(r[0]) +"'")


	for p in result:
		print(result)
		print(p[0])
		if p[0] == "whatsapp":
			return HttpResponseRedirect('/whatsapp_room/1/'+id_plat+'/'+id_ses+'')
		elif p[0] == "telegram":
			nuevochat = str(p[1]).replace('"chat_id"','')
			nuevochat2 = str(nuevochat).replace(':','')
			nuevochat3 = str(nuevochat2).replace('{','')
			nuevochat4 = str(nuevochat3).replace('}','')
			nuevochat5 = str(nuevochat4).replace('"','')
			return HttpResponseRedirect('/telegram_room/1/'+id_plat+'/'+id_ses+'/'+nuevochat5+'')
		elif p[0] == "facebook":
			nuevochat = str(p[1]).replace('"chat_id"','')
			nuevochat2 = str(nuevochat).replace(':','')
			nuevochat3 = str(nuevochat2).replace('{','')
			nuevochat4 = str(nuevochat3).replace('}','')
			nuevochat5 = str(nuevochat4).replace('"','')
			return HttpResponseRedirect('/facebook_room/1/'+id_plat+'/'+id_ses+'/'+nuevochat5+'')
		else:
			return HttpResponseRedirect('/webchat_room/1/'+id_plat+'/'+id_ses+'')
	return HttpResponseRedirect('/notificaciones/')

def eliminar_notificacion4(request, id_not, id_ses):
	query4 = connection.cursor()
	query4.execute("SELECT timestamp FROM notifications_notification WHERE id = '"+ id_not +"' ")
	result2 = query4.fetchall()
	query3 = connection.cursor()
	query3.execute("UPDATE dashboard_sesion SET status = 1 WHERE id = '"+ id_ses +"'")
	for r in result2:
		query = connection.cursor()
		query.execute("DELETE FROM notifications_notification WHERE timestamp = '"+ str(r[0]) +"'")

		return HttpResponseRedirect('/notificaciones/')
def getDataTableInfoRealTime(request,id_agente, source, session):

	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
	plataforma = Plataforma.objects.get(pk=source)
	# SESIONES DEL AGENTE
	sesiones_del_agente = Sesion.objects.filter(id_proyecto=Agente.objects.get(pk=id_agente))
	interacciones = Interaccion.objects.filter(session__in=sesiones_del_agente,
											   originalDetectIntentRequest_source=plataforma).order_by('-date')
	# SESIONES DE LA PLATAFORMA (SOURCE)
	sesiones = Interaccion.objects.select_related('session').filter(originalDetectIntentRequest_source=plataforma,
																	session__in=sesiones_del_agente) \
		.values('session').distinct() \
		.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))
	# PLATAFORMAS DEL AGENTE
	plataformas_del_agente = Interaccion.objects.filter(session__in=sesiones_del_agente) \
		.values('originalDetectIntentRequest_source').distinct() \
		.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))

	dataSesion = Sesion.objects.get(pk=session)
	dataInteraccion = Interaccion.objects.filter(session=dataSesion).order_by('-date').values('queryText','fulfillmentText').order_by('-date')
	#dataInteraccion = Interaccion.objects.filter(session=dataSesion).order_by('-date')
	from pprint import pprint
	#pprint(dataInteraccion)
	#print("\n\nVeamos:{}\n\n".format(dataInteraccion.values_list('queryText', flat=True)))
	try:
		tarjetas = Facebook_card.objects.select_related('responseId').filter(responseId__in=interacciones)
		botones = Buttons_card.objects.select_related('card').filter(card__in=tarjetas)
	except:
		pass

	conversacion_completa = []
	pprint(dataInteraccion)

	for info in range(len(dataInteraccion)):
		conversacion = []
		conversacion.insert(0, dataInteraccion[info]['fulfillmentText'])
		conversacion.insert(0, dataInteraccion[info]['queryText'])
		conversacion_completa.append(conversacion)

	encabezados = [['Opción elegida por usuario'],['Respuesta del Bot']]
	respuesta = {
		'data':list(conversacion_completa),
		'columns':encabezados
	}

	archivo = open('array.txt', 'w+')
	archivo.write("{}".format(respuesta))
	archivo.close()
	return JsonResponse(respuesta)

# El request trae el id se la sesion
@login_required(login_url = '/login/')
def contador_interacciones(request):

	session_id = request.GET.get('session_id', None)

	sesion = Sesion.objects.get(session=session_id)

	lista = []

	lista2 = []

	JSONer = {}

	# Todas las interacciones existentes para esa sesion
	interacciones = Interaccion.objects.filter(session=sesion).order_by('date')
	pending = AsyncChat.objects.filter(id_user = request.user, responseId__in = interacciones)
	if pending.count() == 0:
		for value in interacciones:
			new_pending = AsyncChat(
				responseId = value,
				id_user = request.user,
				date = value.date,
				)
			new_pending.save()

		pending = AsyncChat.objects.filter(id_user = request.user, responseId__in = interacciones).order_by('date')
		#JSONer['interacciones'] = list(pending.values())
		return JsonResponse(JSONer)

	else:
		for value in pending:
			lista.append(value.responseId.responseId)
		interacciones = Interaccion.objects.filter(session=sesion).exclude(responseId__in=lista).order_by('date')
		if interacciones.count() != 0:
			for value in interacciones:
				new_pending = AsyncChat(
					responseId = value,
					id_user = request.user,
					date = value.date,
					)
				new_pending.save()
			for value in pending:
				lista2.append(value.responseId.id)
			pending = AsyncChat.objects.filter(id_user = request.user, responseId__in = interacciones).exclude(responseId__in=lista2).order_by('date')
			JSONer['interacciones'] = list(pending.values())

	return JsonResponse(JSONer)

@csrf_exempt
def send_message(request):
	# message = responseId_id
	# user = usuario en turno
	message = request.POST.get('message', None)
	user = request.POST.get('user', None)
	usuario = User.objects.get(pk=user)
	interaccion = Interaccion.objects.get(id=message)
	interaccion_imprimir = AsyncChat.objects.get(id_user = usuario, responseId = interaccion)
	response_text = { "message": interaccion_imprimir.responseId.queryText,
	 				"bot": interaccion_imprimir.responseId.fulfillmentText,
					"date" : interaccion_imprimir.date,
					"nombre" : interaccion_imprimir.responseId.session.originalDetectIntentRequest_chat_first_name,
					"apellido" : interaccion_imprimir.responseId.session.originalDetectIntentRequest_chat_last_name,
					"nombre_bot" : interaccion_imprimir.responseId.session.id_proyecto.nombre_agente,}

	return JsonResponse(response_text)

# El request trae el id se la sesion
@login_required(login_url = '/login/')
def borrar_registro_interacciones_realtime(request):
	session_id = request.GET.get('session_id', None)

	try:
		sesion = Sesion.objects.get(session=session_id)
	except:
		sesion = Sesion.objects.get(pk=session_id)

	try:
		interacciones = Interaccion.objects.filter(session=sesion).order_by('date')

		pending = AsyncChat.objects.filter(id_user = request.user, responseId__in = interacciones)

		pending.delete()
	except:
		pass

	data = {'resultado': 'Registros de chats en tiempo real borrados'}

	return JsonResponse(data)

@csrf_exempt
def imput_message(request):

	utc=pytz.UTC
	message = request.POST.get('message', None)
	session = request.POST.get('session', None)
	plataforma = request.POST.get('plataforma', None)
	image = request.POST.get('image', None)

	try:
		sesion = Sesion.objects.get(session=session)
	except:
		sesion = Sesion.objects.get(pk=session)

	nueva_interaccion = Interaccion(
		responseId = str(sesion.id_proyecto.nombre_agente)+""+str(datetime.now()),
		queryText = "",
		allRequiredParamsPresent = "HandOver",
		fulfillmentText = message,
		intent_name = "HandOver",
		intent_displayName = "HandOver",
		intentDetectionConfidence = "HandOver",
		languageCode = "HandOver",
		session = sesion,
		originalDetectIntentRequest_source = Plataforma.objects.get(originalDetectIntentRequest_source=plataforma),
		date=datetime.now(),
		)
	nueva_interaccion.save()

	if plataforma == 'telegram':

		TOKEN = sesion.id_proyecto.telegram_token

		tb = telebot.TeleBot(TOKEN)	#create a new Telegram Bot object

		# sendMessage

		chatid = sesion.originalDetectIntentRequest_chat_id

		text = message

		tb.send_message(chatid, text)

	elif plataforma == 'whatsapp':

		numero = titulo_chat(sesion.id)

		Cabecera = sesion.id_proyecto.nombre_agente
		UserName = sesion.id_proyecto.user_wavy
		AuthenticationToken = sesion.id_proyecto.whatsapp_token
		url = "https://api-messaging.movile.com/v1/whatsapp/send"

		try:
			comma = image.index(',')+1
			image = str(image[comma:])

			payload = "{\n    \"destinations\": [\n        {\n            \"correlationId\": \"001\",\n            \"destination\": \""+numero+"\"\n                    }\n    ],\n    \"message\": {\n        \"image\": {\n            \"type\": \"JPG\",\n            \"data\": \""+image+"\",\n            \"caption\": \""+message+"\"\n        }\n    }\n}"

			headers = {
			    'clientId':"30c34f89-e3d1-4371-b697-8775f956f2e7",
			    'Content-Type': "application/json",
			    'UserName': UserName,
			    'AuthenticationToken': AuthenticationToken,
			    'User-Agent': "PostmanRuntime/7.16.3",
			    'Accept': "*/*",
			    'Cache-Control': "no-cache",
			    'Postman-Token': "c3124065-8eaf-4515-9f54-2925fcc5d9cb,e9b628aa-387d-4fab-9d45-8b2a111378a4",
			    'Host': "api-messaging.movile.com",
			    'Accept-Encoding': "gzip, deflate",
			    'Content-Length': "59488",
			    'Connection': "keep-alive",
			    'cache-control': "no-cache"
			    }

			response = requests.request("POST", url, data=payload, headers=headers)

		except:
			print("Sin imagen")
			body_HSM = {
	            "destinations": [
	                {
	                    "correlationId": "001",
	                    "destination": numero
	                }
	            ],
	            "message": {
	                "hsm": {
	                    "namespace": "whatsapp:hsm:ecommerce:movile",
	                    "elementName": "chatclub_open_session_v2",
	                    "parameters": [
	                        ""+Cabecera,
	                        ""+message
	                    ],
	                    "languagePolicy": "DETERMINISTIC"
	                }
	            }
	        }

			response = requests.post(url,data=json.dumps(body_HSM),headers={'Content-Type':'application/json','UserName':UserName,'AuthenticationToken':AuthenticationToken})

	elif plataforma == 'facebook':

		ACCESS_TOKEN = sesion.id_proyecto.facebook_token
		bot = Bot(ACCESS_TOKEN)
		recipient_id = sesion.facebook_chat
		message = message

		bot.send_text_message(recipient_id, message)

	elif plataforma == 'Web Chat':
		headers = {
			'content-type': 'application/json',
		}

		data = '{"username": "springAdmin","password": "abcde"}'

		response = requests.post('https://pruebas4.springlabsdevs.net/webbot-api/login', headers=headers, data=data)
		response_json = response.json()
		token = response_json['result']['token']

		url = "https://pruebas4.springlabsdevs.net/webbot-api/messages/assessorSendMessage"
		print(sesion)
		sesionString = session_uuid(str(sesion.session))
		print("#"*50)
		print(sesionString)
		payload = "{\n    \"id\": \""+sesionString+"\",\n    \"message\": \""+message+"\"\n}"
		headers = {
		    'clientId':"30c34f89-e3d1-4371-b697-8775f956f2e7",
		    'Content-Type': "application/json",
		    'token': token,
		    'User-Agent': "PostmanRuntime/7.13.0",
		    'Accept': "*/*",
		    'Cache-Control': "no-cache",
		    'Postman-Token': "6db1d787-57dc-42af-8ba6-51f891cde87e,b4464c54-335b-48ad-831b-ddcbcebbf500",
		    'Host': "pruebas4.springlabsdevs.net",
		    'accept-encoding': "gzip, deflate",
		    'content-length': "77",
		    'Connection': "keep-alive",
		    'cache-control': "no-cache"
		    }

		response = requests.request("POST", url, data=payload, headers=headers)

	response_text = { "message": "El mensaje ha sido guardado y enviado",}
	return JsonResponse(response_text)

def telegram_sesion(chat_id):
	sesion = Sesion.objects.filter(originalDetectIntentRequest_chat_id = chat_id)
	if sesion.count() > 0:
		for i in sesion:
			return i.session
	return ""

def chats(request, agente, id_sesion, template_name="dashboard/chats.html"):

	usuario = request.user

	# Obtiene objeto de tipo Agente con el agente que llego por parámetro
	objAgente = Agente.objects.get(pk = agente)

	# Obtiene el cliente
	cliente = Cliente.objects.get(nombre_cliente=objAgente.cliente)

	# Obtiene plataforma del id_sesion
	plataforma = Interaccion.objects.filter(session = id_sesion).last()
	plataforma = plataforma.originalDetectIntentRequest_source.id

	# SI no se ha seleccionado ninguna sesión
	if id_sesion == '0':
		print("No hay sesion seleccionada")
	# Si ya se selecciono una sesión, se visualizará su conversación
	else:
		# Obtiene la sección seleccionada que se recibió por parámetro
		sesion_seleccionada = Sesion.objects.get(pk = id_sesion)
		# Obtiene todas las interacciones de la sesión seleccionada ordenadas por fecha
		interacciones_seleccionadas = Interaccion.objects.all().filter(session = sesion_seleccionada).order_by('date')
		try:
			obj_AsyncSession = AsyncSession.objects.get(sessionId = sesion_seleccionada, id_user = usuario)
			obj_AsyncSession.status = 1
			obj_AsyncSession.save()
		except:
			pass
		# Obtiene todas las sesiones que existen en la base de datos
	sesiones_totales = Sesion.objects.all().filter(id_proyecto = objAgente)

	# Obtiene todas las sesiones, con su plataforma y el total de interacciones
	sesion_plataforma_interaccion = Interaccion.objects.filter(session__in=sesiones_totales)\
		.values('session','originalDetectIntentRequest_source')\
		.annotate(total_sesiones=(Count('session', distinct=True)))\
		.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))

	# Obtendrá objetos de SESION y lo agregará a arreglo
	lista_sesiones = []
	for sesiones in sesion_plataforma_interaccion:
		objSesiones = Sesion.objects.get(pk = sesiones['session'])
		lista_sesiones.append(objSesiones)

	# Ordena todas las interacciones filtradas por sesion
	lista_interacciones = []
	for sesion in lista_sesiones:
		interacciones_sesion = Interaccion.objects.all().filter(session = sesion).order_by('-date')
		lista_interacciones.append(interacciones_sesion[0])


	# Crea un objeto personalizado con ID SESION, ID PLATAFORMA, FECHA
	lista_final = []
	contador = 0
	for sesiones in sesion_plataforma_interaccion:
		# Obtiene un objeto SESIÓN de cada sesion
		objSesion = Sesion.objects.get(pk = sesiones['session'])
		# Obtiene un objeto PLATAFORMA de cada sesión
		objPlataforma = Plataforma.objects.get(pk = sesiones['originalDetectIntentRequest_source'])
		try:
			obj_AsyncSession = AsyncSession.objects.get(sessionId = objSesion, id_user = usuario)
			objSesiones = {
				'id_sesion' : sesiones['session'],
				'id_plataforma' : sesiones['originalDetectIntentRequest_source'],
				'fecha' : lista_interacciones[contador].date.replace(tzinfo=timezone.utc).astimezone(tz=None),
				'obj_sesion' : objSesion,
				'objPlataforma' : objPlataforma,
				'status' : obj_AsyncSession.status,
				'mensaje' : lista_interacciones[contador].queryText,
			}
			lista_final.append(objSesiones)
			contador = contador+1
		except:
			objSesiones = {
				'id_sesion' : sesiones['session'],
				'id_plataforma' : sesiones['originalDetectIntentRequest_source'],
				'fecha' : lista_interacciones[contador].date.replace(tzinfo=timezone.utc).astimezone(tz=None),
				'obj_sesion' : objSesion,
				'objPlataforma' : objPlataforma,
				'mensaje' : lista_interacciones[contador].queryText,

			}
			lista_final.append(objSesiones)
			contador = contador+1

	# Ordena la lista de sesiones por fecha ASCENDENTE
	lista_ordenada = sorted(lista_final, key=lambda objeto: objeto['fecha'])

	return render(request, template_name, locals(),)

# El request trae el id se la sesion
@login_required(login_url = '/login/')
def contador_sesiones(request):
	agente_id = request.GET.get('agente_id', None)
	agente = Agente.objects.get(pk=agente_id)

	# Obtiene el cliente
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)

	# Obtiene todas las sesiones que existen en la base de datos
	sesiones_totales = Sesion.objects.all().filter(id_proyecto = agente)

	# Obtiene todas las sesiones, con su plataforma y el total de interacciones
	sesion_plataforma_interaccion = Interaccion.objects.filter(session__in=sesiones_totales)\
		.values('session','originalDetectIntentRequest_source')\
		.annotate(total_sesiones=(Count('session', distinct=True)))\
		.annotate(total_interacciones=(Count('originalDetectIntentRequest_source')))

	# Obtendrá objetos de SESION y lo agregará a arreglo
	lista_sesiones = []
	for sesiones in sesion_plataforma_interaccion:
		objSesiones = Sesion.objects.get(pk = sesiones['session'])
		lista_sesiones.append(objSesiones)

	# Ordena todas las interacciones filtradas por sesion
	lista_interacciones = []
	for sesion in lista_sesiones:
		interacciones_sesion = Interaccion.objects.all().filter(session = sesion).order_by('-date')
		lista_interacciones.append(interacciones_sesion[0])


	# Crea un objeto personalizado con ID SESION, ID PLATAFORMA, FECHA
	lista_final = []
	contador = 0
	for sesiones in sesion_plataforma_interaccion:
		# Obtiene un objeto SESIÓN de cada sesion
		objSesion = Sesion.objects.get(pk = sesiones['session'])
		# Obtiene un objeto PLATAFORMA de cada sesión
		objPlataforma = Plataforma.objects.get(pk = sesiones['originalDetectIntentRequest_source'])
		objSesiones = {
			'id_sesion' : sesiones['session'],
			'id_plataforma' : sesiones['originalDetectIntentRequest_source'],
			'fecha' : lista_interacciones[contador].date.replace(tzinfo=timezone.utc).astimezone(tz=None),
			'obj_sesion' : objSesion,
			'objPlataforma' : objPlataforma,
			'mensaje' : lista_interacciones[contador].queryText,
		}
		lista_final.append(objSesiones)
		contador = contador+1

	# Ordena la lista de sesiones por fecha ASCENDENTE
	lista_ordenada = sorted(lista_final, key=lambda objeto: objeto['fecha'])


	lista = []

	lista2 = []

	lista_auxiliar = []

	JSONer = {}

	JSONer_aux = {}
	# Todas las interacciones existentes para esa sesion
	pending = AsyncSession.objects.filter(id_user = request.user, sessionId__in = sesiones_totales)
	if pending.count() == 0:
		for value in lista_ordenada:
			new_pending = AsyncSession(
				sessionId = value['obj_sesion'],
				id_user = request.user,
				date = value['fecha'],
			)
			new_pending.save()

		pending = AsyncSession.objects.filter(id_user = request.user, sessionId__in = sesiones_totales).order_by('date')
		#JSONer['interacciones'] = list(pending.values())
		return JsonResponse(JSONer)

	else:
		pending = AsyncSession.objects.filter(id_user = request.user, sessionId__in = sesiones_totales).order_by('date')

		for dato in pending:
			lista_auxiliar.append(dato)

		sesiones_1 = len(lista_ordenada)
		sesiones_2 = pending.count()
		esIgual = True

		if sesiones_1 == sesiones_2:
			for i in range(sesiones_1):
				if lista_ordenada[i]['obj_sesion'].session == lista_auxiliar[i].sessionId.session:
					esIgual = True

				else:
					esIgual = False

		else:
			esIgual = False

		lista_json = []
		if esIgual == False:
			for dato in lista_ordenada:
				JSONer_aux = {
					'id_sesion': dato['id_sesion'],
  					'id_plataforma': dato['id_plataforma'],
  					'fecha': str(dato['fecha']),
  					'obj_sesion': dato['obj_sesion'].session,
  					'objPlataforma': dato['objPlataforma'].originalDetectIntentRequest_source,
  					'id_user': request.user.pk
				}
				lista_json.append(JSONer_aux)
			longitud_lista = len(lista_json)


			mensaje_nuevo = lista_json[longitud_lista-1]['id_sesion']
			sesion_nueva = Sesion.objects.get(pk = mensaje_nuevo)
			try:
				obj_AsyncSession1 = AsyncSession.objects.get(id_user = request.user, sessionId = sesion_nueva)
				obj_AsyncSession1.status = 0
				obj_AsyncSession1.save()
			except:
				pass
			JSONer = json.dumps(lista_json)

		else:
			print("")

	return JsonResponse(JSONer, safe=False)

@csrf_exempt
def send_session(request):
	# message = responseId_id
	# user = usuario en turno
	session = request.POST.get('session', None)
	user = request.POST.get('user', None)
	plataforma = request.POST.get('plataforma', None)
	usuario = User.objects.get(pk=user)
	sesion = Sesion.objects.get(pk = session)
	interacciones = Interaccion.objects.all().filter(session = sesion).order_by('-date')
	obj_plataforma = Plataforma.objects.get(pk = plataforma)
	sesion_imprimir = AsyncSession.objects.filter(id_user = usuario, sessionId = sesion).exists()
	if sesion_imprimir == 1:
		sesion_imprimir = AsyncSession.objects.get(id_user = usuario, sessionId = sesion)
		sesion_imprimir.date = interacciones[0].date
		sesion_imprimir.save()
	else:
		nuevaSesion = AsyncSession(
			sessionId = sesion,
			id_user = usuario,
			date = interacciones[0].date,
		)
		nuevaSesion.save()
		sesion_imprimir = AsyncSession.objects.get(id_user = usuario, sessionId = sesion)
	url = "{% url 'chats'"+ str(sesion.id_proyecto.pk) +" "+ str(sesion.pk) + " %}"
	response_text = { "session": sesion.session,
	 				"plataforma": obj_plataforma.originalDetectIntentRequest_source,
					"date" : sesion_imprimir.date,
					"id_session": sesion.pk,
					"status" : sesion_imprimir.status,
					"url" : url,
					"mensaje" : interacciones[0].queryText,
					}
	return JsonResponse(response_text)

@login_required(login_url = '/login/')
def cita_resuelta(request, id_cita):

	objCita = Cita.objects.get(pk = id_cita)

	if objCita.estado == 0:
		objCita.estado = 1
		objCita.save()

	return HttpResponseRedirect('/estadisticas_personalizadas/')

@login_required(login_url = '/login/')
def webchat_room(request, id_agente, source, session, template_name="dashboard/webchat_room.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
	plataforma = Plataforma.objects.get(pk=source)

	# buscamos la session
	dataSesion = Sesion.objects.get(pk=session)
	interacciones = Interaccion.objects.filter(session=dataSesion).order_by('date')
	if dataSesion.status != 2:
		dataSesion.status = 2 # en handover
		dataSesion.save()

		try:
			notificaciones = Notification.objects.filter(target_object_id=session)
			notificaciones.delete()
		except:
			pass
	else:
		mensaje = "Esta sesión ya fue atendida por otro usuario"

	return render(request, template_name, locals(),)

def webchat_catcher(queryText, session):

	message = queryText
	session = session
	plataforma = 'Web Chat'

	try:
		sesion = Sesion.objects.get(session__contains=session)
	except:
		print("#"*50)
		print("SESSION ID NO COINCIDE (WEBAPI)")

	nueva_interaccion = Interaccion(
		responseId = str(sesion.id_proyecto.nombre_agente)+""+str(datetime.now()),
		queryText = message,
		allRequiredParamsPresent = "HandOver",
		fulfillmentText = "",
		intent_name = "HandOver",
		intent_displayName = "HandOver",
		intentDetectionConfidence = "HandOver",
		languageCode = "HandOver",
		session = sesion,
		originalDetectIntentRequest_source = Plataforma.objects.get(originalDetectIntentRequest_source=plataforma),
		date=datetime.now(),
		)
	nueva_interaccion.save()

	return True
