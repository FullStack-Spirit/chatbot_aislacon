from django.conf.urls import url, include
from django.contrib import admin
from dashboard import views as dashboard_views
from estadisticas import views as estadisticas_views
from usuarios import views as usuarios_views
from admin1 import views as admin1_views
from admin2 import views as admin2_views
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth.views import password_reset, password_reset_done, password_reset_confirm, password_reset_complete
import notifications.urls

# urls de app dashboard
extra_patterns1 = [
    url(r'^login', dashboard_views.login_view, name = 'login'),
    url(r'^logout/$', dashboard_views.login_out, name = 'logout'),
    url(r'^home/$', dashboard_views.home, name = 'home'),
    url(r'^agentes/(?P<id_cliente>[-A-Za-z0-9_]+)$', dashboard_views.listar_agentes, name = 'agentes'),
    url(r'^interacciones/(?P<id_agente>[-A-Za-z0-9_]+)$', dashboard_views.listar_interacciones, name = 'interacciones'),
    url(r'^base/(?P<id_agente>[-A-Za-z0-9_]+)/(?P<source>[-A-Za-z0-9_]+)$', dashboard_views.interacciones_source, name = 'base'),
    url(r'^get_request_detail$', dashboard_views.get_request_detail, name = 'get_request_detail'),
    url(r'^activar/$', dashboard_views.activar, name = 'activar'),
    url(r'^eliminar_notificacion3/(?P<id_ses>[-A-Za-z0-9_]+)/(?P<id_plat>[-A-Za-z0-9_]+)/(?P<id_not>[-A-Za-z0-9_]+)$', dashboard_views.eliminar_notificacion3, name='eliminar_notificacion3'),
    url(r'^eliminar_notificacion2/(?P<id_cita>[-A-Za-z0-9_]+)$', dashboard_views.eliminar_notificacion2, name='eliminar_notificacion2'),
    url(r'^eliminar_notificacion5/(?P<id_cita>[-A-Za-z0-9_]+)$', dashboard_views.eliminar_notificacion5, name='eliminar_notificacion5'),
    url(r'^eliminar_notificacion4/(?P<id_not>[-A-Za-z0-9_]+)/(?P<id_ses>[-A-Za-z0-9_]+)$', dashboard_views.eliminar_notificacion4, name='eliminar_notificacion4'),
    url(r'^sesiones/(?P<id_agente>[-A-Za-z0-9_]+)/(?P<source>[-A-Za-z0-9_]+)$', dashboard_views.sesiones, name = 'sesiones'),
    url(r'^timeline/(?P<id_agente>[-A-Za-z0-9_]+)/(?P<source>[-A-Za-z0-9_]+)/(?P<session>[-A-Za-z0-9_]+)$', dashboard_views.timeline, name = 'timeline'),
    url(r'^timelinePDF/(?P<id_agente>[-A-Za-z0-9_]+)/(?P<source>[-A-Za-z0-9_]+)/(?P<session>[-A-Za-z0-9_]+)$', dashboard_views.timelinePDF, name = 'timelinePDF'),
    url(r'^facebook_room/(?P<id_agente>[-A-Za-z0-9_]+)/(?P<source>[-A-Za-z0-9_]+)/(?P<session>[-A-Za-z0-9_]+)/(?P<chat_id>[-A-Za-z0-9_]+)$', dashboard_views.facebook_room, name = 'facebook_room'),
    url(r'^telegram_room/(?P<id_agente>[-A-Za-z0-9_]+)/(?P<source>[-A-Za-z0-9_]+)/(?P<session>[-A-Za-z0-9_]+)/(?P<chat_id>[-A-Za-z0-9_]+)$', dashboard_views.telegram_room, name = 'telegram_room'),
    url(r'^whatsapp_room/(?P<id_agente>[-A-Za-z0-9_]+)/(?P<source>[-A-Za-z0-9_]+)/(?P<session>[-A-Za-z0-9_]+)$', dashboard_views.whatsapp_room, name = 'whatsapp_room'),
    url(r'^ajax/cambiar_sesion_status_on_close/$', dashboard_views.cambiar_sesion_status_on_close, name='sesion_status'),
    url(r'^ajax/cambiar_sesion_status_on_enter/$', dashboard_views.cambiar_sesion_status_on_enter, name='sesion_status_enter'),
    url(r'^ajax/eliminar_notificacion/$', dashboard_views.eliminar_notificacion, name='eliminar_notificacion'),
    url(r'^ajax/contador_interacciones/$', dashboard_views.contador_interacciones, name='contador_interacciones'),
    url(r'^ajax/contador_sesiones/$', dashboard_views.contador_sesiones, name='contador_sesiones'),
    url(r'^ajax/borrar_registro_interacciones_realtime/$', dashboard_views.borrar_registro_interacciones_realtime, name='borrar_registro_interacciones_realtime'),
    url(r'^notificaciones/$', dashboard_views.notificaciones, name = 'notificaciones'),
    url(r'^getDataTableInfoRealTime/(?P<id_agente>[-A-Za-z0-9_]+)/(?P<source>[-A-Za-z0-9_]+)/(?P<session>[-A-Za-z0-9_]+)$', dashboard_views.getDataTableInfoRealTime, name = 'getDataTableInfoRealTime'),
    url(r'^send_message$', dashboard_views.send_message, name = 'send_message'),
    url(r'^send_session$', dashboard_views.send_session, name = 'send_session'),
    url(r'^imput_message$', dashboard_views.imput_message, name = 'imput_message'),
    url(r'^chats/(?P<agente>[-A-Za-z0-9_]+)/(?P<id_sesion>[-A-Za-z0-9_]+)$', dashboard_views.chats, name = 'chats'),
    url(r'^cita_resuelta/(?P<id_cita>[-A-Za-z0-9_]+)$', dashboard_views.cita_resuelta, name = 'cita_resuelta'),
    url(r'^webchat_room/(?P<id_agente>[-A-Za-z0-9_]+)/(?P<source>[-A-Za-z0-9_]+)/(?P<session>[-A-Za-z0-9_]+)$', dashboard_views.webchat_room, name = 'webchat_room'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urls de app estadisticas
extra_patterns2 = [
    url(r'^entradas_utilizadas/(?P<id_agente>[-A-Za-z0-9_]+)$', estadisticas_views.entradas_utilizadas, name = 'entradas_utilizadas'),
    url(r'^frases_erroneas/(?P<id_agente>[-A-Za-z0-9_]+)$', estadisticas_views.frases_erroneas, name = 'frases_erroneas'),
    url(r'^interacciones_hora/(?P<id_agente>[-A-Za-z0-9_]+)$', estadisticas_views.interacciones_hora, name = 'interacciones_hora'),
    url(r'^interacciones_fecha/(?P<id_agente>[-A-Za-z0-9_]+)$', estadisticas_views.interacciones_fecha, name = 'interacciones_fecha'),
    url(r'^sesiones_fecha/(?P<id_agente>[-A-Za-z0-9_]+)$', estadisticas_views.sesiones_fecha, name = 'sesiones_fecha'),
    url(r'^estadisticas_personalizadas/$', estadisticas_views.graficas_personalizadas, name = 'graficas_personalizadas'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urls de app usuarios
extra_patterns3 = [
    url(r'^modificar_perfil/(?P<id_usuario>[-A-Za-z0-9_]+)$', usuarios_views.modificar_perfil, name = 'modificar_perfil'),
    url(r'^operadores/$', usuarios_views.empleados, name='operadores'),
    url(r'^faq/$', usuarios_views.faq, name='faq'),
    url(r'^bots_afiliados/(?P<id_usuario>[-A-Za-z0-9_]+)$', usuarios_views.bots_afiliados, name='bots_afiliados'),
    url(r'^modificar_bots_afiliados/(?P<id_usuario>[-A-Za-z0-9_]+)/(?P<id_bot>[-A-Za-z0-9_]+)$', usuarios_views.modificar_bots_afiliados, name='modificar_bots_afiliados'),
    url(r'^perfil/(?P<id_usuario>[-A-Za-z0-9_]+)$', usuarios_views.perfil, name = 'perfil'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urls de app admin1
extra_patterns4 = [
    url(r'^ingresar_cliente/$', admin1_views.ingresar_cliente, name='ingresar_cliente'),
    url(r'^ingresar_agente/$', admin1_views.ingresar_agente, name='ingresar_agente'),
    url(r'^ingresar_usuario/$', admin1_views.ingresar_usuario, name='ingresar_usuario'),
    url(r'^borrar_sesiones/$', admin1_views.borrar_sesiones_plataforma, name='borrar_sesiones'),
    url(r'^ingresar_grafica/$', admin1_views.ingresar_grafica, name='ingresar_grafica'),
    url(r'^asignar_grafica/(?P<id_cliente>[-A-Za-z0-9_]+)/$', admin1_views.asignar_grafica, name='asignar_grafica'),
    url(r'^agregar_graficas/(?P<id_cliente>[-A-Za-z0-9_]+)/(?P<id_grafica>[-A-Za-z0-9_]+)/$', admin1_views.agregar_graficas, name='agregar_graficas'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urls de app admin2
extra_patterns5 = [
    url(r'^equipos_agente/$', admin2_views.equipos_agente, name='equipos_agente'),
    url(r'^equipos_agenteusuarios/(?P<id_agente>[-A-Za-z0-9_]+)$', admin2_views.equipos_agenteusuarios, name='equipos_agenteusuarios'),
    url(r'^ingresar_operador/$', admin2_views.ingresar_operador, name='ingresar_operador'),
    url(r'^ajax/asignar_bot/$', admin2_views.asignar_bot, name='asignar_bot'),
    url(r'^ajax/desvincular_bot/$', admin2_views.desvincular_bot, name='desvincular_bot'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urls para Recuperar contraseña
extra_patterns6 = [
    url(r'^reset/password_reset/$', password_reset,
        {'template_name':'registration/password_reset_form.html',
        'email_template_name':'registration/password_reset_email.html'},
        name='password_reset'),
    url(r'^reset/password_reset_done/$', password_reset_done,
        {'template_name':'registration/password_reset_done.html'},
        name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[-A-Za-z0-9_]+)/(?P<token>.+)$', password_reset_confirm,
        {'template_name':'registration/password_reset_confirm.html'},
        name='password_reset_confirm'),
    url(r'^reset/done/$', password_reset_complete,
        {'template_name':'registration/password_reset_complete.html'},
        name='password_reset_complete'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urls generales
urlpatterns = [
    url(r'^$', dashboard_views.index, name='index'),
    url(r'^admin/', admin.site.urls),
    url('^inbox/notifications/', include(notifications.urls, namespace='notifications')),
    url(r'^', include(extra_patterns1)),
    url(r'^', include(extra_patterns2)),
    url(r'^', include(extra_patterns3)),
    url(r'^', include(extra_patterns4)),
    url(r'^', include(extra_patterns5)),
    url(r'^', include(extra_patterns6)),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
