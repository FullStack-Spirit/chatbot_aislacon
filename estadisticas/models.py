from django.db import models
from dashboard.models import *

# Create your models here.

class Grafica(models.Model):
	nombre_grafica = models.CharField('Nombre de la gráfica', max_length=600, unique = True)
	nombre_funcion = models.CharField('Nombre de la función', max_length=600, unique = True)
	nombre_div = models.CharField('Nombre del div para la gráfica', max_length=600, unique = True)
	nombre_variable = models.CharField('Nombre de la variable', max_length=600, unique = True)

	def __str__ (self):
		return str (self.nombre_grafica)

class GraficaCliente(models.Model):
	grafica = models.ForeignKey(Grafica, related_name="grafica_grafica")
	cliente = models.ForeignKey(Cliente, related_name="grafica_cliente", blank = False)

	def __str__(self):
		return str ((self.grafica.nombre_grafica)+" "+(self.cliente.nombre_cliente))
