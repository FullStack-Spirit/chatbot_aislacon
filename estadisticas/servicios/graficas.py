
from usuarios.models import *
from dashboard.models import *
from estadisticas.models import *
from django.db.models import Count
from django.db.models.functions import Lower
from datetime import *
from django.db.models.functions import *

def intenciones_frecuentes(id_usuario, id_cliente, agentes, sesiones):
	
	usuario = Usuario.objects.get(pk = id_usuario)
	cliente = Cliente.objects.get(pk = id_cliente)
	grafica = Grafica.objects.get(nombre_funcion = "intenciones_frecuentes")
	#grafica = Grafica.objects.get(nombre_funcion = "frases_no_reconocidas")

	requiereGrafica = GraficaCliente.objects.all().filter(cliente = cliente, grafica = grafica ).exists()

	if requiereGrafica == 0:
		intents_totales = 0
	else:
		intents_totales = Interaccion.objects.filter(session__in=sesiones)\
		.values('intent_displayName').distinct()\
		.annotate(totales=(Count('intent_displayName'))).order_by('-totales')[:10]

	return intents_totales

def frases_no_reconocidas(id_usuario, id_cliente, agentes, sesiones):
	usuario = Usuario.objects.get(pk = id_usuario)
	cliente = Cliente.objects.get(pk = id_cliente)
	#grafica = Grafica.objects.get(nombre_funcion = "intenciones_frecuentes")
	grafica = Grafica.objects.get(nombre_funcion = "frases_no_reconocidas")

	requiereGrafica = GraficaCliente.objects.all().filter(cliente = cliente, grafica = grafica ).exists()

	if requiereGrafica == 0:
		frases_noReconocidas = 0
	else:
		frases_noReconocidas = Interaccion.objects.filter(session__in=sesiones)\
		.values('intent_displayName').distinct()\
		.filter(intent_displayName__exact='Default Fallback Intent')\
		.values(queryText=(Lower('queryText'))).distinct()\
		.annotate(totales=(Count('queryText'))).order_by('-totales')[:10]

	return frases_noReconocidas

def interacciones_por_hora(id_usuario, id_cliente, agentes, sesiones):
	usuario = Usuario.objects.get(pk = id_usuario)
	cliente = Cliente.objects.get(pk = id_cliente)
	#grafica = Grafica.objects.get(nombre_funcion = "intenciones_frecuentes")
	grafica = Grafica.objects.get(nombre_funcion = "interacciones_por_hora")

	requiereGrafica = GraficaCliente.objects.all().filter(cliente = cliente, grafica = grafica ).exists()

	if requiereGrafica == 0:
		interacciones_hora = 0
	else:
		# === INTERACCIONES POR HORA ===
		interacciones_hora = Interaccion.objects.filter(session__in=sesiones, date__date=date.today())\
		.annotate(hora=TruncHour('date'))\
		.values('hora').distinct()\
		.annotate(total=Count('hora'))\
		.order_by('hora')

	return interacciones_hora

def estadisticas_7_dias(id_usuario, id_cliente, agentes, sesiones):
	estadisticas_return = []
	ultimaFecha = Interaccion.objects.dates('date','day').filter(session__in=sesiones).last()
	estadisticas_return.append(ultimaFecha)

	interacciones_totales = Interaccion.objects.filter(session__in=sesiones)
	estadisticas_return.append(interacciones_totales)

	fecha6 = ultimaFecha - timedelta(days=1)
	estadisticas_return.append(fecha6)
	fecha5 = ultimaFecha - timedelta(days=2)
	estadisticas_return.append(fecha5)
	fecha4 = ultimaFecha - timedelta(days=3)
	estadisticas_return.append(fecha4)
	fecha3 = ultimaFecha - timedelta(days=4)
	estadisticas_return.append(fecha3)
	fecha2 = ultimaFecha - timedelta(days=5)
	estadisticas_return.append(fecha2)
	fecha1 = ultimaFecha - timedelta(days=6)
	estadisticas_return.append(fecha1)
	fecha7 = ultimaFecha + timedelta(days=1)
	estadisticas_return.append(fecha7)

	nro_interacciones_7_dias = Interaccion.objects.filter(date__range=(fecha1,fecha7),session__in=sesiones).count()
	estadisticas_return.append(nro_interacciones_7_dias)
	# OBTIENE UN LISTADO DE LAS INTERACCIONES POR CADA FECHA DADA
	nro_interacciones1 = Interaccion.objects.filter(date__range=(fecha1,fecha2),session__in=sesiones).count()
	estadisticas_return.append(nro_interacciones1)
	nro_interacciones2 = Interaccion.objects.filter(date__range=(fecha2,fecha3),session__in=sesiones).count()
	estadisticas_return.append(nro_interacciones2)
	nro_interacciones3 = Interaccion.objects.filter(date__range=(fecha3,fecha4),session__in=sesiones).count()
	estadisticas_return.append(nro_interacciones3)
	nro_interacciones4 = Interaccion.objects.filter(date__range=(fecha4,fecha5),session__in=sesiones).count()
	estadisticas_return.append(nro_interacciones4)
	nro_interacciones5 = Interaccion.objects.filter(date__range=(fecha5,fecha6),session__in=sesiones).count()
	estadisticas_return.append(nro_interacciones5)
	nro_interacciones6 = Interaccion.objects.filter(date__range=(fecha6,ultimaFecha),session__in=sesiones).count()
	estadisticas_return.append(nro_interacciones6)
	nro_interacciones7 = Interaccion.objects.filter(date__range=(ultimaFecha,fecha7),session__in=sesiones).count()
	estadisticas_return.append(nro_interacciones7)

	# === SESIONES POR DÍA ===
	nro_sesiones_sesiones_7_dias = Interaccion.objects.values('session_id').filter(date__range=(fecha1,fecha7),session__in=sesiones).distinct().count()
	estadisticas_return.append(nro_sesiones_sesiones_7_dias)
	#OBTIENE UN LISTADO DE LAS INTERACCIONES POR CADA FECHA DADA
	nro_sesiones1 = Interaccion.objects.values('session_id').filter(date__range=(fecha1,fecha2),session__in=sesiones).distinct().count()
	estadisticas_return.append(nro_sesiones1)
	nro_sesiones2 = Interaccion.objects.values('session_id').filter(date__range=(fecha2,fecha3),session__in=sesiones).distinct().count()
	estadisticas_return.append(nro_sesiones2)
	nro_sesiones3 = Interaccion.objects.values('session_id').filter(date__range=(fecha3,fecha4),session__in=sesiones).distinct().count()
	estadisticas_return.append(nro_sesiones3)
	nro_sesiones4 = Interaccion.objects.values('session_id').filter(date__range=(fecha4,fecha5),session__in=sesiones).distinct().count()
	estadisticas_return.append(nro_sesiones4)
	nro_sesiones5 = Interaccion.objects.values('session_id').filter(date__range=(fecha5,fecha6),session__in=sesiones).distinct().count()
	estadisticas_return.append(nro_sesiones5)
	nro_sesiones6 = Interaccion.objects.values('session_id').filter(date__range=(fecha6,ultimaFecha),session__in=sesiones).distinct().count()
	estadisticas_return.append(nro_sesiones6)
	nro_sesiones7 = Interaccion.objects.values('session_id').filter(date__range=(ultimaFecha,fecha7),session__in=sesiones).distinct().count()
	estadisticas_return.append(nro_sesiones7)
	nro_sesionest = Interaccion.objects.values('session_id').filter(date__range=(fecha1,fecha7),session__in=sesiones).distinct().count()
	estadisticas_return.append(nro_sesionest)


	return estadisticas_return

def citas_saito(id_usuario, id_cliente, agentes, sesiones):
	usuario = Usuario.objects.get(pk = id_usuario)
	cliente = Cliente.objects.get(pk = id_cliente)
	grafica = Grafica.objects.get(nombre_funcion = "citas_saito")
	#grafica = Grafica.objects.get(nombre_funcion = "frases_no_reconocidas")

	requiereGrafica = GraficaCliente.objects.all().filter(cliente = cliente, grafica = grafica ).exists()

	if requiereGrafica == 0:
		posibles_citas = 0
	else:
		posibles_citas = Cita.objects.all().filter(estado = 0).order_by('fecha')

	return posibles_citas
