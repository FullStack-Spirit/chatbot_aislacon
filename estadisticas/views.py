# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from dashboard.models import *
from usuarios.models import *
from datetime import datetime, timedelta
from django.db import IntegrityError
import os
import dialogflow
import requests
import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.utils import timezone
from django.db.models import *
from dashboard.utils import *
from django.db.models.functions import Lower
import time
from estadisticas.models import *
# Importamos la función de prueba de graficas
from estadisticas.servicios.graficas import *
import psycopg2
from django.db import connection

# === ALEJANDRO BARCENAS ===

def entradas_utilizadas(request, id_agente, template_name = "estadisticas/entradas_utilizadas.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		empleado = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)

	sesiones_del_agente = Sesion.objects.filter(id_proyecto=Agente.objects.get(pk=id_agente))
	# INTERACCIONES TOTALES DEL AGENTE
	intents_totales = Interaccion.objects.filter(session__in=sesiones_del_agente)\
	.values('intent_displayName').distinct()\
	.annotate(totales=(Count('intent_displayName'))).order_by('-totales')[:5]
	return render(request, template_name, locals(),)

def frases_erroneas(request, id_agente, template_name = "estadisticas/frases_erroneas.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		empleado = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)

	sesiones_del_agente = Sesion.objects.filter(id_proyecto=Agente.objects.get(pk=id_agente))
	# INTERACCIONES TOTALES DEL AGENTE
	frases_noReconocidas = Interaccion.objects.filter(session__in=sesiones_del_agente)\
	.values('intent_displayName').distinct()\
	.filter(intent_displayName__exact='Default Fallback Intent')\
	.values(queryText=(Lower('queryText'))).distinct()\
	.annotate(totales=(Count('queryText'))).order_by('-totales')[:10]

	return render(request, template_name, locals(),)

# === LUIS ANGEL ===

def interacciones_hora(request, id_agente, template_name="estadisticas/interacciones_hora.html"):
	datos_empresa = DatosEmpresa.objects.first()
	dia = time.strftime("%d")
	mes = time.strftime("%b")
	ano = time.strftime("%Y")
	fecha = ano+"-"+mes+"-"+dia+" 00:00:00.000000-00"
	usuario = request.user
	try:
		empleado = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)

	sesiones_del_agente = Sesion.objects.filter(id_proyecto=Agente.objects.get(pk=id_agente))
	interacciones = Interaccion.objects.filter(session__in=sesiones_del_agente, date__day=dia)
	hora = Interaccion.objects.filter(session__in=sesiones_del_agente, date__hour=8)
	total_int = interacciones.count()

	return render(request, template_name, locals(),)

# === ANGEL PEREZ ===

def interacciones_fecha(request, id_agente, template_name="estadisticas/interacciones_fecha.html"):
	datos_empresa = DatosEmpresa.objects.first()
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
	try:
		empleado = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	# SESIONES DEL AGENTE
	nro_sesiones = Sesion.objects.filter(id_proyecto=Agente.objects.get(pk=id_agente))
	#OBTENER LA ULTIMA FECHA INGRESADA A LA BASE
	ultimaFecha = Interaccion.objects.dates('date','day').last()
	#OBTENER UN LISTADO DE LAS FECHAS A PARTIR DE LA ULTIMA CON DIFERENCIA DE UN DIA CADA UNA
	fecha6 = ultimaFecha - timedelta(days=1)
	fecha5 = ultimaFecha - timedelta(days=2)
	fecha4 = ultimaFecha - timedelta(days=3)
	fecha3 = ultimaFecha - timedelta(days=4)
	fecha2 = ultimaFecha - timedelta(days=5)
	fecha1 = ultimaFecha - timedelta(days=6)
	fecha7 = ultimaFecha + timedelta(days=1)
	#OBTIENE UN LISTADO DE LAS INTERACCIONES POR CADA FECHA DADA
	nro_interacciones1 = Interaccion.objects.filter(date__range=(fecha1,fecha2)).count()
	nro_interacciones2 = Interaccion.objects.filter(date__range=(fecha2,fecha3)).count()
	nro_interacciones3 = Interaccion.objects.filter(date__range=(fecha3,fecha4)).count()
	nro_interacciones4 = Interaccion.objects.filter(date__range=(fecha4,fecha5)).count()
	nro_interacciones5 = Interaccion.objects.filter(date__range=(fecha5,fecha6)).count()
	nro_interacciones6 = Interaccion.objects.filter(date__range=(fecha6,ultimaFecha)).count()
	nro_interacciones7 = Interaccion.objects.filter(date__range=(ultimaFecha,fecha7)).count()

	return render(request, template_name, locals(),)

	# === ANGEL PEREZ ===
def sesiones_fecha(request, id_agente, template_name="estadisticas/sesiones_fecha.html"):
	datos_empresa = DatosEmpresa.objects.first()
	agente = Agente.objects.get(pk=id_agente)
	cliente = Cliente.objects.get(nombre_cliente=agente.cliente)
	try:
		empleado = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0

	#OBTENER LA ULTIMA FECHA INGRESADA A LA BASE
	ultimaFecha = Interaccion.objects.values('date').dates('date','day').last()

	#OBTENER UN LISTADO DE LAS FECHAS A PARTIR DE LA ULTIMA CON DIFERENCIA DE UN DIA CADA UNA
	fecha6 = ultimaFecha - timedelta(days=1)
	fecha5 = ultimaFecha - timedelta(days=2)
	fecha4 = ultimaFecha - timedelta(days=3)
	fecha3 = ultimaFecha - timedelta(days=4)
	fecha2 = ultimaFecha - timedelta(days=5)
	fecha1 = ultimaFecha - timedelta(days=6)
	fecha7 = ultimaFecha + timedelta(days=1)
	#OBTIENE UN LISTADO DE LAS INTERACCIONES POR CADA FECHA DADA
	nro_sesiones1 = Interaccion.objects.values('session_id').filter(date__range=(fecha1,fecha2)).distinct().count()
	nro_sesiones2 = Interaccion.objects.values('session_id').filter(date__range=(fecha2,fecha3)).distinct().count()
	nro_sesiones3 = Interaccion.objects.values('session_id').filter(date__range=(fecha3,fecha4)).distinct().count()
	nro_sesiones4 = Interaccion.objects.values('session_id').filter(date__range=(fecha4,fecha5)).distinct().count()
	nro_sesiones5 = Interaccion.objects.values('session_id').filter(date__range=(fecha5,fecha6)).distinct().count()
	nro_sesiones6 = Interaccion.objects.values('session_id').filter(date__range=(fecha6,ultimaFecha)).distinct().count()
	nro_sesiones7 = Interaccion.objects.values('session_id').filter(date__range=(ultimaFecha,fecha7)).distinct().count()
	nro_sesionest = Interaccion.objects.values('session_id').filter(date__range=(fecha1,fecha7)).distinct().count()

	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
def graficas_personalizadas(request, template_name = "estadisticas/graficas_personalizadas.html"):
	query2 = connection.cursor()
	query2.execute("SELECT registro.empresa, registro.domicilio, registro.telefono, registro.contacto, registro.apellidos, registro.correo, registro.sitio, registro.comentario, registro.celular, i.nombre ,h.nombre ,g.municipio, f.estado, STRING_AGG(d.nombre, ','), STRING_AGG(e.nombre, ',') FROM registro INNER JOIN registro_nichos as b ON registro.sesion = b.sesion INNER JOIN registro_camaras as c on registro.sesion = c.sesion INNER JOIN nicho as d on b.id_nicho = d.id INNER JOIN tipo_camara as e ON c.id_camara = e.id INNER JOIN estados as f ON registro.id_entidad = f.id INNER JOIN municipios as g ON registro.id_ciudad = g.id INNER JOIN proyecto as h ON registro.id_proyecto = h.id INNER JOIN sector as i ON registro.id_sector = i.id GROUP BY registro.empresa, registro.domicilio, registro.telefono, registro.contacto, registro.apellidos, registro.correo, registro.sitio, registro.comentario, registro.celular, i.nombre ,h.nombre ,g.municipio, f.estado")
	result = query2.fetchall()
	# Verificamos si el usuario tiene foto, Si existe algún registro en
	# Tabla usuarios que referencie al usuario en turno y si este posee
	# foto cargada al sistema
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		clientes = Cliente.objects.all()
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0

	# === INTERACCIONES POR DÍA ===
	# CLIENTE ESPECIFICO DEL USUARIO EN TURNO
	if User.objects.filter(groups__name='Administrador', id=usuario.id).count() > 0:
		pass
	else:
		cliente = Cliente.objects.get(pk=extras.cliente.id)

	if User.objects.filter(groups__name='Cliente Administrador', id=usuario.id).count() > 0:			

		# === USUARIOS NO ADMINISTRADORES ===
		userNo_admins = User.objects.filter(groups__name='Usuario Cliente')
		# === USUARIOS DEL CLIENTE ===
		usuarios = Usuario.objects.filter(cliente=cliente, user__in=userNo_admins)
		# BOTS DEL CLIENTE
		agentes = Agente.objects.filter(cliente=cliente)
		# SESIONES DE TODOS AGENTES DEL CLIENTE
		sesiones = Sesion.objects.filter(id_proyecto__in=agentes)
		if sesiones.count() > 0:

			# Obtiene las graficas que quiere el cliente en el dashboard
			graficas_solicitadas = GraficaCliente.objects.all().filter(cliente = cliente)
			# Recorre las graficas que quiere el usuario
			for graph in graficas_solicitadas:
				grafica = Grafica.objects.get(pk = graph.grafica.pk)
				"""nombre = str(grafica.nombre_funcion) + "(" + str(extras.pk) + "," + str(cliente.pk) + ")"
				print(nombre)
				intents_totales = eval(nombre)
				print(intents_totales)"""

				# Si quiere la gráfica de intenciones frecuentes
				if grafica.nombre_funcion == "intenciones_frecuentes":
					intents_totales = intenciones_frecuentes(extras.pk,cliente.pk, agentes, sesiones)

				# Si quiere la gráfica de frases no reconocidas
				elif grafica.nombre_funcion == "frases_no_reconocidas":
					frases_noReconocidas = frases_no_reconocidas(extras.pk,cliente.pk, agentes, sesiones)

				# Si quiere la gráfica de interacciones por hora
				elif grafica.nombre_funcion == "interacciones_por_hora":
					interacciones_hora = interacciones_por_hora(extras.pk,cliente.pk, agentes, sesiones)

					# SESIONES DEL DÍA
					sesiones_del_dia = Interaccion.objects.values('session_id').filter(date__date=date.today(),session__in=sesiones).distinct().count()
					x = interacciones_hora.aggregate(Avg('total'))

				elif grafica.nombre_funcion == "estadisticas_7_dias":
					estadisticas_return = estadisticas_7_dias(extras.pk,cliente.pk, agentes, sesiones)
					ult = estadisticas_return[0]

					interacciones_totales = estadisticas_return[1]

					fecha6 = estadisticas_return[2]
					fecha5 = estadisticas_return[3]
					fecha4 = estadisticas_return[4]
					fecha3 = estadisticas_return[5]
					fecha2 = estadisticas_return[6]
					fecha1 = estadisticas_return[7]
					fecha7 = estadisticas_return[8] - timedelta(days=1)

					nro_interacciones_7_dias = estadisticas_return[9]

					nro_interacciones1 = estadisticas_return[10]
					nro_interacciones2 = estadisticas_return[11]
					nro_interacciones3 = estadisticas_return[12]
					nro_interacciones4 = estadisticas_return[13]
					nro_interacciones5 = estadisticas_return[14]
					nro_interacciones6 = estadisticas_return[15]
					nro_interacciones7 = estadisticas_return[16]

					nro_sesiones_sesiones_7_dias = estadisticas_return[17]

					nro_sesiones1 = estadisticas_return[18]
					nro_sesiones2 = estadisticas_return[19]
					nro_sesiones3 = estadisticas_return[20]
					nro_sesiones4 = estadisticas_return[21]
					nro_sesiones5 = estadisticas_return[22]
					nro_sesiones6 = estadisticas_return[23]
					nro_sesiones7 = estadisticas_return[24]
					nro_sesionest = estadisticas_return[25]

				# Si quiere visualizar la tabla de citas (BOT SAITO)
				elif grafica.nombre_funcion == "citas_saito":
					posibles_citas = citas_saito(extras.pk,cliente.pk, agentes, sesiones)

		else:
			mensaje_sin_estadisticas = "No hay estadísticas que mostrar"

	elif User.objects.filter(groups__name='Usuario Cliente', id=usuario.id).count() > 0:

		usuario_real = Usuario.objects.get(user = usuario)
		try:
			agentes_usuario_real = AgenteUsuario.objects.filter(user=usuario_real)
			lista_de_id_agente = []
			for dato in agentes_usuario_real:
				lista_de_id_agente.append(dato.agente.id_proyecto)
			agentes_finales = Agente.objects.filter(id_proyecto__in=lista_de_id_agente)

			# SESIONES DE TODOS AGENTES DEL USUARIO
			sesiones_por_usuario = Sesion.objects.filter(id_proyecto__in=agentes_finales)

			# Obtiene las graficas que quiere el cliente en el dashboard
			graficas_solicitadas = GraficaCliente.objects.all().filter(cliente = cliente)
			# Recorre las graficas que quiere el usuario
			for graph in graficas_solicitadas:
				grafica = Grafica.objects.get(pk = graph.grafica.pk)
				print("YA ESTA EN FOR")
				# Si quiere la gráfica de intenciones frecuentes
				if grafica.nombre_funcion == "intenciones_frecuentes":
					intents_totales_por_usuario = intenciones_frecuentes(extras.pk,cliente.pk, agentes_finales, sesiones_por_usuario)
					# Si quiere la gráfica de frases no reconocidas
				elif grafica.nombre_funcion == "frases_no_reconocidas":
					frases_noReconocidas_por_usuario = frases_no_reconocidas(extras.pk,cliente.pk, agentes_finales, sesiones_por_usuario)
				# Si quiere la gráfica de interacciones por hora
				elif grafica.nombre_funcion == "interacciones_por_hora":
					interacciones_hora_por_usuario = interacciones_por_hora(extras.pk,cliente.pk, agentes_finales, sesiones_por_usuario)
					print("\n\n\n INTERACCIONES POR HORA POR USUARIO")
					print(interacciones_hora_por_usuario)
					print("\n\n\n")
					# SESIONES DEL DÍA
					sesiones_del_dia_por_usuario = Interaccion.objects.values('session_id').filter(date__date=date.today(),session__in=sesiones_por_usuario).distinct().count()
					x_por_usuario = interacciones_hora_por_usuario.aggregate(Avg('total'))

				elif grafica.nombre_funcion == "estadisticas_7_dias":
					estadisticas_return = estadisticas_7_dias(extras.pk,cliente.pk, agentes_finales, sesiones_por_usuario)
					ult = estadisticas_return[0]

					interacciones_totales_por_usuario = estadisticas_return[1]

					fecha6_por_usuario = estadisticas_return[2]
					fecha5_por_usuario = estadisticas_return[3]
					fecha4_por_usuario = estadisticas_return[4]
					fecha3_por_usuario = estadisticas_return[5]
					fecha2_por_usuario = estadisticas_return[6]
					fecha1_por_usuario = estadisticas_return[7]
					fecha7_por_usuario = estadisticas_return[8]

					nro_interacciones_7_dias_por_usuario = estadisticas_return[9]

					nro_interacciones1_por_usuario = estadisticas_return[10]
					nro_interacciones2_por_usuario = estadisticas_return[11]
					nro_interacciones3_por_usuario = estadisticas_return[12]
					nro_interacciones4_por_usuario = estadisticas_return[13]
					nro_interacciones5_por_usuario = estadisticas_return[14]
					nro_interacciones6_por_usuario = estadisticas_return[15]
					nro_interacciones7_por_usuario = estadisticas_return[16]

					nro_sesiones_sesiones_7_dias_por_usuario = estadisticas_return[17]

					nro_sesiones1_por_usuario = estadisticas_return[18]
					nro_sesiones2_por_usuario = estadisticas_return[19]
					nro_sesiones3_por_usuario = estadisticas_return[20]
					nro_sesiones4_por_usuario = estadisticas_return[21]
					nro_sesiones5_por_usuario = estadisticas_return[22]
					nro_sesiones6_por_usuario = estadisticas_return[23]
					nro_sesiones7_por_usuario = estadisticas_return[24]
					nro_sesionest_por_usuario = estadisticas_return[25]

				# Si quiere visualizar la tabla de citas (BOT SAITO)
				elif grafica.nombre_funcion == "citas_saito":
					posibles_citas = citas_saito(extras.pk,cliente.pk, agentes_finales, sesiones_por_usuario)

		except:
			mensaje_sin_estadisticas = "No hay estadísticas que mostrar, el usuario no tiene bots asignados"

	return render(request, template_name, locals(),)
