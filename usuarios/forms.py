from django import forms
import datetime
from .models import *
from django.contrib.admin import widgets 
from django.forms.extras.widgets import SelectDateWidget
from django.contrib.auth.models import User

years_to_display = range(datetime.datetime.now().year - 100, datetime.datetime.now().year + 1)

class ProfileForm(forms.ModelForm):

    class Meta:
        model = Usuario
        fields = ('apellido_paterno', \
         	'apellido_materno', \
         	'fecha_nacimiento', \
         	'direccion', \
			'tlf', \
			'edad', \
			'cargo')
       	widgets = {
            'fecha_nacimiento': forms.SelectDateWidget(years=years_to_display),
            'direccion': forms.Textarea(attrs={'class': 'form-control', 'rows': '5'}),
            'edad': forms.NumberInput(attrs={'class': 'form-control', 'rows': '2'})
        }
    fecha_nacimiento = forms.DateField(
        widget=forms.DateInput(format='%d/%m/%Y'),
        input_formats=('%d/%m/%Y', )
        )
    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        for campos in self.fields:
            self.fields[campos].widget.attrs.update({'class': 'form-control'})
            self.fields['fecha_nacimiento'].widget.attrs["data-mask"] = '99/99/9999'
            self.fields['edad'].widget.attrs["min"] = "17"
            self.fields['edad'].widget.attrs["max"] = "90"

class ProfileImageForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('foto',)

    foto = forms.ImageField()
    def __init__(self, *args, **kwargs):
        super(ProfileImageForm, self).__init__(*args, **kwargs)
        for campos in self.fields:
            self.fields[campos].widget.attrs.update({'class': 'form-control'})


class UserForm(forms.Form):
    password = forms.CharField(label = "Contraseña nueva", required = True, widget = forms.PasswordInput(attrs={'id': 'password', 'name': 'password', 'class': 'form-control required', 'placeholder': 'Password'}))
    confirm = forms.CharField(label = "Confirma contraseña nueva", required = False, widget = forms.PasswordInput(attrs={'id': 'confirm', 'name': 'confirm','class': 'form-control required', 'placeholder': 'Password'}))
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        # Errores predeterminados definidos en el modelo este disparará errores para campo requerido, unico, invalido y con caracterers faltantes
        for field in self.fields.values():
            field.error_messages = {'unique':'{fieldname} registrada en el sistema'.format(
                fieldname=field.label), 'invalid':'Valor Inválido'.format(
                fieldname=field.label), 'min_length':'Realice completacion de campo {fieldname}'.format(
                fieldname=field.label)}

            