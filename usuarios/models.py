#-*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from os.path import splitext, basename
from django.core.validators import MaxValueValidator
from dashboard.models import *

class Usuario(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
	cliente = models.ForeignKey(Cliente, related_name="usuario_cliente", blank = False)
	apellido_paterno = models.CharField(max_length=600, blank = True, null = True)
	apellido_materno = models.CharField(max_length=600, blank = True, null = True)
	fecha_nacimiento = models.DateTimeField('fecha de nacimiento', auto_now_add = False, blank = True, null = True)
	tlf = models.CharField(max_length=255, blank = True, null = True)
	edad = models.IntegerField(blank = True, null = True)
	foto = models.ImageField(upload_to = 'profiles/', blank = True, null = True)
	cargo = models.CharField(max_length=600, blank = True, null = True)
	status = models.IntegerField(blank = False, default=1)
	direccion = models.CharField(max_length=800, blank = True, null = True)

	def __str__(self):
		return str ((self.user.first_name)+" "+(self.user.last_name))

class AgenteUsuario(models.Model):
	user = models.ForeignKey(Usuario, related_name="usuario")
	agente = models.ForeignKey(Agente, related_name="agente", blank = False)

	def __str__(self):
		return str ((self.user.user.first_name)+" "+(self.user.user.last_name)+" "+(self.agente.nombre_agente))
