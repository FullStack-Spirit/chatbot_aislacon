from django.shortcuts import render
from .forms import *
from dashboard.models import *
from dashboard.views import login_view
from .models import *
import datetime
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect

# Create your views here.

@login_required(login_url = '/login/')
def modificar_perfil(request, id_usuario, template_name = "usuarios/modificar_perfil.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	from pprint import pprint
	from dashboard.services.services import getUserInfoById, updateUserInfo, updateUserImage, updateUserPassword
	
	pprint(request)
	usuario_inDjango, usuarioExt, cliente = getUserInfoById(id_usuario)
	if request.method == 'POST':
		result = updateUserInfo(request, usuarioExt)
		result_image = updateUserImage(request, usuarioExt, id_usuario)
		result = updateUserPassword(request, usuarioExt)
		if result:
			return HttpResponseRedirect('/modificar_perfil/%s' % id_usuario)
		if result_image:
			return HttpResponseRedirect('/modificar_perfil/%s' % id_usuario)
	else:
		perfil = ProfileForm()
		imagen = ProfileImageForm()
		usuarioDjango = UserForm()
		perfil.fields['apellido_paterno'].initial = usuarioExt.apellido_paterno
		perfil.fields['apellido_materno'].initial = usuarioExt.apellido_materno
		perfil.fields['fecha_nacimiento'].initial = usuarioExt.fecha_nacimiento
		perfil.fields['direccion'].initial = usuarioExt.direccion
		perfil.fields['tlf'].initial = usuarioExt.tlf
		perfil.fields['edad'].initial = usuarioExt.edad
		perfil.fields['cargo'].initial = usuarioExt.cargo
	
	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
def empleados(request, template_name = "usuarios/empleados.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	usuarioExt = Usuario.objects.get(user=usuario)
	cliente = Cliente.objects.get(nombre_cliente=usuarioExt.cliente)
	empleados = Usuario.objects.all().filter(cliente=cliente)
	empleados_list = []
	for data in empleados:
		if data.user.groups.filter(name='Usuario Cliente').exists():
			empleados_list.append(data)			
	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
def faq(request, emplate_name="usuarios/faq.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	try:
		usuarioExt = Usuario.objects.get(user=usuario)
		cliente = Cliente.objects.get(nombre_cliente=usuarioExt.cliente)
	except:
		pass
	return render(request, emplate_name, locals(),)

@login_required(login_url = '/login/')
def perfil(request, id_usuario, template_name = "usuarios/perfil.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	estado = 0
	usuario_inDjango = User.objects.get(pk=id_usuario)
	usuarioExt = Usuario.objects.get(user=usuario_inDjango)
	cliente = Cliente.objects.get(nombre_cliente=usuarioExt.cliente)

	if User.objects.filter(groups__name='Usuario Cliente', id=usuario_inDjango.id).count() > 0:
		usario_real = Usuario.objects.get(user = usuario_inDjango)
		agentes_usuario_real = AgenteUsuario.objects.filter(user=usario_real)
		lista_de_id_agente = []
		for dato in agentes_usuario_real:
			lista_de_id_agente.append(dato.agente.id_proyecto)
		agentes_finales = Agente.objects.filter(id_proyecto__in=lista_de_id_agente)

	elif User.objects.filter(groups__name='Cliente Administrador', id=usuario_inDjango.id).count() > 0:
		# BOTS DEL CLIENTE
		agentes = Agente.objects.filter(cliente=cliente)

	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
def bots_afiliados(request, id_usuario, template_name = "usuarios/bots_afiliados.html"):
	datos_empresa = DatosEmpresa.objects.first()
	usuario = request.user
	try:
		extras = Usuario.objects.get(user = usuario.pk)
		foto = 1
	except:
		foto = 0
	estado = 0
	usuario_inDjango = User.objects.get(pk=id_usuario)
	usuarioExt = Usuario.objects.get(user=usuario_inDjango)
	cliente = Cliente.objects.get(nombre_cliente=usuarioExt.cliente)

	bots_afiliados = AgenteUsuario.objects.filter(user=usuarioExt)
	lista_de_id_agente = []
	for bot in bots_afiliados:
		lista_de_id_agente.append(bot.agente.id_proyecto)
	agentes_finales = Agente.objects.filter(id_proyecto__in=lista_de_id_agente)
	bots_no_afiliados = Agente.objects.all().filter(cliente = usuarioExt.cliente).exclude(id__in = agentes_finales)

	return render(request, template_name, locals(),)

@login_required(login_url = '/login/')
def modificar_bots_afiliados(request, id_usuario, id_bot):

	usuario_inDjango = User.objects.get(pk=id_usuario)
	usuarioExt = Usuario.objects.get(user=usuario_inDjango)
	bot = Agente.objects.get(pk = id_bot)
	ya_afiliado = AgenteUsuario.objects.filter(user=usuarioExt, agente = bot).exists()

	if ya_afiliado == 1:
		print("YA ESTÁ AFILIADO")
		bot_afiliado = AgenteUsuario.objects.filter(user=usuarioExt, agente = bot)
		bot_afiliado.delete()
	else:
		print("NO ESTÁ AFILIADO")
		no_afiliado = AgenteUsuario(user=usuarioExt, agente = bot)
		no_afiliado.save()

	return HttpResponseRedirect('/bots_afiliados/%s' % id_usuario)
